<?php

namespace CP;

use Nette;
use CP;

class Template
{
	private $defaultDir = "templates";

	protected $dir;

	protected $parameters = array();

	function __construct($translator)
	{
		$this->setTranslator($translator);

		$this->parameters["basePath"] = WWW_DIR;
		$this->parameters["locale"] = $this->translator->getLocale();
		$this->parameters["cpDir"] = CP_DIR;
		$this->parameters["appTemplatesRoot"] = APP_DIR . "/templates";
		$this->parameters["instancePath"] = \instance::getDir();
	}
	
	public $translator;

	protected function setTranslator($translator)
	{
		$this->translator = $translator;
	}

	public function create()
	{
		$this->setParameters();

		$this->setDir($this->defaultDir);

		$this->setLocale(null);

		return $this;
	}

	public function setDir($dir)
	{
		$this->dir = $dir;

		return $this;
	}

	protected function getPath($path)
	{
		//if(strpos($path, "/") !== false) dump($path);

		$paths = array();

		$path = (strpos($path, "/") === false ? $this->dir . "/" : null) . ($this->getLocale() ? $this->getLocale() . "/" : null) . $path . (substr(strtolower($path), -6, 6) != ".latte" ? ".latte" : null);

		/*if(strpos($path, "Subject") !== false){
			dump($path);
		}*/

		if(strpos($path, APP_DIR . "/") !== false)
			$path = str_replace(APP_DIR . "/", "", $path);

		$instanceDir = \instance::getDir();

		$paths[] = $instanceDir . "/$path";

		$paths[] = APP_DIR . "/$path";

		foreach ($paths AS $path) {
			if (is_file($path))
				return $path;
		}

		dump("Template path: '$path' does not exists!");

		die;
	}

	public function setParameters($parameters = array())
	{
		$this->parameters = array_extend(
			$this->parameters, (array)$parameters
		);

		return $this;
	}
	public $template;

	public function setFile($path)
	{
		$path = $this->getPath($path);

		$this->template = new Nette\Templating\FileTemplate($path);

		return $this;
	}

	public function setSource($source)
	{
		$this->template = new Nette\Templating\Template();

		$this->template->setSource($source);

		return $this;
	}

	public function process()
	{
		if ($this->translator)
			$this->template->setTranslator($this->translator);

		$this->template->registerFilter(new Nette\Latte\Engine);

		$this->template->registerHelperLoader('Nette\Templating\Helpers::loader');

		CP\Helpers\Helpers::register($this);

		$this->template->setParameters($this->parameters);

		$this->template = (string)$this->template;

		return $this->template;
	}

	public function __toString()
	{
		return $this->template;
	}
	protected $locale;

	public function setLocale($locale)
	{
		$this->locale = $locale;

		return $this;
	}

	protected function getLocale()
	{
		$locale = $this->locale; //$this->locale ? $this->locale : ($this->translator ? $this->translator->getLocale() : false);

		return $locale;
	}
}
?>