<?php

namespace CP\Presenters;

use Nette;
use CP\Forms\ArticleForm;
use Nette\Application\BadRequestException;

trait AdminArticlesPresenter
{
	/** @var \CP\Services\Articles @inject */
	public $articlesService;

	private $article;

	public function handleAdd($owner_table, $owner_id)
	{
		$article = array(
			"owner_table" => $owner_table, 
			"owner_id" => $owner_id
		);

		$article = $this->articlesService->create($article);

		$this->redirect("Articles:add", $article["article_id"]);
	}

	public function actionAdd($article_id)
	{
		$this->actionEdit($article_id);
	}

	public function actionEdit($article_id)
	{
		$article = $this->articlesService->getOneById($article_id);

		if($article){
			$this->article = $article;

			$this->template->article = $this->article;
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentArticleForm()
	{
		$form = new ArticleForm($this);

		$form->setImagesDir("{$this->client["dir"]}/files/articles");

		$form->setValues($this->article);
		
		return $form->create();
	}

	public function handleRemove($article_id)
	{
		if ($this->articlesService->remove($article_id)) {
			$this->flashMessage("Článek byl odebrán", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Articles:default");
		} else
			$this->flashMessage("Při odebírání článku došlo k chybě!", "alert-danger");
	}	
}