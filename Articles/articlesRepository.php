<?php

namespace Repository;

class articles extends \Repository\BaseRepository
{
	public $name = "articles";

	public $fields = array(
		"article_id" => "primary",
		"owner_table" => "varchar",
		"owner_id" => "int",
		"client_id" => "int",
		"article_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"article_snippet" => array(
			"type" => "lighthtml",
			"locale" => true
		),
		"article_content" => array(
			"type" => "html",
			"locale" => true
		),
		"article_published" => "timestamp",
		"user_id" => "int",
	);

	public $validateTable = true;
}