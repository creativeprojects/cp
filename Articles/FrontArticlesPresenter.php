<?php

namespace CP\Presenters;

use Nette;
use CP\Forms\ArticleForm;
use Nette\Application\BadRequestException;

trait FrontArticlesPresenter
{
	/** @var \CP\Services\Articles @inject */
	public $articlesService;

	private $article;

	public function actionDetail($article_id)
	{
		$article = $this->articlesService->getOnePublishedByIdLocale($article_id);

		if($article){
			$this->article = $article;
		} else
			throw new BadRequestException;
	}

	public function renderDetail()
	{
		$this->template->article = $this->article;
	}

}