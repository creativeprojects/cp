<?php

namespace CP\Forms;

use Nette,
	CP\Forms\Form;

class ArticleForm extends \CP\Forms\Base
{

	use \CP\Forms\SlugForm\SlugPart;
	protected $imagesDir;

	public function setImagesDir($imagesDir)
	{
		$this->imagesDir = $imagesDir;

		return $this;
	}

	public function create()
	{
		$this->form->addHidden("article_id");

		$this->form->addHidden("client_id");

		$this->form->addHidden("owner_table");

		$this->form->addHidden("owner_id");

		$article_title = $this->form->addContainer("article_title");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$article_title->addText($locale)
				->setRequired("Prosím zadejte titulek článku");
		}

		$article_snippet = $this->form->addContainer("article_snippet");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$article_snippet->addTextArea($locale);
		}

		$article_content = $this->form->addContainer("article_content");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$article_content->addTextArea($locale);
		}

		$this->form->addText("article_published");

		if ($this->presenter->user->isInRole("admin") || $this->presenter->user->isInRole("superAdmin")) {
			$this->form->addSelect("user_id", null, $this->presenter->articlesService->getAuthorOptions());
		} else {
			$this->form->addHidden("user_id");
		}

		$this->form->addGallery($this->presenter, "images", $this->imagesDir);

		$this->addSlugPart();

		$this->form->addSubmit("save")
			->onClick[] = array($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	public function save($button)
	{
		$article = $this->sanitizeOutputValues($button->parent->getValues(true));

		if ($this->presenter->articlesService->save($article)) {
			$this->presenter->flashMessage("Článek byl uložen", "alert-success");

			$this->presenter->restoreRequest($this->presenter->backlink);
			$this->presenter->redirect("this");
		} else {
			$this->presenter->flashMessage("Při ukládání článku došlo k chybě!", "alert-danger");
		}
	}

	protected function sanitizeOutputValues($values)
	{
		unset($values["images"]["files"]);

		return $values;
	}
}