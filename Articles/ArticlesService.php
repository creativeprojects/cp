<?php

namespace CP\Services;

use App;
use CP\Grid\Grid;
use Nette\Utils\Strings;

class Articles extends \CP\Base\Service
{
	var $settings = array(
		"imagesDir" => "files/articles",
		"slug" => array(
			"cs" => "clanky",
			"en" => "clanky"
		),
		"defaultSlug" => "clanky/novy-clanek",
	);

	protected function getGridBy($query)
	{
		$articlesGrid = $this->getGridByClient($query);

		return $articlesGrid;
	}

	public function getGridByOwner($owner_table = null, $owner_id = null)
	{
		$query = array("where" => array());

		if(isset($owner_table)){
			if($owner_table)
				$query["where"]["owner_table"] = "owner_table = '{$owner_table}'";
			else
				$query["where"]["owner_table"] = "owner_table IS NULL";
		}

		if(isset($owner_id)){
			if($owner_id)
				$query["where"]["owner_id"] = "owner_id = {$owner_id}";
			else
				$query["where"]["owner_id"] = "owner_id IS NULL";	
		}

		return $this->getGridBy($query);
	}

	public function getPublishedGridByOwner($owner_table = null, $owner_id = null)
	{
		$query = array(
			"where" => array(
				"article_published" => "article_published < NOW()"
			)
		);

		if($owner_table)
			$query["where"]["owner_table"] = "owner_table = '{$owner_table}'";

		if($owner_id)
			$query["where"]["owner_id"] = "owner_id = {$owner_id}";		

		return $this->getGridBy($query);
	}

	public function getGridByClient($query = array())
	{
		$articlesGrid = new Grid($this->database);

		$articlesGrid->query = array_extend(
			array(
			'fields' => array(
				"a.article_id",
				"a.article_published",
				$this->getSqlUserTitle(),
				"l.article_title",
				"IF(l.article_snippet IS NULL OR LENGTH(l.article_snippet) = 0, l.article_content, l.article_snippet) AS article_snippet",
				"l.article_content",
				$this->filesService->getSqlMainImageLocale($this->repository->articles, "a.article_id", "images"),
			),
			'from' => "{$this->repository->articles} AS a LEFT JOIN {$this->repository->articles->getNameLocale()} AS l ON l.article_id = a.article_id",
			'order_by' => array(
				"article_published DESC"
			),
			'where' => array(
				"client_id" => "a.client_id = " . $this->client['id'],
				"removed" => "a.removed IS NULL"
			)), $query
		);

		$articlesGrid->itemsCallback = callback($this, "sanitizeGridItemValues");

		return $articlesGrid;
	}

	protected function getSqlUserTitle($tableName = "a")
	{
		$sql = "(SELECT u.user_title FROM {$this->repository->users} AS u WHERE u.user_id = {$tableName}.user_id) AS user_title";

		return $sql;
	}

	public function sanitizeGridItemValues($values)
	{
		if ($values["image"]) {
			$image = explode(";", $values["image"]);

			$values["image"] = $this->filesService->getImageEntity(array(
				"file_src" => $image[0],
				"file_alt" => $image[1]
			));
		}

		return $values;
	}

	public function getOneById($article_id)
	{
		if($article_id){
			$by = array(
				"client_id" => $this->client["client_id"],
				"article_id" => $article_id
			);

			$article = $this->repository->articles->getOneBy($by);

			if($article){
				$article["images"] = $this->getImages($article_id);

				$article["slug"] = $this->slugsService->getOneByOwner($this->repository->articles, $article_id);
			}

			return $article; 
		}

		return false;
	}

	public function getImages($article_id)
	{
		return $this->filesService->getImagesByOwner($this->repository->articles, $article_id, 'images');
	}

	public function getOneByIdLocale($article_id)
	{
		if($article_id){
			$by = array(
				"client_id" => $this->client["client_id"],
				"article_id" => $article_id
			);

			$article = $this->getOneByLocale($by);

			return $article; 
		}

		return false;
	}

	public function getOnePublishedByIdLocale($article_id)
	{
		if($article_id){
			$by = array(
				"client_id" => $this->client["client_id"],
				"article_id" => $article_id,
				"article_published < NOW() OR " . $this->sqlUserLoggedIn()
			);

			$article = $this->getOneByLocale($by);

			return $article; 
		}

		return false;
	}

	protected function getOneByLocale($by)
	{
		$query = array(
			"where" => $by,
			"additionalFields" => array(
				$this->getSqlUserTitle($this->repository->articles)
			)
		);

		//$this->repository->articles->debug = true;

		$article = $this->repository->articles->getOneByLocale($query);

		if($article){
			$article["images"] = $this->getImagesLocale($article["article_id"]);

			return $article; 
		}

		return false;
	}

	public function getImagesLocale($article_id)
	{
		return $this->filesService->getImagesByOwnerLocale($this->repository->articles, $article_id, 'images');
	}

	public function getAuthorOptions()
	{
		$by = array(
			"client_id" => $this->client["id"],
		);

		if(!$this->isInRole("superAdmin")){
			$by["role NOT"] = array("superAdmin");
		}

		$users = $this->repository->users->getAllBy($by);

		$options = array();

		foreach($users AS $user){
			$options[$user["user_id"]] = $user["user_title"];
		}

		return $options;
	}

	public function create($article)
	{
		$article = array_extend(
			array(
				"client_id" => $this->client["id"],
				"images" => array(),
				"article_title" => array(
					"cs" => "Nový článek",
					"en" => "New Article"
				)
			),
			$article
		);

		$article["slug"] = $this->slugsService->getDefault($this->settings["defaultSlug"], $article["article_title"]);

		return $this->save($article);
	}

	public function save($article)
	{
		$article = $this->repository->articles->save($article);

		$article["images"] = $this->saveImages($article["article_id"], $article["images"]);

		$article["slug"] = $this->saveSlug($article);

		return $article;
	}

	protected function saveSlug($values)
	{
		$slug = $this->slugsService->saveByOwner($this->repository->articles, $values["article_id"])
			->setAction("Articles:detail")
			->setParameters(array("article_id" => (int)$values["article_id"]))
			->createFrom($values["article_title"], $this->settings["defaultSlug"])
			->save($values);

		return $slug;	
	}

	protected function saveImages($article_id, $images)
	{
		$images = $this->filesService->saveByOwner($images, $this->repository->articles, $article_id);

		return $images;
	}

	public function remove($article_id)
	{
		$this->repository->articles->removeOne($article_id);

		$this->removeImages($article_id);

		return true;
	}

	protected function removeImages($article_id)
	{
		return $this->filesService->removeByOwner($this->repository->articles, $article_id, 'images');
	}

	public function removeByOwner($owner_table, $owner_id)
	{
		$by = array(
			"owner_table" => $owner_table,
			"owner_id" => $owner_id
		);

		$articles = $this->repository->articles->getAllBy($by);

		foreach($articles AS $article){
			$this->remove($article["article_id"]);
		}

		return true;
	}
}