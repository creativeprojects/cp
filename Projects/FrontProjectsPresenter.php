<?php

namespace CP\Presenters;

use Nette,
	Nette\Application\BadRequestException;

trait FrontProjectsPresenter
{
	/** @var \CP\Services\Articles @inject */
	public $articlesService;

	private $projectsGrid;

	/*public function injectProjectsService(\CP\Services\Projects $projectsService)
	{
		if(!$this->projectsService instanceof \CP\Services\Projects)
			$this->projectsService = $projectsService;
	}*/

	public function actionDefault()
	{
		$this->projectsGrid = $this->projectsService->getPublishedGridByClient();

		$this->projectsGrid->paginator->itemsPerPage = 16;

		$this->projectsGrid->create($this);
	}

	public function renderDefault()
	{
		$this->template->projectsGrid = $this->projectsGrid;
	}
	
	private $project;

	private $articlesGrid;

	private $estatesGrid;

	private $brokersGrid;

	public function actionDetail($project_id)
	{
		$project = $this->projectsService->getOneByIdLocale($project_id);

		if ($project) {
			$this->project = $project;

			$this->articlesGrid = $this->articlesService->getPublishedGridByOwner($this->projectsService->repository->projects, $this->project["project_id"]);
			$this->articlesGrid->paginator->itemsPerPage = 4;
			$this->articlesGrid->create($this);

			if(isset($this->estatesService)){
				$this->estatesGrid = $this->estatesService->getActiveGridByProjectId($this->project["project_id"]);
				$this->estatesGrid->paginator->itemsPerPage = 20;
				$this->estatesGrid->create($this);

				$this->brokersGrid = $this->usersService->getBrokersGridByProjectId($this->project["project_id"]);
				$this->brokersGrid->create($this);
			}

		} else {
			throw new BadRequestException;
		}
	}

	public function renderDetail()
	{
		$this->template->project = $this->project;

		$this->template->articlesGrid = $this->articlesGrid;

		$this->template->estatesGrid = $this->estatesGrid;

		$this->template->brokersGrid = $this->brokersGrid;
	}
}