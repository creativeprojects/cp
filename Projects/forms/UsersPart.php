<?php

namespace CP\Forms\ProjectForm;

use CP\Forms\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

trait UsersPart
{
	private function createUsersPart(){
		$parent = $this;

		$container = $this->form->addDynamic("users", function (Container $container) use ($parent) {
			$container->addHidden("id");

			$container->addHidden("user_id");

			$container->addSubmit("remove")
				->setValidationScope(false)
				->onClick[] = callback($parent, "removeUser");
		});

		$container->addText('user_id');

		$container->addSubmit('add')
			->setValidationScope(false)
			->onClick[] = callback($this, "addUser");
	}

	public function addUser($button){
		$values = $button->parent->getValues();

		$values = array(
			"id" => null,
			"user_id" => $values["user_id"],
		);

		$button->parent->createOne()
			->setValues($values);

		$this->presenter->flashMessage("Makléř byl přidán", "alert-success");
	}

	public function removeUser($button){
		$users = $button->parent->parent;

		$users->remove($button->parent, TRUE);

		$this->presenter->flashMessage("Makléř byl odebrán", "alert-success");
	}
}