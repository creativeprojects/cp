<?php

namespace CP\Forms;

use Nette,
	CP\Forms\Form;

class ProjectForm extends \CP\Forms\Base
{
	use \CP\Forms\ProjectForm\UsersPart;

	use \CP\Forms\SlugForm\SlugPart;

	protected $imagesDir;

	public function setImagesDir($imagesDir)
	{
		$this->presenter->filesService->validateDir($imagesDir);
		
		$this->imagesDir = $imagesDir;

		return $this;
	}

	public function create()
	{
		$this->form->addHidden("project_id");

		$this->form->addHidden("project_rank");

		$this->form->addHidden("client_id");

		$project_title = $this->form->addContainer("project_title");

		foreach($this->presenter->translator->getAvailableLocales() AS $locale){
			$project_title->addText($locale)
				->setRequired("Prosím zadejte název projektu");
		}

		$project_description = $this->form->addContainer("project_description");

		foreach($this->presenter->translator->getAvailableLocales() AS $locale){
			$project_description->addTextArea($locale);
		}

		$this->form->addText('project_address');
		
		$this->form->addHidden('project_latitude');

		$this->form->addHidden('project_longitude');
		
		$this->form->addSelect('project_status', null, $this->presenter->projectsService->settings["statusOptions"]);

		$this->form->addText('project_established');

		$this->form->addText('project_finished');

		$this->createUsersPart();

		$this->form->addGallery($this->presenter, "images", $this->imagesDir)
			->addWatermark(TRUE);

		$this->createSlugPart();

		$this->form->addSubmit("save")
			->onClick[] = array($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	public function save($button)
	{
		$project = $this->sanitizeOutputValues($button->parent->getValues(true));

		if ($this->presenter->projectsService->save($project)) {
			$this->presenter->flashMessage("Projekt byl uložen", "alert-success");

			$this->presenter->restoreRequest($this->presenter->backlink);
			$this->presenter->redirect("Projects:default");
		} else {
			$this->presenter->flashMessage("Při ukládání projektu došlo k chybě!", "alert-danger");
		}
	}

	protected function sanitizeOutputValues($values)
	{
		unset($values["users"]["user_id"]);
		unset($values["images"]["files"]);

		return $values;
	}
}