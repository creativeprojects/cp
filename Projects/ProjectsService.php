<?php

namespace CP\Services;

use App;
use CP\Grid\Grid;
use Nette\Utils\Strings;

class Projects extends \CP\Base\Service
{
	/** @var \CP\Services\Articles @inject */
	public $articlesService;

	public $settings = array(
		"statusOptions" => array(
			"prepare" => "V přípravě",
			"progress" => "Realizuje se",
			"sale" => "V prodeji",
			"finished" => "Dokončen"
		),
		"slug" => array(
			"cs" => "projekty",
			"en" => "projects"
		),
		"default" => array(
			"project_title" => array(
				"cs" => "Nový projekt",
				"en" => "New project"
			),
		),
		"defaultSlug" => "projekty/novy-projekt"
	);

	/* public function construct()
	  {
	  $this->slugsService
	  ->addMask("Projects:default")
	  ->addMask("Projects:detail", array("project_id"));
	  } */

	public function getPublishedGridByClient($query = array())
	{
		$query = array_extend(
			array(
			"fields" => array(
				"project_description" => "l.project_description"
			),
			"where" => array(
			)
			), $query
		);

		$projectsGrid = $this->getGridByClient($query);

		return $projectsGrid;
	}

	public function getActiveGridByClient()
	{
		$query = array(
			"where" => array(
				"project_status" => "project_status IN ('prepare','progress','sale')"
			)
		);

		$projectsGrid = $this->getGridByClient($query);

		return $projectsGrid;
	}

	public function getArchiveGridByClient()
	{
		$query = array(
			"where" => array(
				"project_status" => "project_status = 'finished'"
			)
		);

		$projectsGrid = $this->getGridByClient($query);

		return $projectsGrid;
	}

	public function getGridByClient($query = array())
	{
		$projectsGrid = new Grid($this->database);

		$projectsGrid->query = array_extend(
			array(
			'fields' => array(
				"p.project_id",
				"p.project_established",
				"p.project_finished",
				"p.project_status",
				"l.project_title",
				"p.project_address",
				$this->slugsService->getSqlSlugUrlByOwnerLocale($this->repository->projects, "p.project_id"),
				$this->filesService->getSqlMainImageLocale($this->repository->projects, "p.project_id", "images"),
			),
			'from' => "{$this->repository->projects} AS p LEFT JOIN {$this->repository->projects->getNameLocale()} AS l ON l.project_id = p.project_id",
			"order_by" => "project_rank ASC",
			'where' => array(
				"client_id" => "p.client_id = " . $this->client['id'],
				"removed" => "p.removed IS NULL"
			)), $query
		);

		$projectsGrid->itemsCallback = callback($this, "sanitizeGridItemValues");

		return $projectsGrid;
	}

	public function getMenu()
	{
		$query = array(
			"where" => array(
				"client_id" => $this->client["id"],
			),
			"order" => array(
				"project_rank" => "ASC"
			),
			"additionalFields" => array(
				$this->slugsService->getSqlSlugUrlByOwnerLocale($this->repository->projects, "{$this->repository->projects}.project_id"),
				$this->slugsService->getSqlSlugLabelByOwnerLocale($this->repository->projects, "{$this->repository->projects}.project_id"),
				$this->slugsService->getSqlSlugTitleByOwnerLocale($this->repository->projects, "{$this->repository->projects}.project_id"),
			)
		);

		$menuItems = $this->repository->projects->getAllByLocale($query);

		return $menuItems;
	}

	public function sanitizeGridItemValues($values)
	{
		if ($values["image"]) {
			$image = explode(";", $values["image"]);

			$values["image"] = $this->filesService->getImageEntity(array(
				"file_src" => $image[0],
				"file_alt" => $image[1]
			));
		}

		return $values;
	}

	public function getOneById($project_id)
	{
		if ($project_id) {
			$by = array(
				"client_id" => $this->client["client_id"],
				"project_id" => $project_id
			);

			$project = $this->repository->projects->getOneBy($by);

			if ($project) {
				$project["images"] = $this->getImages($project_id);

				$project["users"] = $this->getUsersByProject($project_id);

				$project["slug"] = $this->slugsService->getOneByOwner($this->repository->projects, $project_id);
			}

			return $project;
		}

		return false;
	}

	public function getImages($project_id)
	{
		return $this->filesService->getImagesByOwner($this->repository->projects, $project_id, 'images');
	}

	public function getOneByIdLocale($project_id)
	{
		if ($project_id) {
			$by = array(
				"client_id" => $this->client["client_id"],
				"project_id" => $project_id
			);

			$project = $this->repository->projects->getOneByLocale($by);

			if ($project) {
				$project["images"] = $this->getImagesLocale($project_id);

				$project["users"] = $this->getUsersByProject($project_id);
			}

			return $project;
		}

		return false;
	}

	protected function getUsersByProject($project_id)
	{
		$users = $this->repository->projectsUsers->getAllByLocale(array("project_id" => $project_id));

		return $users;
	}

	public function getImagesLocale($project_id)
	{
		return $this->filesService->getImagesByOwnerLocale($this->repository->projects, $project_id, 'images');
	}

	public function getOptionsByClient()
	{
		$by = array(
			"client_id" => $this->client["id"]
		);

		$projects = $this->repository->projects->getAllByLocale($by);

		$options = array(
			"" => "Není součástí developerského projektu"
		);

		foreach ($projects AS $project) {
			$options[$project["project_id"]] = $project["project_title"];
		}

		return $options;
	}

	public function getUserOptionsByClient($disabled_user_id = array())
	{
		$options = array();

		$users = $this->repository->users->getAllBy(array("client_id" => $this->client["id"], "role" => "broker"));

		foreach ($users AS $user) {
			if (!in_array($user["user_id"], $disabled_user_id))
				$options[$user["user_id"]] = "{$user["user_title"]}";
		}

		return $options;
	}

	public function getUsers($user_id)
	{
		if (class_exists("Repository\\usersBroker")) {
			$query = array(
				"where" => array(
					"client_id" => $this->client["id"],
					"user_id" => $user_id
				),
				"additionalFields" => array(
					"(SELECT broker_phone FROM {$this->repository->usersBroker} AS b WHERE b.user_id = {$this->repository->users}.user_id) AS user_phone",
					$this->filesService->getSqlMainImageLocale($this->repository->usersBroker, "{$this->repository->users}.user_id", "portrait", "portrait")
				)
			);

			$users = $this->repository->users->getAllByLocale($query);

			foreach ($users AS & $user) {
				$user["portrait"] = $this->filesService->parseImageEntity($user["portrait"]);
			}

			return $users;
		}

		return null;
	}

	public function create()
	{
		$project = array_extend(
			$this->settings["default"], array(
			"project_status" => "prepare",
			"client_id" => $this->client["id"],
			"images" => array(),
			"users" => array(),
			)
		);

		$project["slug"] = $this->slugsService->getDefault($this->settings["defaultSlug"], $project["project_title"]);

		return $this->save($project);
	}

	public function save($project)
	{
		$project = $this->sanitizeInputValues($project);

		$project = $this->repository->projects->save($project);

		$project["users"] = $this->saveUsers($project["project_id"], $project["users"]);

		$project["images"] = $this->saveImages($project["project_id"], $project["images"]);

		$project["slug"] = $this->saveSlug($project);

		return $project;
	}

	protected function sanitizeInputValues($project)
	{
		if (@!$project["project_rank"]) {
			$by = array(
				"client_id" => $this->client["id"],
			);

			$maxRank = $this->repository->projects->getMaxBy("project_rank", $by);

			$project["project_rank"] = $maxRank + 1;
		}

		return $project;
	}

	private function saveSlug($values)
	{
		$slug = $this->slugsService->saveByOwner($this->repository->projects, $values["project_id"])
			->setAction("Projects:detail")
			->setParameters(array("project_id" => (int)$values["project_id"]))
			->createFrom($values["project_title"], $this->settings["defaultSlug"])
			->save($values);

		return $slug;
	}

	private function saveUsers($project_id, $users)
	{
		$savedIds = array();

		foreach ($users AS & $user) {
			$user["project_id"] = $project_id;

			$user = $this->repository->projectsUsers->save($user);

			$savedIds[] = $user["id"];
		}

		$removeBy = array(
			"project_id" => $project_id,
			"id NOT" => $savedIds
		);

		$this->repository->projectsUsers->removeBy($removeBy);

		return $users;
	}

	public function saveImages($project_id, $images)
	{
		$images = $this->filesService->saveByOwner($images, $this->repository->projects, $project_id);

		return $images;
	}

	public function remove($project_id)
	{
		$this->repository->projects->removeOne($project_id);

		$this->removeImages($project_id);

		$this->repository->projectsUsers->removeBy(array("project_id" => $project_id));

		$this->articlesService->removeByOwner($this->repository->projects, $project_id);

		return true;
	}

	protected function removeImages($project_id)
	{
		return $this->filesService->removeByOwner($this->repository->projects, $project_id, 'images');
	}

	public function move($project_id, $value)
	{
		$source = $this->repository->projects->getOne($project_id);

		if ($source && $source["project_rank"]) {
			$destinationQuery = array(
				"where" => array(
					"client_id" => $this->client["id"],
					"project_rank " . ($value > 0 ? " > " : " < ") . "{$source["project_rank"]}"
				),
				"order" => array(
					"project_rank" => $value > 0 ? "ASC" : "DESC"
				)
			);

			$destination = $this->repository->projects->getOneByLocale($destinationQuery);

			if ($destination) {

				$this->repository->projects->saveField($source["project_id"], "project_rank", $destination["project_rank"]);

				$this->repository->projects->saveField($destination["project_id"], "project_rank", $source["project_rank"]);
			}
		}
	}
}