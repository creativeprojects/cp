<?php

namespace CP\Presenters;

use Nette;
use CP\Forms\ProjectForm;
use Nette\Application\BadRequestException;

trait AdminProjectsPresenter
{
	/** @var \CP\Services\Projects @inject */
	public $projectsService;

	private $projectsGrid;

	public function actionDefault()
	{
		if ($this->user->isAllowed("Admin:Projects", "default")) {
			$this->projectsGrid = $this->projectsService->getGridByClient();

			$this->projectsGrid->paginator->itemsPerPage = 20;

			$this->projectsGrid->create($this);

			$this->template->projectsGrid = $this->projectsGrid;
		} else {
			throw new BadRequestException;
		}
	}
	private $project;

	public function handleAdd()
	{
		$project = $this->projectsService->create();

		$this->redirect("Projects:add", $project["project_id"]);
	}

	public function actionAdd($project_id)
	{
		$this->actionEdit($project_id);
	}

	public function actionEdit($project_id)
	{
		$project = $this->projectsService->getOneById($project_id);

		if ($project) {
			$this->project = $project;

			$this->template->project = $this->project;
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentProjectForm()
	{
		$form = new ProjectForm($this);

		$form->setImagesDir("{$this->client["dir"]}/files/projects");

		$form->setValues($this->project);

		return $form->create();
	}

	public function handleRemove($project_id)
	{
		if ($this->projectsService->remove($project_id)) {
			$this->flashMessage("Projekt byl odebrán", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Projects:default");
		} else
			$this->flashMessage("Při odebírání projektu došlo k chybě!", "alert-danger");
	}
	/** @var \CP\Services\Articles @inject */
	public $articlesService;

	private $articlesGrid;

	public function actionArticles($project_id)
	{
		$project = $this->projectsService->getOneByIdLocale($project_id);

		if ($this->user->isAllowed("Admin:Projects", "articles") && $project) {
			$this->project = $project;

			$this->template->project = $this->project;

			$this->articlesGrid = $this->articlesService->getGridByOwner($this->projectsService->repository->projects, $project_id);

			$this->articlesGrid->paginator->itemsPerPage = 20;

			$this->articlesGrid->create($this);

			$this->template->articlesGrid = $this->articlesGrid;
		} else {
			throw new BadRequestException;
		}
	}

	public function loadFormProperties($form)
	{
		$user_ids = array();

		foreach ($form["users"]->components AS $key => $user)
			if (is_numeric($key))
				$user_ids[] = $user->components["user_id"]->value;

		$form->properties["userOptions"] = $this->projectsService->getUserOptionsByClient($user_ids);

		$form->properties["users"] = $this->projectsService->getUsers($user_ids);
	}

	public function handleMoveUp($project_id)
	{
		$project = $this->projectsService->getOneById($project_id);

		if ($project) {
			$this->projectsService->move($project["project_id"], -1);

			$this->flashMessage("Projekt byl v seznamu posunut nahoru", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("this");
		} else
			throw new BadRequestException;
	}

	public function handleMoveDown($project_id)
	{
		$project = $this->projectsService->getOneById($project_id);

		if ($project) {
			$this->projectsService->move($project["project_id"], 1);

			$this->flashMessage("Projekt byl v seznamu posunut dolů", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("this");
		} else
			throw new BadRequestException;
	}
}