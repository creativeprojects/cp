<?php

namespace Repository;

class projectsUsers extends \Repository\BaseRepository
{
	public $name = "projects_brokers";

	public $fields = array(
		"id" => "primary",
		"project_id" => "int",
		"user_id" => "int",
	);

	public $validateTable = true;
}