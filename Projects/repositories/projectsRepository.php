<?php

namespace Repository;

class projects extends \Repository\BaseRepository
{
	public $name = "projects";

	public $fields = array(
		"project_id" => "primary",
		"client_id" => "int",
		"project_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"project_description" => array(
			"type" => "html",
			"locale" => true
		),
		"project_address" => "varchar",
		"project_latitude" => "double",
		"project_longitude" => "double",
		"project_established" => "date",
		"project_finished" => "date",
		"project_status" => array(
			"type" => "varchar",
			"notNull" => true,
			"default" => "new"
		),
		"project_rank" => "tinyint"
	);

	public $validateTable = true;
}