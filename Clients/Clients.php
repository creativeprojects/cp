<?php

namespace CP\Services;

use Nette,
	CP;

class Clients extends CP\Service
{
	//public $client;

	public function getById($client_id)
	{
		return $this->repository->clients->getOneLocale($client_id);
	}

	public function getClient()
	{
		if(!$this->client){
			$this->client = array_extend(
				$this->settings, (array)$this->getById($this->settings["id"])
			);

			$this->client["domain"] = $_SERVER["SERVER_NAME"];
		}

		return $this->client;
	}

	public function componentAllowed($component)
	{
		$allowed = in_array($component, $this->client["components"]) || $this->user["role"] == "superAdmin";

		return $allowed;
	}
}