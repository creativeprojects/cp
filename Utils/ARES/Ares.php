<?php

namespace CP\Utils;

class ARES
{
	private $paths = array(
		'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_rzp.cgi?ico=%ic',
		'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=%ic',
	);

	private $xml;

	private $ic;

	public function __construct($ic)
	{
		$this->ic = $ic;

		$this->getXML();
	}

	private function getXML()
	{
		foreach($this->paths AS $path){
			$path = str_replace("%ic", $this->ic, $path);

			$this->xml .= file_get_contents($path);
		}

		$this->xml = preg_replace("/<([a-z]\:[a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $this->xml);
	}

	private function getField($key)
	{
		$pattern = '/<'.$key.'>(.*)<\/'.$key.'>/';
		preg_match($pattern, $this->xml, $matches);

		return isset($matches[1]) ? $matches[1] : false;
	}

	public function getName()
	{
		return $this->getField("D:OF");
	}

	public function getIC()
	{
		return $this->getField("D:ICO");
	}

	public function getICO()
	{
		return $this->getIC();
	}

	public function getDIC()
	{
		return $this->getField("D:DIC");
	}

	public function getCity()
	{
		return $this->getField("D:N");
	}

	public function getDistrict()
	{
		return $this->getField("D:NCO");
	}

	public function getStreet()
	{
		return $this->getField("D:NU");
	}

	public function getCP()
	{
		$no = array(
			$this->getField("D:CD"),
			$this->getField("D:CO"),
		);

		return implode("/", array_filter($no));
	}

	public function getCO()
	{
		return $this->getField("D:CO");
	}

	public function getPostCode()
	{
		return $this->getField("D:PSC");
	}

	public function getPSC()
	{
		return $this->getPostCode();
	}
}