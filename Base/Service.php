<?php

namespace CP\Base;

use Nette,
	CP;

class Service extends CP\Service
{
	public $email;

	public $notificationsService;

	public $filesService;

	public $slugsService;

	public function __construct(
		Nette\Database\Context $database, 
		Nette\Security\IUserStorage $user, 
		Nette\Localization\ITranslator $translator, 
		CP\Emails\Service $emailsService, 
		CP\Notifications\Service $notificationsService, 
		CP\Files\Service $filesService, 
		CP\Services\Clients $clientsService, 
		CP\Services\Slugs $slugsService, 
		CP\Regions\Service $regionsService
	)
	{
		parent::__construct($database, $user, $translator);

		$this->emailsService = $emailsService;

		$this->notificationsService = $notificationsService;

		$this->filesService = $filesService;

		$this->slugsService = $slugsService;

		$this->regionsService = $regionsService;

		$this->serviceClient($clientsService);

		$this->construct();
	}

	protected function construct()
	{

	}

	protected function serviceClient($clientsService)
	{
		parent::serviceClient($clientsService);

		$this->emailsService->client = $this->client;
		$this->notificationsService->client = $this->client;
		$this->filesService->client = $this->client;
		$this->slugsService->client = $this->client;
	}
}