<?php

namespace CP\Base;

use Nette,
	CP,
	Nette\Application\BadRequestException;

abstract class Presenter extends Nette\Application\UI\Presenter
{
	/** @persistent */
    public $backlink = '';

    public $macros = array();

    //Testování funkčnosti snippetů - nefungují pokud includuju šablonu s bloky
    /*public function handleTest()
	{
		if($this->isAjax()){
			$this->invalidateControl();
		}
	}*/

	public function startup()
	{
		parent::startup();

		CP\Helpers\Helpers::register($this);

		$this->localeStartup();

		if (explode(':', $this->getPresenter()->getName())[0] != 'Sale') {
			$this->client = $this->clientsService->getClient();
		}

		if (!is_localhost() && !empty($this->client['client_domain']) && $this->client['client_domain'] != $_SERVER['SERVER_NAME']) {
			$this->redirectUrl('http://' . $this->client['client_domain'] . $_SERVER['REQUEST_URI'], 301);
		}

		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	}

	public function beforeRender()
	{
		parent::beforeRender();
		
		$this->template->multiLocale = $this->multiLocale;

		$this->template->locale = $this->translator->getLocale();

		$this->template->currency = $this->regionsService->getCurrency();

		$this->template->domain = $_SERVER["SERVER_NAME"];

		$this->template->currentSlug = $this->getCurrentSlug();

		$this->template->client = $this->client;

		$this->template->instancePath = $this->template->basePath . \instance::getRelativeDir();

		$this->template->cpDir = CP_DIR;
	}
	
	/************************************************* Security *********************************************************** */
	
	/** @var \CP\Users\Service @inject */
	public $usersService;

	public function isUserAllowed()
	{
		$user = $this->getUser();

		if (!$user->isLoggedIn() AND ! $user->isAllowed($this->name, $this->action)) {
			if ($user->getLogoutReason() === Nette\Http\UserStorage::INACTIVITY) {
				$this->flashMessage('Z důvodu příliš dlouhé neaktivity jste byli odhlášeni', 'alert-warning');

				$this->redirect($this->usersService->settings['loginPage'], array('backlink' => $this->storeRequest()));
			}

			$this->redirect($this->usersService->settings['loginPage']);
		} else {
			if (!$user->isAllowed($this->name, $this->action)) {
				$this->flashMessage('Nemáte dostatečné oprávnění!', 'alert-danger');

				$this->redirect($this->usersService->settings['loginPage']);
			}
		}
	}
	
	/************************************************* Webloader *********************************************************** */
	
	/** @var \WebLoader\Nette\LoaderFactory @inject */
	public $webLoader;

	/************************************************* Translator *********************************************************** */

	/** @var \CP\Regions\Service @inject */
	public $regionsService;

	/** @var $locale */
	public $locale;

	/** @var $defaultLocale */
	public $defaultLocale;

	/** @var $multiLocale */
	public $multiLocale;

	/** @var \Kdyby\Translation\Translator @inject */
	public $translator;

	/** @var \Kdyby\Translation\LocaleResolver\SessionResolver @inject */
	public $translatorSession;

	private function localeStartup()
	{
		$this->multiLocale = count($this->translator->getAvailableLocales()) > 1 ? true : false;
			
		$fallbackLocales = $this->translator->getFallbackLocales();
			
		$this->defaultLocale = $fallbackLocales[0];
	}

	public function handleSetLocale($locale)
	{
		$this->translatorSession->setLocale($locale);

		$httpRequest = $this->getRequest();

		$presenter = $httpRequest->getPresenterName();

		$parameters = $httpRequest->getParameters();

		$action = $parameters["action"];

		unset($parameters["action"]);

		unset($parameters["do"]);

		unset($parameters["locale"]);

		$this->redirect(":{$presenter}:{$action}", $parameters);
	}
	
	/************************************************* Templates *********************************************************** */

	public function flashMessage($message, $type = "info")
	{
		if ($this->translator)
			$message = $this->translator->translate($message);

		parent::flashMessage($message, $type);
	}
	
	/************************************************* Notifications *********************************************************** */
	
	/** @var \CP\Notifications\Service @inject */
	public $notificationsService;

	public function handleNotificationClickThru($notification_id)
	{
		if ($notification = $this->notificationsService->getById($notification_id)) {
			if (!$notification->notification_persist)
				$this->notificationsService->saveDone();

			$notification_url = (filter_var($notification->notification_url, FILTER_VALIDATE_URL) === FALSE ? $this->getHttpRequest()->getUrl()->scriptPath : null) . $notification->notification_url;

			$this->redirectUrl($notification_url);
		} else {
			throw new BadRequestException;
		}
	}
	
	/** @var \CP\Emails\Service @inject */
	public $emailsService;

	/************************************************* Slugs *********************************************************** */
	
	/** @var \CP\Services\Slugs @inject */
	public $slugsService;

	protected function getCurrentSlug()
	{
		$currentSlug = array();

		if(isset($this->request->getParameters()["slug"]))
			$currentSlug = array_extend($currentSlug, $this->request->getParameters()["slug"]);

		//dump($currentSlug);

		return $currentSlug;
	}

	private $menu;

	public function getMenu($menu)
	{
		if (!isset($this->menu[$menu]))
			$this->menu[$menu] = $this->slugsService->getMenu($menu);

		return $this->menu[$menu];
	}

	/************************************************* Files *********************************************************** */

	/** @var \CP\Files\Service @inject */
	public $filesService;

	/************************************************* Clients *********************************************************** */

	/** @var \CP\Services\Clients @inject */
	public $clientsService;
	
	public $client;

	public function isClients($info)
	{
		if (is_array($info) && $info['client_id'] == $this->client['id']) {
			return TRUE;
		} elseif (is_object($info) && $info->client_id == $this->client['id']) {
			return TRUE;
		}
		throw new BadRequestException();
		return FALSE;
	}

	public function componentAllowed($component)
	{
		return $this->clientsService->componentAllowed($component);
	}

}