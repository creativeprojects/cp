<?php

namespace Repository;

class addressBookContacts extends \Repository\BaseRepository
{
	public $name = "addressbook_contacts";

	public $fields = array(
		"contact_id" => "primary",
		"client_id" => "int",
		"contact_name" => "varchar",
		"contact_ic" => "varchar",
		"contact_dic" => "varchar",
		"contact_address_street" => "varchar",
		"contact_address_no" => "varchar",
		"contact_address_city" => "varchar",
		"contact_address_postcode" => "varchar",
		"contact_address_country" => "varchar",
		"contact_email" => "varchar",
		"contact_phone" => "varchar",
	);

	protected $validateTable = true;

}