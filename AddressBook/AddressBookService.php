<?php

namespace CP\Services;

use CP\Grid\Grid;

class AddressBook extends \CP\Base\Service
{
	public function getGridByUser($query = array())
	{
		$contactsGrid = new Grid($this->database);

		$contactsGrid->query = array_extend(array(
			'fields' => array(
				'*',
			),
			'from' => "{$this->repository->addressBookContacts}",
			'where' => array(
				"removed IS NULL",
				"created_user_id = {$this->user["user_id"]}"
			)
		), $query);

		//$contactsGrid->itemsCallback = callback($this, "gridItemCallback");

		return $contactsGrid;
	}

	public function getGridByClient($query = array())
	{
		$contactsGrid = new Grid($this->database);

		$contactsGrid->query = array_extend(array(
			'fields' => array(
				'*',
			),
			'from' => "{$this->repository->addressBookContacts}",
			'where' => array(
				"removed IS NULL",
				"client_id = " . $this->client['id']
			)
		), $query);

		//$contactsGrid->itemsCallback = callback($this, "gridItemCallback");

		return $contactsGrid;
	}

	public function getOneById($contact_id)
	{
		if($contact_id){
			$by = array(
				"contact_id" => $contact_id,
			);

			$by["client_id"] = $this->client["id"];

			$contact = $this->repository->addressBookContacts->getOneBy($by);

			return $contact;
		}
	}

	public function create()
	{
		$contact = array(
			"client_id" => $this->client["id"]
		);

		$contact = $this->save($contact);

		return $contact;
	}

	public function save($contact)
	{
		$contact = $this->repository->addressBookContacts->save($contact);

		return $contact;
	}

	public function remove($contact_id)
	{
		$contact = $this->getOneById($contact_id);

		if($contact)
			return $this->repository->addressBookContacts->removeOne($contact["contact_id"]);

		return false;
	}

	public function getSuggestions($field, $value)
	{
		$contacts = $this->database->query("SELECT * FROM {$this->repository->addressBookContacts} WHERE {$field} LIKE ('%{$value}%')")->fetchAll();

		$suggestions = array();

		//Pozor pro jquery.autocomplete musí pole suggestions mit klice poporade od 0

		foreach($contacts AS $contact){
			$suggestions[] = array(
				"value" => implode(", ", array_filter(array($contact["contact_name"], $contact["contact_email"], $contact["contact_phone"]))),
				"data" => $contact
			);
		}

		return $suggestions;
	}

	public function getFromARES($ic)
	{
		$ares = new \CP\Utils\ARES($ic);

		$contact = array(
			"contact_name" => $ares->getName(),
			"contact_ic" => $ares->getIC(),
			"contact_dic" => $ares->getDIC(),
			"contact_address_street" => $ares->getStreet(),
			"contact_address_no" => $ares->getCP(),
			"contact_address_postcode" => $ares->getPostCode(),
			"contact_address_city" => $ares->getCity(),
			"contact_address_country" => "CZ",
		);

		return $contact;
	}
}