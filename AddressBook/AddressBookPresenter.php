<?php

namespace CP\Presenters;

use Nette;
use CP\Forms\AddressBook\ContactForm;
use Nette\Application\BadRequestException;

trait AddressBookPresenter
{
	/** @var \CP\Services\AddressBook @inject */
	public $addressBookService;

	public function startup()
	{
		parent::startup();

		$this->isUserAllowed();
	}
	public $contactsGrid;

	public function actionDefault()
	{
		switch ($this->user->getIdentity()->data["role"]) {
			case "seller":
			case "broker":
				$this->contactsGrid = $this->addressBookService->getGridByUser();
				break;
			case 'admin':
			case 'superAdmin':
				$this->contactsGrid = $this->addressBookService->getGridByClient();
				break;
		}
	}

	public function renderDefault()
	{
		if ($this->contactsGrid) {
			$this->contactsGrid->paginator->itemsPerPage = 20;

			$this->contactsGrid->create($this);

			$this->template->contactsGrid = $this->contactsGrid;
		} else
			throw new BadRequestException;
	}
	public $contact;

	public function actionEdit($contact_id)
	{
		if ($this->contact = $this->addressBookService->getOneById($contact_id)) {
			$this->template->contact = $this->contact;
		} else
			throw new BadRequestException;
	}

	public function handleAdd()
	{
		$contact = $this->addressBookService->create();

		$this->redirect('AddressBook:add', $contact["contact_id"]);
	}

	public function actionAdd($contact_id)
	{
		$this->actionEdit($contact_id);
	}

	protected function createComponentContactForm()
	{
		$form = new ContactForm($this);

		$form->setValues($this->contact);

		return $form->create();
	}

	public function handleRemove($contact_id)
	{
		if ($this->addressBookService->remove($contact_id)) {
			$this->flashMessage("Kontakt byl odebrán", "alert-success");
		} else {
			$this->flashMessage("Kontakt se nepodařilo odebrat!", "alert-danger");
		}

		$this->redirect("AddressBook:default");
	}

	public function actionGetSuggestions($field, $query)
	{
		$this->payload->suggestions = $this->addressBookService->getSuggestions($field, $query);

		$this->sendPayload();
	}

	public function actionGetFromAres($ic)
	{
		$this->payload->contact = $this->addressBookService->getFromARES($ic);

		$this->sendPayload();
	}
}