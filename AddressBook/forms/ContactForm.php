<?php

namespace CP\Forms\AddressBook;

use Nette,
	CP,
	CP\Forms\Form;

class ContactForm extends \CP\Forms\Base
{

	public function create()
	{
		$this->form->addHidden("contact_id");

		$this->form->addHidden("client_id");

		$this->form->addText("contact_name");

		$this->form->addText("contact_ic");

		$this->form->addText("contact_dic");

		$this->form->addText("contact_phone");

		$this->form->addText("contact_email")
			->addCondition(Form::FILLED)
			->addRule(Form::EMAIL, "Zadjte platnou emailovou adresu");

		$this->form->addText("contact_address_street");

		$this->form->addText("contact_address_no");

		$this->form->addText("contact_address_postcode");

		$this->form->addText("contact_address_city");

		if (@$this->presenter->client["selectCountry"]) {
			$this->form->addSelect("contact_address_country", null, $this->presenter->regionsService->getCountries())
				->setDefaultValue(@$this->presenter->client["defaultCountry"]);
		} else {
			$this->form->addHidden("contact_address_country");
		}

		$this->form->addSubmit("save")
			->onClick[] = callback($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($values = $this->presenter->addressBookService->save($values)) {
			$this->presenter->flashMessage("Kontakt byl uložen", "alert-success");

			$this->presenter->restoreRequest($this->presenter->backlink);
			$this->presenter->redirect("AddressBook:default");
		} else {
			$this->presenter->flashMessage("Při ukládání kontaktu došlo k chybě!", "alert-danger");
		}
	}
}