<?php

namespace CP\Files;

use Nette;
use CP;

class Presenter extends CP\Base\Presenter
{
	/** @var \CP\Files\Service @inject */
	public $filesService;

	public function actionCreateThb()
	{
		$result = $this->filesService->createThb($_POST);

		if ($this->isAjax()) {
			$this->payload->result = $result;

			$this->sendPayload();
		} else {
			$this->redirect("this");
		}
	}

	public function actionRemoveUnused()
	{
		$log = array("Pozastaveno"); //$this->filesService->removeUnused();

		$this->template->log = $log;
	}
}