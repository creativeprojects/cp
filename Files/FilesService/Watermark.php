<?php

namespace CP\Services\Files;

use Nette\Utils\Image;

trait Watermark
{
	public function addWatermark($image)
	{
		if (is_array($this->settings["watermark"])) {
			$img = Image::fromFile(WWW_DIR . "/" . $image);
			$img = $this->createWatermark($img);
			$img->save(WWW_DIR . "/" . $image);
		}

		return $image;
	}

	public function createWatermark($img)
	{
		$imgWidth = $img->width;
		$imgHeight = $img->height;

		$watermark = Image::fromFile(WWW_DIR . "/" . $this->settings["watermark"]["dir"]);
		$watermarkWidth = min($this->settings["watermark"]["width"], $imgWidth) - (2 * $this->settings["watermark"]["offset"]["x"]);
		$watermarkHeight = min($this->settings["watermark"]["height"], $imgHeight) - (2 * $this->settings["watermark"]["offset"]["y"]);
		$watermark->resize($watermarkWidth, $watermarkHeight);

		if ($this->settings["watermark"]["horizontalPosition"] == "right")
			$horizontal = $imgWidth - $watermarkWidth - $this->settings["watermark"]["offset"]["x"];
		elseif ($this->settings["watermark"]["horizontalPosition"] == "left")
			$horizontal = 0 + $this->settings["watermark"]["offset"]["x"];
		elseif ($this->settings["watermark"]["horizontalPosition"] == "center")
			$horizontal = $imgWidth / 2 - $watermarkWidth / 2;

		if ($this->settings["watermark"]["verticalPosition"] == "bottom")
			$vertical = $imgHeight - $watermarkHeight - $this->settings["watermark"]["offset"]["y"];
		elseif ($this->settings["watermark"]["verticalPosition"] == "top")
			$vertical = 0 + $this->settings["watermark"]["offset"]["y"];
		elseif ($this->settings["watermark"]["verticalPosition"] == "center")
			$vertical = $imgHeight / 2 - $watermarkHeight / 2;

		//TODO - vyřešit vkládání PNG s opacitou
		//$watermark->alphablending(true);
		//$watermark->savealpha(true);

		$img->place($watermark, $horizontal, $vertical, $this->settings["watermark"]["opacity"]);

		return $img;
	}
}