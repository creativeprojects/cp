<?php

namespace CP\Services\Files;

trait CRUD
{

	public function getUploadedFileName($path, $fileName)
	{
		$pathInfo = pathinfo($fileName);

		$i = 0;

		do {
			$name = time() . ($i ? "_{$i}" : null) . (array_key_exists("extension", $pathInfo) ? ".{$pathInfo['extension']}" : null);

			$i++;
		} while (file_exists(WWW_DIR . "/" . $path . "/" . $name));

		$path = $path . "/" . $name;

		return $path;
	}

	public function uploadFromRemote($url, $path)
	{
		$filePath = $this->getUploadedFileName($path, $url);

		$result = copy($url, WWW_DIR . "/" . $filePath);

		if ($result)
			return $filePath;
		else
			return false;
	}

	private function uploadFile($file, $path)
	{
		$pathInfo = pathInfo($file);

		$fileName = $file->getName();

		if(!isset($pathInfo["extension"]) && $this->isImage($file->getTemporaryFile())){
			$fileName .= "." . $this->getExtensionByMimeType($file->getContentType());
		}

		$filePath = WWW_DIR . "/" . $fileName;

		$filePath = $this->getUploadedFileName($path, $fileName);

		$file->move(WWW_DIR . "/" . $filePath);

		return $filePath;
	}

	public function upload($files, $path)
	{
		if (!is_array($files))
			$files = array($files);

		$filePath = array();

		foreach ($files AS $file) {
			$filePath[] = $this->uploadFile($file, $path);
		}

		return $filePath;
	}

	public function saveByOwner($files, $owner_table, $owner_id, $file_rel = null, $client_id = false)
	{
		if (is_array($files)) {
			if (count($files) && !is_numeric(key($files)))
				$files = array($files);

			$savedIds = array();

			$client_id = $client_id !== false ? $client_id : $this->client["id"];

			foreach ($files AS & $file) {
				if (!empty($file)) {
					$file["owner_table"] = $owner_table;
					$file["owner_id"] = $owner_id;
					$file["client_id"] = $client_id;
					$file["file_rel"] = $file_rel ? $file_rel : $file["file_rel"];

					//TODO - opravovat řazení souborů file_rank

					$file = $this->repository->files->save($file);

					$savedIds[] = $file["file_id"];
				}
			}

			$by = array(
				"owner_table" => $owner_table,
				"owner_id" => $owner_id,
				"client_id" => $client_id
			);

			if ($file_rel)
				$by["file_rel"] = $file_rel;

			if (count($savedIds))
				$by[] = "file_id NOT IN (" . implode(",", $savedIds) . ")";

			$this->repository->files->removeBy($by);

			return $files;
		}

		return true;
	}

	public function setMainFile($fileId, $ownerTable, $ownerId)
	{
		return $this->database->table($this->repository->files->getName())->where(array(
				'file_id' => $fileId,
				'owner_table' => $ownerTable,
				'owner_id' => $ownerId
			))->update(array(
				'file_main' => 1
		));
	}

	public function getMaxRank($ownerTable, $ownerId)
	{
		$row = $this->database->table($this->repository->files->getName())->where(array(
				'owner_table' => $ownerTable,
				'owner_id' => $ownerId
			))->max('file_rank');
		if ($row) {
			return $row;
		}
		return 0;
	}

	public function removeByOwner($owner_table, $owner_id, $file_rel = null)
	{
		$by = array(
			"client_id" => $this->client["id"],
			"owner_table" => (string)$owner_table,
			"owner_id" => $owner_id
		);

		if ($file_rel)
			$by["file_rel"] = $file_rel;

		return $this->repository->files->removeBy($by);
	}

	public function removeUnused()
	{
		$filesInDb = $this->database
			->table($this->repository->files->getName())
			->fetchPairs('file_id', 'file_src');

		$folders = array();
		$filesInDbToRemove = array();
		$filesToRemove = array();

		foreach ($filesInDb as $file_id => $file) {
			if ($file) {
				$pathinfo = pathinfo($file);
				$dirname = $pathinfo['dirname'];

				if (is_dir($dirname) && !in_array($dirname, $folders, TRUE)) {
					array_push($folders, $dirname);
				}

				if (!file_exists($file))
					$filesInDbToRemove[] = $file_id;
			} else {
				$filesInDbToRemove[] = $file_id;
			}
		}

		if (count($filesInDbToRemove)) {
			$by = array(
				"file_id" => $filesInDbToRemove
			);

			$this->repository->files->deleteBy($by);
		}

		foreach ($folders as $folder) {
			$files = scandir($folder);
			foreach ($files as $file) {
				if (is_file($folder . '/' . $file) && strpos($folder, "/files/") !== false && !in_array($folder . '/' . $file, $filesInDb)) {
					try {
						unlink(WWW_DIR . "/" . $folder . '/' . $file);
						$filesToRemove[] = $folder . '/' . $file;
					} catch (Exception $e) {
						die("I could not delete file");
					}
				}
			}
		}

		return array(
			"filesInDb" => $filesInDb,
			"filesInDbToRemove" => $filesInDbToRemove,
			"filesToRemove" => $filesToRemove
		);
	}

	public function deleteFile($file_id)
	{
		$result = $this->repository->files->deleteOne($file_id);

		return $result;
	}

	public function validateDir($path, $fullPath = false, $backTrace = array())
	{
		$info = pathinfo($path);

		$dir = ($fullPath ? null : WWW_DIR . "/");

		if (!is_dir($dir . $info["dirname"])) {
			array_push($backTrace, $info["basename"]);

			$this->validateDir($info["dirname"], $fullPath, $backTrace);
		}

		if (!is_dir($dir . $path)) {
			mkdir($dir . $path, 0777);

			foreach ($backTrace AS $dirname) {
				mkdir($dir . $path . "/$dirname", 0777);

				$dir .= "$dirname/";
			}
		}

		return $path;
	}
}