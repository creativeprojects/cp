<?php

namespace CP\Services\Files;

use Nette\Utils\Image;

trait ResizeImages
{
	public function resizeImage($file_src, $width = NULL, $height = NULL, $quality = NULL, $resize = "FIT")
	{
		if($this->isImage($file_src)){
			if (!$width)
				$width = $this->settings['image']['maxWidth'];
			if (!$height)
				$height = $this->settings['image']['maxHeight'];
			if (!$quality)
				$quality = $this->settings['image']['quality'];

			switch (strtoupper($resize)) {
				case "FILL": $resize = Image::FIT; break;
				case "EXACT": $resize = Image::EXACT; break;
				case "SHRINK_ONLY": $resize = Image::SHRINK_ONLY; break;
				case "STRETCH": $resize = Image::STRETCH; break;
				default: $resize = Image::FIT; break;
			}

			$img = Image::fromFile(WWW_DIR . "/" . $file_src);

			$this->fixOrientation(WWW_DIR . "/" . $file_src, $img);

			$img->resize($width, $height, $resize);
			$img->save(WWW_DIR . "/" . $file_src, $quality);
		}

		return $file_src;
	}

	public function resizeImageByRel($file_src, $rel)
	{
		$props = $this->settings["image"];
		if (array_key_exists($rel, $this->settings["rel"])) {
			$props = array_extend($props, $this->settings["rel"][$rel]["image"]);
		}

		return $this->resizeImage($file_src, $props["maxWidth"], $props["maxHeight"], $props["quality"], $props["resize"]);
	}

	public function fixOrientation($filePath, $image = null)
	{
		if(exif_imagetype($filePath) == IMAGETYPE_JPEG){
			if($ex = @exif_read_data($filePath, 'EXIF')){
			
				if($image){
					$this->_fixOrientation($ex, $image);
				} else {
					$image = Image::fromFile($filePath);

					$this->_fixOrientation($ex, $image);
				
					$img->save($filePath);
				}
			}
		}
	}

	protected function _fixOrientation($ex, $image)
	{
		if(!empty($ex['Orientation'])) {
			switch($ex['Orientation']) {
				case 8:
					$image->rotate(90, 0);
					break;
				case 3:
					$image->rotate(180, 0);
					break;
				case 6:
					$image->rotate(-90, 0);
					break;
			}
		}
	}
}