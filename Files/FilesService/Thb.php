<?php

namespace CP\Services\Files;

use Nette\Utils\Image;
use Nette\Utils\Strings;

trait Thb
{
	/*	 * ************************************************************************************************************** */
	/* Thb */
	/*	 * ************************************************************************************************************** */

	protected function getThbSettings($rel = null)
	{
		$settings = array_extend($this->settings["image"], $this->settings["thb"]);

		if (@array_key_exists($rel, $this->settings["rel"]))
			$settings = array_extend($settings, $this->settings["rel"][$rel]["thb"]);

		return $settings;
	}

	public function createThb($record)
	{
		$defaults = $this->getThbSettings(@$record["rel"]);

		$record["thb"]["ratio"] = @$record["thb"]["ratio"] > 0 ? $record["thb"]["ratio"] : $defaults["ratio"];
		$record["thb"]["quality"] = @$record["thb"]["quality"] ? @$record["thb"]["quality"] : $defaults["quality"];
		$record["thb"]["resizeType"] = @$record["thb"]["resizeType"] ? $record["thb"]["resizeType"] : $defaults["resizeType"];
		$record["thb"]["backgroundColor"] = @$record["thb"]["backgroundColor"] ? $record["thb"]["backgroundColor"] : $defaults["backgroundColor"];

		$record["thb"]["width"] = round($record["thb"]["width"]);
		$record["thb"]["height"] = round($record["thb"]["width"] / $record["thb"]["ratio"]);
		$record["thb"]["src"] = $this->getThbPath($record["src"], $record["thb"]);

		$img = Image::fromFile(WWW_DIR . "/" . $record["src"]);

		switch ($record["thb"]["resizeType"]) {
			default:
				$img->resize($record["thb"]["width"], $record["thb"]["height"], Image::EXACT);

				break;

			case "FIT":
				$background = Image::fromBlank($record["thb"]["width"], $record["thb"]["height"], Image::rgb($record["thb"]["backgroundColor"]["red"], $record["thb"]["backgroundColor"]["green"], $record["thb"]["backgroundColor"]["blue"], $record["thb"]["backgroundColor"]["alpha"]));

				$img->resize($record["thb"]["width"], $record["thb"]["height"], Image::FIT);

				$background->place($img, ($background->getWidth() / 2) - ($img->getWidth() / 2), ($background->getHeight() / 2) - ($img->getHeight() / 2));

				$img = $background;

				break;
		}

		//$img = $this->createWatermark($img, $record["thb"]["width"], $record["thb"]["height"]);

		$img->save(WWW_DIR . "/" . $record["thb"]["src"], $record["thb"]["quality"]);

		return $record;
	}

	public function getThbPath($src, $properties = array())
	{
		//if(is_file(WWW_DIR . $src)){
			$path = pathinfo($src);

			$properties = array_extend(
				$this->settings["thb"], $properties
			);

			$path["dirname"] = $path["dirname"] . "/" . $properties["dirname"];

			if (!is_dir(WWW_DIR . "/" . $path["dirname"])) {
				mkdir(WWW_DIR . "/" . $path["dirname"], 0777);
			}

			$thbPath = $path["dirname"] . "/" . Strings::webalize("{$path["filename"]}") . "_" . Strings::webalize($properties["ratio"]) . ".{$path["extension"]}";

			return $thbPath;
		//}

		return false;
	}
}