<?php

namespace CP\Services\Files;

trait GetImages
{
	public function getSqlMainImageLocale($owner_table, $owner_id, $file_rel = "images", $as = "image")
	{
		return "(SELECT CONCAT_WS(';', file_src, IF(file_alt IS NULL, '', file_alt), IF(file_rel IS NULL, '', file_rel)) FROM {$this->repository->files->getName()} AS f LEFT JOIN {$this->repository->files->getNameLocale()} AS l ON l.file_id = f.file_id WHERE f.owner_table = '{$owner_table}' AND f.owner_id = {$owner_id} AND f.file_rel = '{$file_rel}' AND f.removed IS NULL ORDER BY file_main DESC, file_rank ASC LIMIT 1) AS {$as}";
	}
	
	public function getImagesByOwnerLocale($owner_table, $owner_id, $file_rel = NULL)
	{
		$files = $this->getByOwnerLocale($owner_table, $owner_id, $file_rel);

		foreach ($files AS & $file)
			$file = $this->getImageEntity($file);

		return $files;
	}

	public function getImagesByFolder($dir)
	{
		$d = dir($this->absolutePath($dir));

		$files = array();

		while (false !== ($entry = $d->read())) {
			if (/* $entry != '.' && $entry != '..' && !is_dir($dir."/".$entry) */in_array(strtolower(substr($entry, -3, 3)), array("jpg", "peg", "gif", "png", "bmp", "svg"))) {
				$file = array(
					"file_src" => $dir . "/" . $entry
				);

				$files[] = $this->getImageEntity($file);
			}
		}

		$d->close();

		return $files;
	}

	public function getOneImageByOwner($owner_table, $owner_id, $file_rel = NULL)
	{
		$file = $this->getOneByOwner($owner_table, $owner_id, $file_rel);
		if ($file) {
			return $this->getImageEntity($file->toArray());
		}
		return FALSE;
	}

	public function getImagesByOwner($owner_table, $owner_id, $file_rel = NULL)
	{
		$where = array(
			"owner_table" => $owner_table,
			"owner_id" => $owner_id,
		);

		if ($file_rel)
			$where["file_rel"] = $file_rel;

		$files = $this->repository->files->getAllBy(array("where" => $where, "order" => "file_rank ASC"));

		foreach ($files AS $file_id => & $file) {
			if($this->isValid($file)){
				$file = $this->getImageEntity($file);
			} else {
				$this->deleteFile($file["file_id"]);

				unset($files[$file_id]);
			}
		}

		return $files;
	}

	public function getImageEntity($file)
	{
		$file = (object)$file;

		$thbSettings = $this->getThbSettings(@$file->file_rel);

		$file->exists = is_file($this->getThbPath($file->file_src, $thbSettings));
		$file->thb = $file->exists ? $this->getThbPath($file->file_src, $thbSettings) : $file->file_src;
		$file->file_alt = isset($file->file_alt) ? $file->file_alt : "Bez názvu";

		if (is_file($file->thb)) {
			$image_size = getimagesize($file->thb);

			$file->width = $image_size[0];
			$file->height = $image_size[1];
		} else {
			$file->width = 0;
			$file->height = 0;
		}

		return $file;
	}

	public function parseImageEntity($file = null)
	{
		if ($file) {
			$image = explode(";", $file);

			if(is_file($this->absolutePath($image[0]))){
				$file = $this->getImageEntity(array(
					"file_src" => $image[0],
					"file_alt" => $image[1],
					"file_rel" => @$image[2]
				));
			} else {
				$file = null;
			}
		}

		return $file;
	}

	public function getImageExtension($filePath)
	{
		switch(exif_imagetype($filePath)){
			case IMAGETYPE_GIF: return "gif";
			case IMAGETYPE_JPEG: return "jpg";
			case IMAGETYPE_PNG: return "png";
			case IMAGETYPE_BMP: return "bmp";
		}
	}

	public function isImage($filePath)
	{
		return getimagesize($filePath);
	}
}