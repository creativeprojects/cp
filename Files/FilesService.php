<?php

namespace CP\Files;

use CP;

class Service extends CP\Service
{
	public $settings = array(
		"image" => array(
			"maxWidth" => 1600,
			"maxHeight" => 1600,
			"quality" => 70,
			"resize" => "SHRINK_ONLY"
		),
		"watermark" => false,
		"rel" => array()
	);

	use CP\Services\Files\CRUD;

	use CP\Services\Files\GetFiles;

	use CP\Services\Files\GetImages;

	use CP\Services\Files\ResizeImages;

	use CP\Services\Files\Thb;

	use CP\Services\Files\Watermark;
}