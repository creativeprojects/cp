<?php

namespace Repository;

class files extends \Repository\BaseRepository
{
	protected $name = "files";

	protected $fields = array(
		"file_id" => "primary",
		"client_id" => array(
			"type" => "int",
			"notNull" => true
		),
		"file_src" => array(
			"type" => "varchar",
			"notNull" => true
		),
		"owner_table" => "varchar",
		"owner_id" => "int",
		"file_rank" => "int",
		"file_main" => "bool",
		"file_rel" => "varchar",
		"file_alt" => array(
			"type" => "varchar",
			"locale" => true
		)
	);

	protected $validateTable = true;
}
