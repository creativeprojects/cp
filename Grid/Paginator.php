<?php

namespace CP\Grid;

use Nette\Utils\Html;

class Paginator extends Component
{
	public
		$defaultPage = 1,
		$variableName = "p",
		$itemsPerPage = null,
		$showRange = 0,
		$pagesCount = 0,
		$forced = false;

	function create()
	{
		$this->pagesCount = $this->itemsPerPage ? ceil($this->grid->itemsCount / $this->itemsPerPage) : 1;

		$this->page = @$_GET[$this->variableName] && @$_GET[$this->variableName] <= $this->pagesCount ? @$_GET[$this->variableName] : $this->defaultPage;

		if (!isset($this->grid->query["limit"]) && $this->itemsPerPage)
			$this->grid->query["limit"] = ($this->page - ($this->pagesCount ? 1 : 0)) * $this->itemsPerPage . ", {$this->itemsPerPage}";
	}

	function __toString()
	{
		$html = array();

		for ($page = 1; $page <= $this->pagesCount; $page++) {
			if (!$this->showRange || ($page == 1 || $page == $this->pagesCount || ($page > ($this->page - $this->showRange) && $page < ($this->page + $this->showRange))))
				$html[] = "<li" . ($page == $this->page ? " class=\"active\"" : "") . "><a href=\"" . $this->grid->presenter->link("this", array_extend($_GET, array("p" => $page))) . "\">" . $page . "</a></li>";
			else
				$html[] = "<li class=\"collapsed\"><em>...</em></li>";
		}

		$html = $this->pagesCount > 1 || $this->forced ? "<ul class=\"pages paginator\">" . implode($html) . "</ul>" : "";

		return $html;
	}

	public function createControl()
	{
		$html = $this->__toString();

		return Html::el()->setHtml($html);
	}
}