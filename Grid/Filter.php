<?php

namespace CP\Grid;

use CP\GeoCoder\GeoCoder;

class Filter extends Component{
	public 
		$rules = array(),
		$minStringLength = 2,
		$minLocationLength = 3,
		$buttonLabel = "Hledat",
		$buttonClass = "btn btn-primary",
		$buttonIcon = "fa-search";

	function create(){
		$rules = $this->grid->presenter->request->getParameters();

		foreach ($this->rules as $key => $rule) {
			if (isset($rules[$key])) {
				$value = $rules[$key];

				switch ($rule['type']) {
					default:
						$rule["minLength"] = @$rule["minLength"] ? @$rule["minLength"] : $this->minStringLength;

						if(strlen($value) >= $rule["minLength"]){
							$temp = array();

							foreach (array_force($rule['match']) as $match) $temp[] = 'MATCH(' . $match . ') AGAINST ( "+' . $value . '*" IN BOOLEAN MODE)';
							
							$this->grid->query['where'][$key] = '(' . implode(' OR ', $temp) . ')';
						}
						
						break;
					case "location":
						$rule["minLength"] = @$rule["minLength"] ? @$rule["minLength"] : $this->minLocationLength;

						if(strlen($value) >= $rule["minLength"]){
							$location = GeoCoder::getLocation($value);

							$this->grid->query['where'][$key] = GeoCoder::sqlDistance($location['lat'], $location['lng'], $rule["fieldLat"], $rule["fieldLng"]) . " < " . $rule["distance"];
						}
						break;
					case "select":
						if($value == "_all") break;
					case "multiselect":
						switch(@$rule["dataSource"]){
							default:
								$this->grid->query['where'][$key] = "{$key} IN ('".implode("','", (array) $value) . "')";

								break;
							case "json":
								$temp = array();

								foreach((array) $value AS $option) $temp[] = "{$key} LIKE ('%\"{$option}\"%')";

								$this->grid->query['where'][$key] = '(' . implode(' OR ', $temp) . ')';

								break;
						}
						
						break;
					case "range":
						$fieldFrom = isset($rule["fieldFrom"]) ? $rule["fieldFrom"] : (isset($rule["field"])) ? $rule["field"] : $key;
						$fieldTo = isset($rule["fieldTo"]) ? $rule["fieldTo"] : (isset($rule["field"])) ? $rule["field"] : $key;

						if(@is_numeric($value["from"])) $this->grid->query['where'][$key] = "{$fieldFrom} >= {$value["from"]}";
						if(@is_numeric($value["to"])) $this->grid->query['where'][$key] = "{$fieldTo} <= {$value["to"]}";

						break;
				}
			}
		}

		$this->createForm();
	}

	function createForm(){
		$this->form = new \CP\Forms\Form;

		$this->form->properties = array(
			"buttonIcon" => $this->buttonIcon,
			"buttonLabel" => $this->buttonLabel,
			"buttonClass" => $this->buttonClass,
			"rules" => $this->rules
		);

		$this->form->setMethod('get');

		foreach ($this->rules as $key => $rule) {
			switch ($rule['type']) {
				default:
					$this->form->addText($key, $rule["label"])
						->setAttribute("placeholder", @$rule["placeholder"]);
					
					break;
				case "select":
					if(@$rule["all"]) $rule["options"] = array_replace_recursive(array("_all" => $rule["all"]), $rule["options"]);

					$select = $this->form->addSelect($key, $rule["label"], $rule["options"]);

					if(@$rule["prompt"]) $select->setPrompt($rule["prompt"]);
					
					break;
				case "checkboxlist":
				case "multiselect":
					$this->form->addCheckboxList($key, $rule["label"], $rule["options"]);
					
					break;
				case "range":
					$container = $this->form->addContainer($key);

					$container->addText("from", $rule["labelFrom"]);
					$container->addText("to", $rule["labelTo"]);

					/*$this->form->addText("{$key}[from]", $rule["labelFrom"]);
					$this->form->addText("{$key}[to]", $rule["labelTo"]);*/
					
					break;
			}
		}

		$this->form->setValues($this->grid->presenter->request->getParameters());

		$this->form->addSubmit('search');

		//$this->form->onSuccess[] = array($this, "filterFormSucceeded");

		return $this->form;
	}

	function __toString(){
		return (string) $this->form;
	}

	function filterFormSucceeded($form){
		$values = $form->getValues();

		unset($values["search"]);

		$this->redirect('this', array('search' => $values));
	}
}