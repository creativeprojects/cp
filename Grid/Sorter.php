<?php

namespace CP\Grid;

use Nette\Utils\Html;

class Sorter extends Component{
	public 
		$fields = array(),
		$sort = "",
		$directions = array("ASC","DESC"), 
		$fieldName = "order",
		$fieldOrder = "dir";

	private $form;

	public function add($field, $title)
	{
		$this->fields[$field] = $title;

		return $this;
	}

	function create()
	{
		if (isset($_GET[$this->fieldName])) {
			$this->sort = $_GET[$this->fieldName] . ' ' . (in_array(strtoupper(@$_GET[$this->fieldOrder]), $this->directions) ? $_GET[$this->fieldOrder] : reset($this->directions));
			$this->grid->query["order_by"][$_GET[$this->fieldName]] = $this->sort;
		}
	}

	public function createForm()
	{
		if(count($this->fields)){
			$this->form = new \CP\Forms\Form;

			$this->form->setMethod('get');

			$this->form->addSelect($this->fieldName, "Seřadit podle", $this->fields);

			$this->form->addSelect("dir", "Seřadit", array("asc" => "Vzestupně", "desc" => "Sestupně"));
			
			$this->form->addSubmit("sort", "Seřadit", "fa-sort", "btn btn-default");

			return $this->form;
		}

		return null;
	}

	public function createControl()
	{
		$html = $this->__toString();

		return Html::el()->setHtml($html);
	}

	function __toString()
	{
		return $this->createForm();
	}
}