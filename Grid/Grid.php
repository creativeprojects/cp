<?php

namespace CP\Grid;

class Grid{
	public
		$db,
		$presenter,
		$forced = false,
		$itemsCount = 0,
		$itemsCallback = false,
		$query = array(
			"fields" => "*"
		),
		$items = array();

	function __construct(\Nette\Database\Context $db){
		$this->db = $db;

		$this->filter = new Filter($this);

		$this->sorter = new Sorter($this);

		$this->paginator = new Paginator($this);
	}

	public function setLimit($limit)
	{
		$this->query["limit"] = $limit;

		return $this;
	}

	function create(& $presenter){
		$this->presenter = & $presenter;

		$this->filter->create();

		$this->getItemsCount();
		
		$this->sorter->create();

		$this->paginator->create();

		$this->getItems();

		return $this;
	}

	function getItemsCount(){
		$fields = array(
			"COUNT(*) AS items_count"
		);

		$query = $this->query;

		$query["fields"] = array_merge($query['fields'], $fields);

		if(isset($query["union"])){
			if(!is_array($query["union"])){
				$query["union"]["from"] = $query["union"];
			}
			
			$query["union"]["fields"] = array_merge($query['fields'], $fields);
		}

		$result = $this->sql($query);
		
		foreach($result AS $itemsCount) $this->itemsCount += $itemsCount['items_count'];
	}

	function getItems(){
		$this->items = $this->sql($this->query);

		$keyField = isset($this->query["keyField"]) && array_key_exists($this->query["keyField"], reset($this->items)) ? $this->query["keyField"] : false;

		if($keyField || $this->itemsCallback){
			$items = array();

			foreach($this->items AS $key => $item){
				$items[($keyField ? $item[$keyField] : $key)] = $this->itemsCallback ? $this->itemsCallback->invokeArgs(array("values" => (object) $item)) : $item;
			}

			return $this->items = $items;
		}
	}

	function sql($query){
		if (!is_array($query["fields"]))
			$query["fields"] = array(
				array(
					"sql" => $query["fields"],
				)
			);

		return $this->db->query($this->query($query))->fetchAll();
	}

//Return MySQL query
	function query($query = array())
	{
		@$query = array(
			"fields" => $this->prepare_fields($query["fields"]),
			"from" => $this->prepare_from($query["from"]),
			"where" => $this->prepare_where($query["where"]),
			"order_by" => $this->prepare_order_by($query["order_by"]),
			"union" => ($query["union"] ?
				array(
				"from" => $this->prepare_from(is_array($query["union"]) && isset($query["union"]["from"]) ? $query["union"]["from"] : $query["union"]),
				"fields" => $this->prepare_fields(is_array($query["union"]) && isset($query["union"]["fields"]) ? $query["union"]["fields"] : $query["fields"]),
				"where" => $this->prepare_where(is_array($query["union"]) && isset($query["union"]["where"]) ? $query["union"]["where"] : $query["where"])
				) :
				null),
			"group_by" => $this->prepare_group_by($query["group_by"]),
			"having" => $this->prepare_having($query["having"]),
			"limit" => $query["limit"]
		);

//Return query string
		return "SELECT " .
			($query["fields"] ? $query["fields"] : " * ") .
			($query["from"] ? " FROM {$query["from"]}" : null) .
			($query["where"] ? " WHERE {$query["where"]}" : null) .
			($query["union"] ?
				" UNION SELECT " .
				($query["union"]["fields"]) .
				($query["union"]["from"] ? " FROM {$query["union"]["from"]}" : null) .
				($query["union"]["where"] ? " WHERE {$query["union"]["where"]}" : null) :
				null
			) .
			($query["group_by"] ? " GROUP BY {$query["group_by"]}" : null) .
			($query["having"] ? " HAVING {$query["having"]}" : null) .
			($query["order_by"] ? " ORDER BY {$query["order_by"]}" : null) .
			($query["limit"] ? " LIMIT {$query["limit"]}" : null);
	}

	function prepare_fields($fields)
	{
		$prepared_fields = array();

		foreach (array_filter(array_force($fields)) AS $key => $field)
			$prepared_fields[$key] = is_array($field) ? "{$field["sql"]} AS {$key}" : $field;

		return implode(", ", array_filter(array_force($prepared_fields)));
	}

	function prepare_from($from)
	{
		return implode(" ", array_filter(array_force($from)));
	}

	function prepare_where($where)
	{
		return implode(" AND ", array_filter(array_force($where)));
	}

	function prepare_group_by($group_by)
	{
		return implode(", ", array_filter(array_force($group_by)));
	}

	function prepare_order_by($order_by)
	{
		$prepared_order_by = array();

		foreach (@array_filter(array_force($order_by)) AS $key => $value)
			$prepared_order_by[$key] = implode(" ", array_force($value));

		return implode(", ", array_filter(array_force($prepared_order_by)));
	}

	function prepare_having($having)
	{
		return implode(" AND ", array_filter(array_force($having)));
	}
}
