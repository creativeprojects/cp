<?php

use Nette\Utils\Strings;

class instance
{
	private static $appDomain;

	private static $localhostName;

	private static $name;

	private static $dir;

	private static $relativeDir;

	public static function setLocalhostName($localhostName)
	{
		self::$localhostName = $localhostName;
	}

	public static function setAppDomain($appDomain)
	{
		self::$appDomain = $appDomain;
	}

	private static $appTitle;

	public static function setAppTitle($appTitle)
	{
		self::$appTitle = $appTitle;
	}

	public static function getAppTitle()
	{
		return self::$appTitle;
	}

	private static $appEmail;

	public static function setAppEmail($appEmail)
	{
		self::$appEmail = $appEmail;
	}

	public static function getAppEmail()
	{
		return self::$appEmail;
	}

	public static function getName()
	{
		if (!self::$name)
			self::setName();

		return self::$name;
	}

	public static function setName($name = null)
	{
		if (is_localhost()) {
			if (!self::$localhostName)
				dump("Set instance::setLocalhostName('Name') in localhostSettings.php");

			$name = self::$localhostName;
		} elseif (!$name){
			$domain = explode('.', $_SERVER["HTTP_HOST"]);

			//TODO - jak řešit ostatní TLD kromě CZ?

			if ($domain[1] == self::$appDomain && $domain[0] == 'www') {
				$name = "www";
			} elseif ($domain[1] == self::$appDomain) {
				$name = 'www-' . $domain[0] . '-cz';
			} elseif(self::$appDomain == null){
				$name = 'www-' . $domain[1] . '-cz';
			}else {
				$name = $_SERVER["HTTP_HOST"];
			}
		}

		$name = Strings::webalize($name);

		self::$name = $name;
	}

	public static function setDir()
	{
		self::$dir = WWW_DIR . '/theme/' . self::getName();
	}

	public static function getDir()
	{
		if (!self::$dir)
			self::setDir();

		return self::$dir;
	}

	public static function getRelativeDir()
	{
		if (!self::$relativeDir)
			self::setRelativeDir();

		return self::$relativeDir;
	}

	public static function setRelativeDir()
	{
		self::$relativeDir = '/theme/' . self::getName();
	}
}