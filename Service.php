<?php

namespace CP;

use Nette,
	CP;

class Service
{
	public function __construct(Nette\Database\Context $database, Nette\Security\IUserStorage $user, Nette\Localization\ITranslator $translator)
	{
		$this->serviceDatabase($database);

		$this->serviceUser($user);

		$this->serviceTranslator($translator);

		$this->serviceRepository();

		$this->serviceTemplate();
	}

	public $template;

	private function serviceTemplate()
	{
		$this->template = new CP\Template($this->translator);
	}

	public $repository;

	private function serviceRepository()
	{
		$this->repository = new CP\Repository\Manager($this->database, $this->user, $this->translator);
	}
	
	public $settings;

	public function serviceSettings($settings, $key = null)
	{
		if ($key)
			$settings = array($key => $settings);

		$this->settings = array_extend($this->settings, $settings);
	}
	
	public $database;

	public $connection;

	public function serviceDatabase($database)
	{
		$this->database = $database;

		$this->connection = $this->database->getConnection();
	}
	
	public $user;

	public function serviceUser($user)
	{
		if ($identity = $user->getIdentity())
			$this->user = $identity->data;
	}
	
	public $translator;

	public function serviceTranslator($translator)
	{
		$this->translator = $translator;

		//die;
	}

	public function quote($input)
	{
		return $this->connection->quote($input);
	}

	public $client;

	public $clientsService;

	protected function serviceClient($clientsService)
	{
		$this->clientsService = $clientsService;

		$this->client = $this->clientsService->getClient();
	}

	public function isMy($user_id)
	{
		return $this->user && $user_id == $this->user["user_id"];
	}

	public function isInRole($roles)
	{
		$roles = (array) $roles;

		return in_array($this->user["role"], $roles);
	}

	//TODO - přesunout do repository?
	public function sqlInJson($field, $options, $nullIncluded = false, $operant = "AND")
	{
		$sql = array();

		$options = array_filter((array)$options);

		foreach ($options AS $option) {
			$sql[] = "{$field} LIKE ('%\"{$option}\"%')";
		}

		return count($options) ? "(" . implode(" {$operant} ", $sql) . ($nullIncluded ? " OR {$field} IS NULL" : null) . ")" : null;
	}

	//TODO - přesunout do repository?
	public function sqlJsonPercentage($field, $options, $as = null, $nullIncluded = true)
	{
		$sql = array();

		$options = (array)$options;

		foreach ($options AS $option) {
			$sql[] = "IF({$field} LIKE ('%\"{$option}\"%'), 1, 0)";
		}

		return ($nullIncluded ? "IF({$field} IS NULL, 100, " : null) . (count($options) ? "((" . implode(" + ", $sql) . ") / " . count($options) . ") * 100 " : 100) . ($nullIncluded ? ")" : null) . ($as ? " AS {$as}" : null);
	}

	public function sqlUserLoggedIn()
	{
		$sql = (in_array($this->user["role"], array("admin", "superAdmin")) ? 1 : 0) . " = 1";

		return $sql;
	}

	//TODO - přesunout do repository?
	public function sqlRangePercentage($field, $from, $to, $min, $max, $as = null)
	{
		return ($min && $max ?
				"CASE "
				. "WHEN {$field} BETWEEN IFNULL({$from}, $min) AND IFNULL({$to}, {$max}) THEN 100 "
				. "WHEN {$field} < IFNULL({$from}, {$min}) THEN 100 * ({$field} - {$min}) / ({$from} - {$min}) "
				. "WHEN {$field} > IFNULL({$to}, {$max}) THEN 100 * ({$max} - {$field}) / ({$max} - {$to}) "
				. "END" :
				"NULL"
			) . ($as ? " AS {$as}" : null);
	}
	/* public $client;

	  public function setClient($client)
	  {
	  $this->client = array_extend($this->client, $client);
	  } */
}