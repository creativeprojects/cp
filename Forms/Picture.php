<?php

namespace CP\Forms;

use Nette;
use Nette\Forms\Controls\SubmitButton;

class Picture extends Nette\Object
{
	protected $name;

	protected $uploadName = "files";

	protected $path;

	protected $form;

	protected $presenter;

	protected $callbacks = array();

	protected $label = array(
		"uploaded" => "Obrázek byl nahrán",
		"nothingToUpload" => "Nevybrán žádný obrázek",
		"fileTypeAlert" => 'Obrázek musí být ve formátu JPEG, PNG nebo GIF'
	);

	public function __construct($presenter)
	{
		$this->presenter = $presenter;
	}

	public function setLabel($key, $label)
	{
		$this->label[$key] = $label;

		return $this;
	}

	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	public function setPath($path)
	{
		$this->path = $path;

		$this->presenter->filesService->validateDir($this->path);

		return $this;
	}
	protected $watermark;

	public function addWatermark($watermark = FALSE)
	{
		$this->watermark = $watermark;

		return $this;
	}
	protected $resizeWidth;

	protected $resizeHeight;

	protected $resizeType;

	public function resize($resizeWidth, $resizeHeight, $resizeType = Image::EXACT)
	{
		$this->resizeWidth = $resizeWidth;

		$this->resizeHeight = $resizeHeight;

		$this->resizeType = $resizeType;

		return $this;
	}

	protected $container;

	public function create($form)
	{
		$this->form = $form;

		$this->container = $this->form->addContainer($this->name);

		$this->addContainer($this, $this->container);

		$this->addUpload();
	}

	protected function addUpload($multiple = false)
	{
		$this->container->addUpload($this->uploadName, null, $multiple);

		$this->container->addSubmit('addImages')
			->setValidationScope(FALSE)
			->onClick[] = callback($this, "addImages");
	}

	protected function addContainer($parent, $image)
	{
		$image->addHidden("file_id");

		$image->addHidden("file_src");

		$file_alt = $image->addContainer("file_alt");

		foreach ($this->presenter->translator->getAvailableLocales() as $locale) {
			$file_alt->addText($locale);
		}

		$image->addHidden("file_rank");

		$image->addHidden("file_rel");

		$image->addHidden("file_main");

		$image->addSubmit("remove")
				->setValidationScope(false)
			->onClick[] = callback($parent, "removeImage");
	}

	public function addImages(SubmitButton $button)
	{
		$imagesCount = 0;

		$filesToUpload = array();

		$container = $button->parent->getValues();

		if ($container[$this->uploadName] instanceof Nette\Http\FileUpload && $container[$this->uploadName]->getName()) {
			$filesToUpload = array($container[$this->uploadName]);
		} elseif (is_array($container[$this->uploadName])) {
			$filesToUpload = $container[$this->uploadName];
		}

		if (count($filesToUpload)) {
			$images = $this->presenter->filesService->upload($filesToUpload, $this->path);

			foreach ($images AS $image) {

				if ($this->presenter->filesService->isImage(WWW_DIR . "/" . $image)) {

					if ($this->resizeWidth && $this->resizeHeight) {
						$this->presenter->filesService->resizeImage($image, $this->resizeWidth, $this->resizeHeight, null, $this->resizeType);
					} else {
						$this->presenter->filesService->resizeImageByRel($image, $this->name);
					}

					if ($this->watermark == TRUE) {
						$image = $this->presenter->filesService->addWatermark($image);
					}

					$image = array(
						"file_src" => $image,
						"file_rank" => 0,
						"file_main" => 0,
						"file_rel" => $this->name,
						"file_alt" => array(),
					);

					foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
						$image["file_alt"][$locale] = "";
					}

					foreach ($this->callbacks as $callback) {
						$callback->invokeArgs(array($image));
					}

					$this->setValues($button, $image);

					$imagesCount++;
				}
			}
		}

		if (!$imagesCount)
			$this->presenter->flashMessage($this->label["nothingToUpload"], "alert-info");
	}

	public function removeImage(SubmitButton $button)
	{
		$images = $button->parent->parent;

		$images->remove($button->parent, TRUE);
	}

	protected function setValues($button, $image)
	{
		$button->parent->setValues($image);
	}

	public function addCallback($callback)
	{
		$this->callbacks[] = $callback;
		
		return $this;
	}
}