<?php

namespace CP\Forms;

use Nette;
use CP\Forms\Picture;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

class Gallery extends Picture
{
	public function create($form){
		$this->form = $form;

		$parent = $this;

		$this->container = $this->form->addDynamic($this->name, function (Container $container) use ($parent) {
			$this->addContainer($parent, $container);
		});

		$this->addUpload(true);

		$this->container->addSubmit('removeAllImages')
			->setValidationScope(FALSE)
			->onClick[] = callback($this, "removeAllImages");

		return $this;
	}

	protected function setValues($button, $image){
		$button->parent->createOne()
			->setValues($image);
	}

	public function removeAllImages(SubmitButton $button){
		$images = $button->parent;

		foreach($images->containers AS $image){
			$images->remove($image, TRUE);
		}
	}
}
