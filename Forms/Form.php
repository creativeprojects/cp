<?php

namespace CP\Forms;

use Nette,
	CP,
	Nette\Utils\Html;

use CP\Forms\Gallery;
use CP\Forms\Picture;

\Kdyby\Replicator\Container::register();

class Form extends Nette\Application\UI\Form{

	public $properties;

	public function __construct(Nette\ComponentModel\IContainer $parent = NULL, $name = NULL){
		parent::__construct($parent, $name);

		$this->getElementPrototype()->class[] = "no-enter prevent-dblclick";

		$renderer = $this->getRenderer();
		
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['container'] = null;
		$renderer->wrappers['label']['container'] = null;
	}

	public function addAttachments($presenter, $name)
	{
		$attachments = new Attachments($presenter);

		$attachments->setName($name)
			->create($this);

		return $attachments;
	}

	//TODO - vyřešit abysme sem nemuseli posilat presenter
	public function addGallery($presenter, $name, $path){
		$gallery = new Gallery($presenter);

		$gallery->setName($name)
			->setPath($path)
			->create($this);

		return $gallery;
	}

	//TODO - vyřešit abysme sem nemuseli posilat presenter
	public function addPicture($name, $path, $presenter){
		$picture = new Picture($presenter);

		$picture->setName($name)
			->setPath($path)
			->create($this);

		return $picture;
	}

	public function loadValues(& $values){
		$values = (array) $values;

		if (!empty($_POST)) {
			foreach ($_POST as $key => $value) $values[$key] = $value;
		}
	}

	public function setValues($values, $erase = FALSE){
		parent::setValues($values, $erase);

		return $values;
	}

	public function attached($presenter){
		parent::attached($presenter);

		$this->setTranslator($this->getPresenter(true)->translator);
	}

	public function addPassword($name, $label = NULL, $cols = NULL, $maxLength = NULL){
		parent::addPassword($name, $label, $cols, $maxLength);
		
		$this->setBootstrapRendering($name);

      	return $this[$name];	
	}


	public function addSelect($name, $label = NULL, array $items = NULL, $size = NULL){
		parent::addSelect($name, $label, $items, $size);

		$this->setBootstrapRendering($name);

      	return $this[$name];
    }

    public function addCheckbox($name, $label = NULL){
		parent::addCheckbox($name, Html::el('span')->setHtml($label));

		$label = $this[$name]->getLabelPrototype();
      	$label->class[] = "control-label";

		return $this[$name];
	}

    public function addCheckboxlist($name, $label = NULL, array $items = NULL){
		parent::addCheckboxlist($name, $label, $items);

		$label = $this[$name]->getLabelPrototype();
      	$label->class[] = "control-label";

      	return $this[$name];
    }

	/*public function addTextLocale($name, $label = NULL, $cols = NULL, $maxLength = NULL){
		parent::addText($name, Html::el('span')->setHtml($label), $cols, $maxLength);

		$this->setBootstrapRendering($name);

		return $this[$name];
	}*/

	public function addTextArea($name, $label = NULL, $cols = NULL, $maxLength = NULL){
		parent::addTextArea($name, Html::el('span')->setHtml($label), $cols, $maxLength);

		$this->setBootstrapRendering($name);

		return $this[$name];
	}

	public function addHTML($name, $label = NULL, $cols = NULL, $maxLength = NULL){
		parent::addTextArea($name, Html::el('span')->setHtml($label), $cols, $maxLength);

		$this->setBootstrapRendering($name);

		$control = $this[$name]->getControlPrototype();
      	$control->class[] = "html";

		return $this[$name];
	}

	public function addSubmit($name, $caption = NULL, $icon = null, $class = "btn", $value = null){
		parent::addSubmit($name, NULL);

		$button = $this[$name]->getControlPrototype();
      	$button->setName("button");
      	$button->class = $class;
      	$button->setHtml(($icon ? "<i class=\"fa {$icon}\"></i> " : null) ."<span>{$caption}</span>");

     	return $this[$name];
    }

    function setBootstrapRendering($name){
		$control = $this[$name]->getControlPrototype();
      	$control->class[] = "form-control";
      	
      	$label = $this[$name]->getLabelPrototype();
      	$label->class[] = "control-label";
	}
}