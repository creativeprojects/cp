<?php

namespace CP\Forms;

use Nette,
	CP\Forms\Form;

abstract class Base extends Nette\Object
{
	protected $form;

	protected $values = array();

	protected $presenter;

	protected $attached;

	public function __construct($presenter, $name = null)
	{
		$this->presenter = $presenter;

		$this->form = new Form;

		//Attach form to presenter
		if(isset($name)){
			$this->presenter[$name] = $this->form;

			$this->attached = true;
		}
	}

	public function setValues($values){
		$this->values = (array) $values;

		$this->sanitizeInputValues();

		return $this;
	}

	protected function setDefaults()
	{
		if($this->attached ? !$this->form->isSubmitted() : empty($_POST)){
			$this->sanitizeDefaults($this->values);

			$this->form->setDefaults($this->values);
		}
	}

	protected function sanitizeDefaults(& $values)
	{
		foreach($values AS & $value){
			if(is_object($value)){
				if(!($value instanceof Nette\Utils\DateTime))
					$value = (array) $value;
			}

			if(is_array($value)) $this->sanitizeDefaults($value);
		}
	}

	protected function sanitizeInputValues()
	{

	}

	protected function invalidateControl()
	{
		if($this->presenter->isAjax()){
			$this->presenter->invalidateControl();
		}
	}
}