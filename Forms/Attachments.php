<?php

//Není funkční - přesunout jako rozšíření CP\Forms\Form jako nové kompoentny $form->addAttachments();

namespace CP\Forms;

use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

class Attachments
{
	protected $name;

	protected $uploadName;

	protected $dir;

	protected $form;

	public function __construct($presenter)
	{
		$this->presenter = $presenter;

		$this->uploadName = "files";

		$this->dir = "attachments";
	}

	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	//alias setDir()
	public function setPath($dir, $permissions = 0777)
	{
		$this->setDir($dir, $permissions);

		return $this;
	}

	public function setDir($dir, $permissions = 0777)
	{
		$this->presenter->filesService->validateDir($dir, $permissions);

		$this->dir = $dir;

		return $this;
	}

	public function create($form)
	{
		$this->form = $form;

		$parent = $this;

		$container = $this->form->addDynamic($this->name, function (Container $container) use ($parent) {

			$container->addHidden("file_id");

			$container->addHidden("file_src");

			$container->addSubmit("remove")
					->setValidationScope(false)
				->onClick[] = callback($parent, "remove");
		});

		$container->addUpload($this->uploadName);

		$container->addSubmit("add")
				->setValidationScope(FALSE)
			->onClick[] = callback($this, "add");

		$container->addSubmit('removeall')
				->setValidationScope(FALSE)
			->onClick[] = callback($this, "removeAll");
	}

	protected function setValues($button, $attachment)
	{
		$button->parent->createOne()
			->setValues($attachment);
	}

	public function add($button)
	{
		$filesCount = 0;

		$filesToUpload = array();

		$attachments = $button->parent->getValues();

		if ($attachments[$this->uploadName] instanceof \Nette\Http\FileUpload && $attachments[$this->uploadName]->getName()) {
			$filesToUpload = array($attachments[$this->uploadName]);
		} elseif (is_array($attachments[$this->uploadName])) {
			$filesToUpload = $attachments[$this->uploadName];
		}

		if (count($filesToUpload)) {
			$files = $this->presenter->filesService->upload($filesToUpload, $this->dir);

			foreach ($files AS $file) {
				$file = array(
					"file_src" => $file
				);

				$button->parent->createOne()
					->setValues($file);

				$filesCount++;
			}
		}

		if ($filesCount)
			$this->presenter->flashMessage("Přílohy byly přidány", "alert-success");
		else
			$this->presenter->flashMessage("Nebyly vybrány žádné soubory k nahrání", "alert-info");
	}

	public function remove($button)
	{
		$attachment = $button->parent->parent;

		$attachment->remove($button->parent, TRUE);

		$this->presenter->flashMessage("Příloha byla odebrána", "alert-success");
	}

	public function removeAll($button)
	{
		$attachments = $button->parent;

		foreach ($attachments->containers AS $attachment) {
			$attachments->remove($attachment, TRUE);
		}

		$this->presenter->flashMessage("Všechny přílohy byly odebrány", "alert-success");
	}
}