<?php

namespace Repository;

class newsletterContacts extends \Repository\BaseRepository
{
	public $name = "newsletter_contacts";

	public $fields = array(
		"client_id" => "int",
		"newsletter_contact_id" => "primary",
		"newsletter_contact_email" => "varchar"
	);

	public $validateTable = true;

}