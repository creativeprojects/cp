<?php

namespace CP\Forms;

use Nette,
	CP\Forms\Form;

class NewsletterForm extends Base
{
	public function create()
	{
		$this->form->addText('newsletter_contact_email')
			->addRule(Form::EMAIL, "Zadejte správný email.")
			->setRequired();

		$this->form->addSubmit('save')
			->onClick[] = callback($this, "save");

		return $this->form;
	}

	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->presenter->newsletterService->save($values)) {
			$this->presenter->flashMessage("Děkujeme. Váš email byl přihlášen k odběru novinek.", "alert-success");

			$this->presenter->restoreRequest($this->backlink);
			$this->presenter->redirect("this");
		} else {
			$this->presenter->flashMessage("Při ukládání emailu došlo k chybě!", "alert-danger");
		}
	}
}