<?php

namespace CP\Services;

use CP;

use Nette;
use Nette\Utils\Strings;
use Nette\Utils\Validators;

class Newsletter extends CP\Base\Service
{
	public $settings = array(
		"multiInstance" => false
	);

	public function subscribe($newsletter_contact_email)
	{
		$values = array(
			'newsletter_contact_email' => $newsletter_contact_email
		);

		return $this->save($values);
	}

	public function unSubscribe($newsletter_contact_email)
	{
		if(Validators::isEmail($newsletter_contact_email)){
			$by = array(
				"newsletter_contact_email" => $newsletter_contact_email,
			);

			if($this->settings["multiInstance"])
				$by["client_id"] = $this->client["id"];
			
			return $this->repository->newsletterContacts->deleteBy($by);
		}

		return false;
	}

	public function save($values)
	{
		if(Validators::isEmail($values["newsletter_contact_email"])){
			if (!$this->repository->newsletterContacts->existsBy(array('newsletter_contact_email' => $values["newsletter_contact_email"]))) {
				if($this->settings["multiInstance"])
					$values["client_id"] = $this->client["id"];

				return $this->repository->newsletterContacts->insert($values);
			}

			return TRUE;
		}

		return false;
	}
}