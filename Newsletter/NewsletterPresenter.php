<?php

namespace CP\Newsletter;

use Nette\Utils\Validators;

use Nette\Application\BadRequestException;

trait Presenter
{

	public function actionUnSubscribe($newsletter_contact_email){
		if(Validators::isEmail($newsletter_contact_email)){
			$this->newsletterService->unSubscribe($newsletter_contact_email);

			$this->template->newsletter_contact_email = $newsletter_contact_email;
		} else
			throw new BadRequestException;
	}

	public function actionSubscribe($newsletter_contact_email = null){
		if(Validators::isEmail($newsletter_contact_email)){
			$this->newsletterService->subscribe($newsletter_contact_email);
		}

		$this->template->newsletter_contact_email = $newsletter_contact_email;
	}		
}