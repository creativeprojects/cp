<?php

namespace Repository;

use Nette;

abstract class BaseRepository extends \Nette\Object
{
	protected $hasLocale = false;

	protected $includeRemoved = false;

	protected $nameLocale;

	protected $translator;

	protected $database;

	protected $user;

	protected $validateTable = false;

	protected $defaultRows = array();

	protected $forceRowsValidation = false;

	public $debug = false;

	public function __construct(\Nette\Database\Context $database, $user, \Nette\Localization\ITranslator $translator)
	{
		$this->database = $database;

		$this->translator = $translator;

		$this->user = $user;

		$this->validateFields();

		if ($this->hasLocale() && !$this->nameLocale)
			$this->setNameLocale("{$this->name}");

		if (DEBUG_MODE && $this->validateTable) {
			$this->validateTable();
		}
	}
	/*	 * ***************************************** Get ********************************************* */

	protected function _getAllBy($params)
	{
		$result = $this->_get($params)->fetchPairs($this->getPrimaryKey());

		return $result;
	}

	protected function _getAllByLocale($params, $locale)
	{
		$resultLocale = $this->_getLocale($params, $locale)->fetchPairs($this->getPrimaryKey());

		return $resultLocale;
	}

	protected function _getOneBy($params)
	{
		$params["limit"] = 1;

		$result = $this->_get($params)->fetch();

		if ($result)
			$result = (array)$result;

		return $result;
	}

	protected function _getOneByLocale($params, $locale)
	{
		$params["limit"] = 1;

		$resultLocale = $this->_getLocale($params, $locale)->fetch();

		if ($resultLocale)
			$resultLocale = (array)$resultLocale;

		return $resultLocale;
	}

	protected function _getOneByIncludeLocale($params, $locale = null)
	{
		$params["limit"] = 1;

		$result = $this->_getIncludeLocale($params, $locale)->fetch();

		if ($result)
			$result = (array)$result;

		return $result;
	}

	protected function _get($params)
	{

		$result = @$this->database->query("SELECT {$this->getSqlFields($params["fields"])} {$this->getSqlAdditionalFields($params["additionalFields"])} FROM {$this->getName()} {$this->getSqlWhere($params["where"], null, $params["includeRemoved"])} {$this->getSqlOrder($params["order"])} {$this->getSqlLimit($params["limit"])}");

		return $result;
	}

	protected function _getIncludeLocale($params, $locale = null)
	{
		if (!$locale)
			$locale = $this->translator->getLocale();

		$sql = array();

		$sql[] = "SELECT";

		$sql[] = $this->getSqlFields($params["fields"]);

		if ($this->hasLocale()) {
			$sql[] = ",";

			$sql[] = $this->getSqlFieldsLocale($locale, $params["fields"]);
		}

		$sql[] = $this->getSqlAdditionalFields(@$params["additionalFields"]);

		$sql[] = "FROM {$this->getName()}";

		if ($this->hasLocale()) {
			$sql[] = "LEFT JOIN {$this->getNameLocale($locale)}";

			$sql[] = "ON {$this->getNameLocale($locale)}.{$this->getPrimaryKey()} = {$this->getName()}.{$this->getPrimaryKey()}";
		}

		$sql[] = $this->getSqlWhere($params["where"], $locale, $params["includeRemoved"]);

		$sql[] = $this->getSqlOrder($params["order"]);

		$sql[] = $this->getSqlLimit($params["limit"]);

		if ($this->debug)
			dump($sql);

		$sql = implode(" ", array_filter($sql));

		if ($this->debug)
			var_dump($sql);

		$result = $this->database->query($sql);

		if ($this->debug)
			dump($result);

		return $result;
	}

	protected function _getLocale($params, $locale)
	{
		$fields = array(
			"{$this->getName()}.{$this->getPrimaryKey()}"
		);

		$fields[] = $this->getSqlFieldsLocale($locale, $params["fields"]);

		$resultLocale = @$this->database->query("SELECT " . implode(",", array_filter($fields)) . " {$this->getSqlAdditionalFields($params["additionalFields"])} FROM {$this->getName()} LEFT JOIN {$this->getNameLocale($locale)} ON {$this->getNameLocale($locale)}.{$this->getPrimaryKey()} = {$this->getName()}.{$this->getPrimaryKey()} {$this->getSqlWhere($params["where"], $locale)} {$this->getSqlOrder($params["order"])} {$this->getSqlLimit($params["limit"])}");

		return $resultLocale;
	}

//TODO - deprecated
	public function get($where)
	{
		return $this->getAllBy($where);
	}

	private function validateSqlParams($params)
	{
		$paramsTemplate = array(
			"fields" => null,
			"where" => array(),
			"order" => null,
			"limit" => null,
			"includeRemoved" => false
		);

		if (!array_key_exists("where", $params)) {
			$params = array(
				"where" => $params
			);
		}
		return array_extend($paramsTemplate, $params);
	}

	public function getCountBy($where, $locale = null, $includeRemoved = false)
	{
		$result = @$this->database->query("SELECT COUNT(*) AS rows_count FROM " . ($locale ? "{$this->getNameLocale($locale)} LEFT JOIN {$this->getName()} ON {$this->getNameLocale($locale)}.{$this->getPrimaryKey()} = {$this->getName()}.{$this->getPrimaryKey()}" : $this->getName()) . " {$this->getSqlWhere($where, $locale, $includeRemoved)}")->fetch();

		if ($result)
			return $result->rows_count;
		else
			return 0;
	}

	public function getAllBy($params = array())
	{
		$params = $this->validateSqlParams($params);

		$result = $this->_getAllBy($params);

		$resultLocale = array();

		if ($this->hasLocale()) {
			foreach ($this->translator->getAvailableLocales() AS $locale) {
				$resultLocale[$locale] = $this->_getAllByLocale($params, $locale);
			}
		}

		foreach ($result AS $primaryKey => & $row) {
			$row = (array)$row;

			foreach ($this->getFieldsLocale() AS $key => $field) {
				$row[$key] = array();

				foreach ($this->translator->getAvailableLocales() AS $locale) {
					$row[$key][$locale] = isset($resultLocale[$locale][$primaryKey]->$key) ? $resultLocale[$locale][$primaryKey]->$key : null;
				}
			}
			$row = $this->sanitizeOutput($row, true, true);
		}

		return $result;
	}

	public function getAll()
	{
		return $this->getAllBy(array());
	}

//TODO - deprecated
	public function getLocale($where)
	{
		return $this->getAllByLocale($where);
	}

	public function getAllByLocale($params = array())
	{
		$params = $this->validateSqlParams($params);

		$result = $this->_getAllBy($params);

		if ($this->hasLocale()) {
			$resultLocale = $this->_getAllByLocale($params, $this->translator->getLocale());
		}
		foreach ($result AS $primaryKey => & $row) {
			$row = (array)$row;

			if ($this->hasLocale()) {
				$rowLocale = (array)$resultLocale[$primaryKey];

				$row = array_merge($row, $rowLocale);
			}

			$row = $this->sanitizeOutput($row, false, false);
		}

		return $result;
	}

	public function getAllLocale()
	{
		return $this->getAllByLocale(array());
	}

	public function getOneBy($params)
	{

		$params = $this->validateSqlParams($params);

		$result = $this->_getOneBy($params);

		if ($result) {
			$resultLocale = array();

			if ($this->hasLocale()) {
				foreach ($this->translator->getAvailableLocales() AS $locale) {
					$resultLocale[$locale] = $this->_getOneByLocale($params, $locale);
				}
			}

			foreach ($this->getFieldsLocale() AS $key => $field) {
				$result[$key] = array();

				foreach ($this->translator->getAvailableLocales() AS $locale) {
					$result[$key][$locale] = $resultLocale[$locale][$key];
				}
			}

			$result = $this->sanitizeOutput($result, false, true);
		}

		return $result;
	}

	public function getOneByLocale($params, $locale = null)
	{
		$params = $this->validateSqlParams($params);

		if ($this->debug)
			dump($params);

		$result = $this->_getOneByIncludeLocale($params, $locale);

		if ($result) {
			$result = $this->sanitizeOutput($result, false, false);
		}

		return $result;
	}
	/* public function getOneByLocale($params)
	  {
	  $params = $this->validateSqlParams($params);

	  $result = $this->_getOneBy($params);

	  if ($result) {
	  if ($this->hasLocale()) {
	  $resultLocale = $this->_getOneByLocale($params, $this->translator->getLocale());
	  $result = array_merge((array)$result, (array)$resultLocale);
	  }

	  $result = $this->sanitizeOutput($result, false, false);
	  }

	  return $result;
	  } */

	public function getMaxBy($field, $by = array(), $locale = null)
	{
		return $this->getMinMaxBy("MAX", $field, $by, $locale);
	}

	public function getMinBy($field, $by = array(), $locale = null)
	{
		return $this->getMinMaxBy("MIN", $field, $by, $locale);
	}

	protected function getMinMaxBy($request, $field, $by, $locale = null)
	{
		$result = $this->database->query("
			SELECT {$request}({$field}) AS max_value
			FROM {$this->getName()}" .
				($this->hasLocale ? "
				LEFT JOIN {$this->getNameLocale($locale)}
				ON {$this->getName()}.{$this->getPrimaryKey()} = {$this->getNameLocale($locale)}.{$this->getPrimaryKey()}" :
					null
				) .
				" {$this->getSqlWhere($by, $locale)}")->fetch();

		if ($result) {
			return $result->max_value;
		}

		return false;
	}

	public function getOne($primaryKeyValue)
	{
		$by = array(
			$this->getPrimaryKey() => $primaryKeyValue
		);

		$result = $this->getOneBy($by);

		return $result;
	}

	public function getOneLocale($primaryKeyValue)
	{
		$by = array(
			$this->getPrimaryKey() => $primaryKeyValue
		);

		$result = $this->getOneByLocale($by);

		return $result;
	}
	/*	 * ***************************************** Save ********************************************* */

	public function exists($primaryKeyValue)
	{
		$where = array(
			$this->getPrimaryKey() => $primaryKeyValue
		);

		return $this->database->table($this->getName())->where($where)->count();
	}

	public function existsBy($where, $locale = null, $includeRemoved = false)
	{
		$count = $this->getCountBy($where, $locale, $includeRemoved = false);

		return $count ? true : false;
	}

	public function save($values)
	{
		$where = array(
			$this->getPrimaryKey() => @$values[$this->getPrimaryKey()]
		);

		if (@$values[$this->getPrimaryKey()] && $this->exists(@$values[$this->getPrimaryKey()])) {
			$values = $this->_update($where, $values);
		} else {
			$values = $this->_insert($values);
		}

		return $values;
	}

	public function saveField($primaryKeyValue, $field, $value)
	{
		return $this->_update(array($this->getPrimaryKey() => $primaryKeyValue), array($field => $value));
	}

	public function insert($values)
	{
		return $this->_insert($values);
	}

	protected function _insert($values)
	{
		$primaryKey = $this->getPrimaryKey();

		$this->setCreated($values);

		$sanitizedValues = $this->sanitizeInputValues($values, false);

		$result = $this->database->table($this->getName())->insert($sanitizedValues);

		if ($result) {
			$values[$primaryKey] = $result->$primaryKey;

			if ($this->hasLocale()) {
				foreach ($this->translator->getAvailableLocales() AS $locale) {
					$sanitizedValues = $this->sanitizeInputValues($values, $locale);

					$this->database->table($this->getNameLocale($locale))->insert($sanitizedValues);
				}
			}

			return $values;
		}

		return false;
	}

	public function update($values)
	{
		$where = array(
			$this->getPrimaryKey() => $values[$this->getPrimaryKey()]
		);

		return $this->_update($where, $values);
	}

	public function updateBy($where, $values)
	{
		return $this->_update($where, $values);
	}

	protected function _update($where, $values)
	{
		$this->setChanged($values);

		$this->unsetRemoved($values);

		$sanitizedValues = $this->sanitizeInputValues($values, false);

		$this->database->table($this->getName())->where($this->filterWhere($where, false))->update($sanitizedValues);

		if ($this->hasLocale()) {
			foreach ($this->translator->getAvailableLocales() AS $locale) {
				$sanitizedValues = $this->sanitizeInputValues($values, $locale);

				if (count($sanitizedValues)) {
					if ($this->existsBy($where, $locale, true)) {
						$this->database->query("UPDATE {$this->getNameLocale($locale)} LEFT JOIN {$this->getName()} ON {$this->getNameLocale($locale)}.{$this->getPrimaryKey()} = {$this->getName()}.{$this->getPrimaryKey()} {$this->getSqlUpdateValues($sanitizedValues, $locale)} {$this->getSqlWhere($where, $locale)}");
					} else {
						if (!array_key_exists($this->getPrimaryKey(), $sanitizedValues)) {
							$primaryKeys = $this->getAllBy(array(
								"where" => $where,
								"fields" => array(
									$this->getPrimaryKey()
								)
							));

							foreach ($primaryKeys AS $row) {
								$sanitizedValues[$this->getPrimaryKey()] = $row[$this->getPrimaryKey()];

								$this->database->table($this->getNameLocale($locale))->insert($sanitizedValues);
							}
						} else {
							$this->database->table($this->getNameLocale($locale))->insert($sanitizedValues);
						}
					}
				}
			}
		}

		return $values;
	}
	/*	 * ***************************************** Remove, Delete ********************************************* */

	public function remove($values)
	{
		if ($primaryKeyValue = $values[$this->getPrimaryKey()]) {
			$by = array(
				$this->getPrimaryKey() => $primaryKeyValue
			);

			return $this->removeBy($by);
		}

		return false;
	}

	public function removeOne($primaryKeyValue)
	{
		$by = array(
			$this->getPrimaryKey() => $primaryKeyValue
		);

		$result = $this->removeBy($by);

		return $result;
	}

	public function removeBy($where)
	{
		$values = array();

		$this->setRemoved($values);

		$this->database->table($this->getName())->where($this->filterWhere($where, false))->update($values);

		return true;
	}

	public function delete($primaryKeyValue)
	{
		return $this->deleteOne($primaryKeyValue);
	}

	public function deleteOne($primaryKeyValue)
	{
		$by = array(
			$this->getPrimaryKey() => $primaryKeyValue
		);

		$result = $this->deleteBy($by);

		return $result;
	}

	public function deleteBy($where)
	{
		//TODO - jak řešit při podmínkách, které se nevztahují na lokalizovanou tabulku
		if ($this->hasLocale()) {
			foreach ($this->translator->getAvailableLocales() AS $locale) {
				$this->database->query("DELETE {$this->getNameLocale($locale)} FROM {$this->getNameLocale($locale)} INNER JOIN {$this->getName()} ON {$this->getNameLocale($locale)}.{$this->getPrimaryKey()} = {$this->getName()}.{$this->getPrimaryKey()} {$this->getSqlWhere($where, $locale)}");
			}
		}

		$this->database->table($this->getName())->where($this->filterWhere($where, false))->delete();

		return true;
	}
	/*	 * ***************************************** Logs ********************************************* */
	public $logFields = array(
		"created" => array(
			"type" => "created",
		),
		"created_user_id" => array(
			"type" => "int",
		),
		"changed" => array(
			"type" => "changed",
		),
		"changed_user_id" => array(
			"type" => "int"
		),
		"removed" => array(
			"type" => "removed",
		),
		"removed_user_id" => array(
			"type" => "int"
		)
	);

	private function addLogFields()
	{
		$this->fields = array_merge($this->fields, $this->logFields);
	}

	private function setCreated(& $values)
	{
		$this->setLogField($values, "created");
	}

	private function setChanged(& $values)
	{
		$this->setLogField($values, "changed");
	}

	private function setRemoved(& $values)
	{
		$this->setLogField($values, "removed");
	}

	private function unsetRemoved(& $values)
	{
		$this->setLogField($values, "removed", false);
	}

	private function setLogField(& $values, $operation, $set = true)
	{
		$values[$operation] = $set ? now() : null;
		$values[$operation . "_user_id"] = $set ? $this->user["user_id"] : null;
	}
	/*	 * ***************************************** Values sanitizations, filters ********************************************* */

	public function sanitizeInputValues($values, $locale = null, $forced = false)
	{
		$sanitizedValues = array();

		foreach ($this->fields as $field => $props) {
			$validValue = array_key_exists($field, $values) || ($forced AND in_array($props["type"], array('checkbox', "switch", "bool")));

			if ($this->validLocale($locale, $props) && $validValue) {
				if ($props["locale"]) {
					$value = @$values[$field][$locale];
				} else {
					$value = $values[$field];
				}

				$sanitizedValues[$field] = $this->sanitizeInputValue($props, $value);
			}
		}

		return $sanitizedValues;
	}

	private function validLocale($locale, $props)
	{
		return $locale === null ? true : ($locale === false && !$props["locale"] ? true : (strlen($locale) && ($props["locale"] || strtolower($props["type"]) == "primary") ? true : false));
	}

	private function sanitizeInputValue($props, $value)
	{
// TODO Co vše nastavit na NULL ?
		if (empty($value) && !$props["notNull"]) {
			return null;
		} else {
			switch ($props["type"]) {
				case 'json':
					return json_encode($value);
				case 'switch':
				case 'bool':
				case 'checkbox':
					return $value ? 1 : 0;
				case 'float':
				case 'int':
				case 'double':
				case 'number':
//TODO - zjistit jestli tenhle regual funguje správně (záporná čísla)
					return (float)preg_replace('/[^0-9.]*/', '', str_replace(",", ".", $value));
				case "date":
					return date("Y-m-d", strtotime($value));
				case "time":
					return date("H:i:s", strtotime($value));
				case "datetime":
				case "timestamp":
				case "created":
				case "changed":
				case "removed":
					$timestamp = strtotime($value);

					return $timestamp ? date("Y-m-d H:i:s", strtotime($value)) : null;
				case "youtube":
					if (strpos($value, "/") !== false) {
						preg_match("#([\/|\?|&]vi?[\/|=]|youtu\.be\/|embed\/)(\w+)#", $value, $matches);

						if (count($matches))
							return end($matches);
						else
							return null;
					}

					break;
				default:
					return $value;
			}
		}
	}

	public function sanitizeOutput($values = array(), $fieldsOnly = false, $allLocales = false)
	{
		foreach ($values as $field => $value) {
			if (array_key_exists($field, $this->fields)) {
				$props = $this->fields[$field];

				if ($props["locale"] && $allLocales) {
					$tempValue = $value;
					$values[$field] = array();

					foreach ($this->translator->getAvailableLocales() AS $locale) {
						$values[$field][$locale] = $this->sanitizeOutputValue($props, $value[$locale]);
					}
				} else {
					$values[$field] = $this->sanitizeOutputValue($props, $value);
				}
			} elseif (array_key_exists($field, $this->queryAdditionalFields)){
				$values[$field] = $value;
			} elseif ($fieldsOnly) {
				unset($values[$field]);
			}
		}

		$this->queryAdditionalFields = array();

		return $values;
	}

	protected function sanitizeOutputValue($props, $value = null)
	{
		if (empty($value) && !$props["notNull"]) {
			switch ($props["type"]) {
				case "json": return array(); break;
				default: return null; break;
			}
		} else {
			switch ($props["type"]) {
				case "json":
					return json_decode($value, true);
				case "created":
				case "changed":
				case "removed":
					$props["type"] = "datetime";
				case "timestamp":
					$props["type"] = "datetime";
				case "time":
				case "date":
				case "datetime":
					$this->loadDateTimeFormats();

					$value = (string) $value;

					return date($this->dateTimeFormats[$this->translator->getLocale()]["format"][$props["type"]], strtotime($value));
				default:
					return $value;
			}
		}
	}

	protected $dateTimeFormats;

	protected function loadDateTimeFormats()
	{
		if(!$this->dateTimeFormats)
			$this->dateTimeFormats = Nette\Utils\Neon::decode(file_get_contents(CP_DIR . "/Regions/dateTimeFormats.neon"));
	}

	/*	 * ***************************************** Fields ********************************************* */
	protected $fieldTemplate = array(
		"type" => "",
		"locale" => false,
		"notNull" => false,
		"primary" => false,
		"autoIncrement" => false,
		"length" => null,
		"default" => null,
		"key" => null,
		"onUpdate" => null,
		"deprecated" => null
	);

	protected $fieldTypeTemplates = array(
		"primary" => array(
			"sqlType" => "int",
			"length" => 11,
			"key" => "primary",
			"notNull" => true,
			"autoIncrement" => true
		),
		"int" => array(
			"sqlType" => "int",
			"length" => 11
		),
		"tinyint" => array(
			"sqlType" => "tinyint",
			"length" => 4
		),
		"string" => array(
			"sqlType" => "varchar",
			"length" => 255
		),
		"varchar" => array(
			"sqlType" => "varchar",
			"length" => 255
		),
		"bool" => array(
			"sqlType" => "tinyint",
			"length" => 1,
			"notNull" => true
		),
		"json" => array(
			"sqlType" => "text",
		),
		"html" => array(
			"sqlType" => "longtext"
		),
		"lighthtml" => array(
			"sqlType" => "text"
		),
		"created" => array(
			"sqlType" => "timestamp",
			"notNull" => true,
			"default" => "CURRENT_TIMESTAMP"
		),
		"changed" => array(
			"sqlType" => "timestamp",
			"onUpdate" => "CURRENT_TIMESTAMP"
		),
		"removed" => array(
			"sqlType" => "timestamp"
		),
		"time" => array(
			"sqlType" => "varchar",
			"length" => 50
		),
		"youtube" => array(
			"sqlType" => "varchar",
			"length" => 100
		),
		"locale" => array(
			"sqlType" => "varchar",
			"length" => 3
		)
	);

	protected $sqlKeyNames = array(
		"pri" => "PRI",
		"primary" => "PRI"
	);

	protected $fields;

	protected function validateFields()
	{
		$this->addLogFields();

		foreach ($this->fields AS $name => & $props)
			$this->validateField($name, $props);
	}

	protected function validateField($name, & $field)
	{
		if (!is_array($field)) {
			$field = array(
				"type" => $field,
			);
		}

		if (in_array($field["type"], array_keys($this->fieldTypeTemplates)))
			$field = array_merge($this->fieldTypeTemplates[$field["type"]], $field);
		else
			$field["sqlType"] = $field["type"];

		$field = array_extend($this->fieldTemplate, $field);

		$this->addFieldSqlProperties($name, $field);

		if (strtolower($field["key"]) == "primary")
			$this->setPrimaryKey($name);

		if ($field["locale"])
			$this->hasLocale = true;
	}

	protected function addFieldSqlProperties($name, & $props)
	{
		$props["sql"] = array(
			"Field" => $name,
			"Type" => $props["sqlType"] . ($props["length"] ? "(" . implode(",", (array)$props["length"]) . ")" : null),
			"Null" => $props["notNull"] ? "NO" : "YES",
			"Default" => $props["default"],
			"Key" => in_array(strtolower($props["key"]), array_keys($this->sqlKeyNames)) ? $this->sqlKeyNames[$props["key"]] : "",
			"Extra" => @implode(" ", array_filter(array(($props["autoIncrement"] ? "auto_increment" : null), ($props["onUpdate"] ? "ON UPDATE {$props["onUpdate"]}" : null))))
		);
	}
	/*	 * ***************************************** Repository methods ********************************************* */

	public function __toString()
	{
		return $this->getName();
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getNameLocale($locale = null)
	{
		if (!isset($locale))
			$locale = $this->translator->getLocale();

		return "{$this->nameLocale}_{$locale}";
	}

	public function setNameLocale($nameLocale)
	{
		$this->nameLocale = $nameLocale;
	}

	protected function hasLocale()
	{
		return $this->hasLocale;
	}

	protected function getFields()
	{
		$fields = array();

		foreach ($this->fields AS $key => $field) {
			if (!$field["locale"]) {
				$fields[$key] = $field;
			}
		}

		return $fields;
	}

	protected function getFieldsLocale($includePrimaryKey = false)
	{
		$fields = array();

		foreach ($this->fields AS $key => $field) {
			if ($field["locale"] || ($includePrimaryKey && strtolower($field["key"]) == "primary")) {
				$fields[$key] = $field;
			}
		}

		return $fields;
	}

	protected function filterFields($key, $fieldsFilter = null)
	{
		return !$fieldsFilter || in_array($key, $fieldsFilter);
	}
	protected $primaryKey;

	public function getPrimaryKey()
	{
		return $this->primaryKey;
	}

	public function setPrimaryKey($primaryKey)
	{
		$this->primaryKey = $primaryKey;
	}

	private function getPrimaryKeyProperties($isLocale = false)
	{
		$props = $this->fields[$this->getPrimaryKey()];

		if ($isLocale) {
			$props["autoIncrement"] = false;
			$props["sql"]["Extra"] = null;
		}

		return $props;
	}

	private function filterWhere($where, $locale = null)
	{
		$filteredWhere = array();

		$where = array_filter($where);

		foreach ($where AS $key => $condition) {
			if (is_numeric($key) || $this->validLocale($locale, $this->fields[$this->getFieldName($key)]))
				$filteredWhere[$key] = $condition;
		}

		return $filteredWhere;
	}

	private function getFieldName($name)
	{
		$name = explode(" ", $name);

		return $name[0];
	}
	/*	 * ***************************************** SQL builder ********************************************* */
	private $nonQuotedValues = array(
		"CURRENT_TIMESTAMP",
		"NOW()",
		"NULL"
	);

	public function getSqlUpdateValues($values, $locale)
	{
		$sql = array();

		foreach ($values AS $field => $value) {
			$sql[] = ($locale ? $this->getNameLocale($locale) : $this->getName()) . ".`{$field}` = {$this->getSqlQuote($value, $field)}";
		}

		return count($sql) ? " SET " . implode(", ", $sql) : null;
	}

	public function getSqlQuote($value, $field = null)
	{
		if((isset($field) && array_key_exists($field, $this->fields) && !$this->fields[$field]["notNull"] && $value == null) || $value === null) $value = "NULL";

		if (in_array(strtoupper($value), $this->nonQuotedValues))
			return strtoupper($value);
		elseif (is_numeric($value))
			return $value;
		else
			return $this->database->getConnection()->quote($value);
	}

	public function getSqlWhere($where, $locale = null, $includeRemoved = false)
	{
		if (is_array($where)) {
			foreach ($where AS $key => $value) {
				if (is_numeric($key))
					$condition[] = $value;
				else {
					$name = $this->getFieldName($key); //toto je kvůli NOT

					if ($value === null){
						$value = " IS NULL";
					} elseif (is_array($value)) {
						$in = array_filter($value);

						if (count($in))
							$value = " IN ('" . implode("','", $in) . "')";
						else {
							$value = "!=" . ($this->fields[$name]["locale"] ? $this->getNameLocale($locale) : $this->getName()) . "." . $name;
							//TODO jak řešit prázdné NOT IN
						}
					} elseif (strtolower(substr($value, 0, 7)) == "(select") {
						$value = " IN {$value}";
					} else {
						$value = "=" . $this->getSqlQuote($value, $key);
					}
			
					$where[$key] = ($this->fields[$name]["locale"] ? $this->getNameLocale($locale) : $this->getName()) . "." . "{$key}{$value}";
				}
			}
		}

		if (!$this->includeRemoved && !$includeRemoved)
			$where["removed"] = "{$this->getName()}.removed IS NULL";

		return count($where) ? " WHERE (" . implode(") AND (", $where) . ")" : null;
	}

	protected function getSqlLimit($limit)
	{
		if (is_array($limit))
			$limit = implode(",", $limit);

		if ($limit)
			$limit = " LIMIT {$limit} ";
		else
			$limit = null;

		return $limit;
	}

	protected function getSqlOrder($order = null)
	{
		if (is_array($order)) {
			$_order = array();

			foreach ($order AS $field => $dir) {
				if (is_numeric($field)) {
					$_order[] = implode(" ", (array)$dir);
				} else {
					$_order[] = $field . " " . strtoupper($dir);
				}
			}

			$order = implode(", ", $_order);
		}

		if ($order)
			$order = " ORDER BY {$order} ";
		else
			$order = null;

		return $order;
	}

	public function getSqlFields($fieldsFilter = null)
	{
		$fields = $this->_getSqlFields($fieldsFilter, false);

		return $fields;
	}

	protected $queryAdditionalFields;

	public function getSqlAdditionalFields($additionalFields)
	{
		$this->queryAdditionalFields = $additionalFields;

		$additionalFields = array_filter((array)$additionalFields);

		return $additionalFields ? ", " . implode(",", $additionalFields) : null;
	}

	public function getSqlFieldsLocale($locale, $fieldsFilter = null)
	{
		$fields = $this->_getSqlFields($fieldsFilter, $locale);

		return $fields;
	}

	protected function _getSqlFields($fieldsFilter = null, $locale = false)
	{
		$fields = array();

		foreach ($this->fields AS $key => $field) {
			if (($locale ? $field["locale"] : !$field["locale"]) && $this->filterFields($key, $fieldsFilter)) {
				$fields[$key] = ($locale ? $this->getNameLocale($locale) : $this->getName()) . ".{$key}";
			}
		}

		return implode(", ", array_filter($fields));
	}

//TODO - cizí klíče
	private function getSqlColumn($name, $props = null, $create = false)
	{
		$sql = array(
			"`{$name}`",
			($props ? "{$props["sql"]["Type"]}" : null),
			($props ? ($props["sql"]["Null"] == "YES" ? "NULL" : "NOT NULL") : null),
			($props && strlen($props["sql"]["Default"]) ? "DEFAULT {$this->getSqlQuote($props["sql"]["Default"], $name)}" : null),
			($props ? $props["sql"]["Extra"] : null),
			($props && $props["key"] ? ", " . (!$create ? "ADD " : null) . "{$props["key"]} KEY (`{$name}`)" : null) //TODO - jak řešit klíče při modifikaci
		);

		return implode(" ", $sql);
	}
	/*	 * ***************************************** SQL Table validator ********************************************* */

	private function validateTable()
	{
		$validateRows = false;

		if (!$this->tableExists($this->getName())) {
			$this->createTable($this->getName());

			$validateRows = true;
		}

		if ($this->hasLocale()) {
			foreach ($this->translator->getAvailableLocales() AS $locale) {
				if (!$this->tableExists($this->getNameLocale($locale)))
					$this->createTable($this->getNameLocale($locale), true);
			}
		}

		$this->validateColumns();

		if ($validateRows || $this->forceRowsValidation)
			$this->validateDefaultRows();
	}

//TODO - nelze řešit jednoduššeji?
	private function tableExists($tableName)
	{
		$exists = $this->database->query("SHOW TABLES LIKE '{$tableName}'")->fetchAll();

		return count($exists) ? true : false;
	}

	private function createTable($tableName, $isLocale = false)
	{
		$primaryKeyName = $this->getPrimaryKey();

		$primaryKeyProps = $this->getPrimaryKeyProperties($isLocale);

		$sqlPrimaryKey = $this->getSqlColumn($primaryKeyName, $primaryKeyProps, true);

		$result = $this->database->query("CREATE TABLE `{$tableName}` ({$sqlPrimaryKey}) ENGINE=MyISAM DEFAULT CHARSET=utf8");

		return $result;
	}

	private function validateColumns()
	{
		$deprecatedFound = false;

		foreach ($this->fields AS $name => $props) {
			if ($props["deprecated"]) {
				if ($props["locale"] || $props["primary"]) {
					foreach ($this->translator->getAvailableLocales() AS $locale) {
						if ($this->columnExists($this->getNameLocale($locale), $props["deprecated"])) {
							$this->renameColumn($this->getNameLocale($locale), $props["deprecated"], $name, $props);

							$deprecatedFound = true;
						} else {
							//dump("Deprecated column {$props["deprecated"]} no more exists in table {$this->getNameLocale($locale)} for " . get_class($this) . " !");
						}
					}
				}

				if (!$props["locale"]) {
					if ($this->columnExists($this->getName(), $props["deprecated"])) {
						$this->renameColumn($this->getName(), $props["deprecated"], $name, $props);

						$deprecatedFound = true;
					} else {
						//dump("Deprecated column {$props["deprecated"]} no more exists in table {$this->getName()} for " . get_class($this) . " !");
					}
				}
			}

			$this->fields[$name]["deprecated"] = null;
		}

		if ($deprecatedFound)
			$this->getColumns(true);

		foreach ($this->fields AS $name => $props) {
			if ($props["locale"] || ($this->hasLocale() && (strtolower($props["key"]) == "primary"))) {
				$correctedProps = strtolower($props["key"]) == "primary" ? $this->getPrimaryKeyProperties(true) : $props;

				foreach ($this->translator->getAvailableLocales() AS $locale) {
					$this->validateColumn($this->getNameLocale($locale), $name, $correctedProps);
				}
			}

			if (!$props["locale"])
				$this->validateColumn($this->getName(), $name, $props);
		}

		$this->removeUnusedColumns();
	}

	private function validateColumn($tableName, $name, $props)
	{
		if (!$this->columnExists($tableName, $name, $props)) {
			$this->database->query("ALTER TABLE `{$tableName}` ADD COLUMN " . $this->getSqlColumn($name, $props));
		} else {
			$columnProps = $this->columns[$tableName][$name];

			$modified = false;

			foreach ($columnProps AS $prop => $value) {
				if (strtolower($value) != strtolower($props["sql"][$prop])) {
					$this->database->query("ALTER TABLE `{$tableName}` MODIFY COLUMN " . $this->getSqlColumn($name, $props));

					$modified = true;

					break;
				}
			}

			/* if($modified){
			  dump($columnProps);
			  dump($props["sql"]);
			  } */
		}
	}

	private function removeUnusedColumns()
	{
		$tables = array(
			$this->getName() => false
		);

		if ($this->hasLocale()) {
			foreach ($this->translator->getAvailableLocales() AS $locale) {
				$tables[$this->getNameLocale($locale)] = true;
			}
		}

		foreach ($tables AS $tableName => $isLocale) {
			$fields = $isLocale ? $this->getFieldsLocale(true) : $this->getFields();

			foreach (array_keys($this->columns[$tableName]) AS $name) {
				if (!in_array($name, array_keys($fields)))
					$this->database->query("ALTER TABLE `{$tableName}` DROP COLUMN " . $this->getSqlColumn($name));
			}
		}
	}
	protected $columns;

	protected function getColumns($forceLoad = false)
	{
		if (!$this->columns || $forceLoad) {
			$this->columns = array();

			$this->columns[$this->getName()] = $this->database->query("SHOW COLUMNS FROM `{$this->getName()}`")->fetchPairs("Field");

			if ($this->hasLocale()) {
				foreach ($this->translator->getAvailableLocales() AS $locale) {
					$this->columns[$this->getNameLocale($locale)] = $this->database->query("SHOW COLUMNS FROM `{$this->getNameLocale($locale)}`")->fetchPairs("Field");
				}
			}
		}

		return $this->columns;
	}

	protected function columnExists($tableName, $name)
	{
		$columns = $this->getColumns();

		return in_array($name, array_keys($columns[$tableName]));
	}

	protected function renameColumn($tableName, $oldName, $newName, $props)
	{
		return $this->database->query("ALTER TABLE `{$tableName}` CHANGE `{$oldName}` " . $this->getSqlColumn($newName, $props));
	}

	public function sqlNotRemoved()
	{
		return "removed IS NULL";
	}

	public function sqlCreated()
	{
		return "created";
	}
	/*	 * ***************************************** Add default rows ********************************************* */

	protected function validateDefaultRows()
	{
		foreach ($this->defaultRows AS $row) {
			if (!$this->exists($row[$this->getPrimaryKey()])) {
				$this->save($row);
			}
		}
	}
}