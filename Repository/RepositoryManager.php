<?php

namespace CP\Repository;

use Nette;
use	CP;

class Manager {
	private $database;

	private $translator;

	private $user;

	function __construct(Nette\Database\Context $database, $user, Nette\Localization\ITranslator $translator){
		$this->database = $database;

		$this->translator = $translator;

		$this->user = $user;
	}

	function register($repositoryKey) {
		$className = "Repository\\{$repositoryKey}";

		$this->$repositoryKey = new $className($this->database, $this->user, $this->translator);
	}

	function __get($repositoryKey){
		if(!property_exists($this, $repositoryKey)){
			$this->register($repositoryKey);
		}

		return $this->$repositoryKey;
	}
}