<?php

namespace CP\Forms\InvoiceForm;

trait Validation
{
	protected function addCustomerValidation()
	{
		$this->form["invoice_customer_name"]
			->addCondition(callback($this, "customerRequiredFrom"))
				->setRequired("Prosím vyplňte jméno odběratele či název firmy");

		$this->form["invoice_bill_address_no"]
			->addCondition(callback($this, "customerRequiredFrom"))
				->setRequired("Prosím vyplňte č.p. odběratele");

		$this->form["invoice_bill_address_postcode"]
			->addCondition(callback($this, "customerRequiredFrom"))
				->setRequired("Prosím vyplňte PSČ odběratele");

		$this->form["invoice_bill_address_city"]
			->addCondition(callback($this, "customerRequiredFrom"))
				->setRequired("Prosím vyplňte město odběratele");
	}

	public function validateTemplateDuringRange($template_ends_at)
	{
		$values = $template_ends_at->parent->getValues(true);

		if($values["template_ends_at"] == null || $values["template_starts_at"] == null || (strtotime($values["template_ends_at"]) >= strtotime($values["template_starts_at"])))
			return true;
		else
			return false;
	}

	public function customerRequiredFrom($input)
	{
		$values = $input->parent->getValues();

		$sum = 0;

		foreach($values["items"] AS $item){
			$sum += $item["item_amount"] * $item["item_price_main"];
		}

		return $sum > $this->presenter->invoicesService->settings["customerRequiredFrom"] ? true : false;
	}
}