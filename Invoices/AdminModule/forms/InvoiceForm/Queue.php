<?php

namespace CP\Forms\InvoiceForm;

trait Queue
{
	protected function addInvoiceQueue()
	{
		if(count($this->presenter->getQueueOptions()) > 1){
			$this->form->addSelect("invoice_queue", null, $this->presenter->getQueueOptions());

			$this->form->addSubmit("set_queue")
				->onClick[] = callback($this, "setQueue");
		} else {
			$this->form->addHidden("invoice_queue");
		}
	}

	public function setQueue($button)
	{
		$form = $button->parent;

		$values = $form->getValues(true);

		$form["invoice_no"]->value = $this->presenter->invoicesService->getInvoiceNo($values);

		$form["invoice_vs"]->value = $this->presenter->invoicesService->getInvoiceVs($values);

		if(isset($this->presenter->invoicesService->settings["queues"][$values["invoice_queue"]]["invoice_bank_account"]))
			$form["invoice_bank_account"]->value = $this->presenter->invoicesService->settings["queues"][$values["invoice_queue"]]["invoice_bank_account"];
	}
}