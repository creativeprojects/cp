<?php

namespace CP\Forms\InvoiceForm;

use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

trait Items
{
	protected function addItems()
	{
		$parent = $this;

		$container = $this->form->addDynamic("items", function (Container $container) use ($parent) {
			$container->addHidden("id");

			$container->addText("item_title");

			$container->addTextArea("item_description");

			$container->addText("item_amount");

			$container->addText("item_amount_unit");

			$container->addText("item_price_main");

			$container->addText("item_price_vat");

			$container->addSubmit("remove")
				->setValidationScope(false)
				->onClick[] = callback($parent, "removeItem");
		});

		$container->addSubmit('recalculate')
			->setValidationScope(false)
			->onClick[] = callback($this, "recalculateItems");

		$container->addSubmit('truncate')
			->setValidationScope(false)
			->onClick[] = callback($this, "truncateItems");

		$container->addSubmit('add')
			->setValidationScope(false)
			->onClick[] = callback($this, "addItem");
	}

	public function addItem($button)
	{
		$values = $this->presenter->invoicesService->getNewItem($this->values);

		$button->parent->createOne()
			->setValues($values);

		$this->invalidateControl();

		$this->presenter->flashMessage("Nová položka byla přidána", "alert-success");
	}

	public function removeItem($button)
	{
		$items = $button->parent->parent;

		$items->remove($button->parent, TRUE);

		$this->invalidateControl();

		$this->presenter->flashMessage("Položka byla odebrána z dokladu", "alert-success");
	}

	public function recalculateItems($button)
	{
		$this->invalidateControl();
		
		$this->presenter->flashMessage("Položky dokladu byly přepočítány", "alert-success");
	}

	public function truncateItems($button)
	{
		$items = $button->parent;

		foreach($items->containers AS $item){
			$items->remove($item, TRUE);
		}

		$this->invalidateControl();

		$this->presenter->flashMessage("Doklad byl vyprázdněn", "alert-success");
	}
}