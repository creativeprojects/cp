<?php

namespace CP\Forms\InvoiceForm;

use Nette\Forms\Controls\SubmitButton;

trait Save
{
	public function save(SubmitButton $button)
	{
		$values = $button->parent->getValues(true);

		$this->AddContactToAddressBook($values);

		if($this->type == "regular"){
			$itemsPriceIncludeVatSum = $this->presenter->invoicesService->getItemsPriceIncludeVatSum($values["items"]);

			$values["invoice_remain_charge"] += $itemsPriceIncludeVatSum - $this->itemsPriceIncludeVatSum;

			$values = $this->presenter->invoicesService->save($values);
		} else {
			$values = $this->presenter->invoicesService->saveTemplate($values);
		}

		if ($values) {
			$this->presenter->flashMessage($this->flashMessage[$this->type]["success"], "alert-success");

			if($this->type == "regular")
				$this->presenter->redirect("Invoices:detail", array("invoice_id" => $values["invoice_id"]));
			else{
				$this->presenter->restoreRequest($this->presenter->backlink);
				$this->presenter->redirect("Invoices:default");
			}
		} else {
			$this->presenter->flashMessage($this->flashMessage[$this->type]["error"], "alert-danger");
		}
	}

	protected function addContactToAddressBook($values)
	{
		if($values["invoice_customer_name"] && !$values["contact_id"]){
			$contact = array(
				"contact_name" => $values["invoice_customer_name"],
				"contact_ic" => $values["invoice_customer_id"],
				"contact_dic" => $values["invoice_customer_vat"],
				"contact_phone" => $values["invoice_customer_phone"],
				"contact_email" => $values["invoice_customer_email"],
				"contact_address_street" => $values["invoice_bill_address_street"],
				"contact_address_no" => $values["invoice_bill_address_no"],
				"contact_address_postcode" => $values["invoice_bill_address_postcode"],
				"contact_address_city" => $values["invoice_bill_address_city"],
				"contact_address_country" => @$values["invoice_bill_address_country"],
			);

			$this->presenter->addressBookService->save($contact);
		}
	}
}