<?php

namespace CP\Forms\InvoiceForm;

use CP\Forms\Form;

trait Fields
{
	protected function addFields()
	{
		$this->form->addHidden("invoice_id");
		
		$this->form->addHidden("client_id");
		
		$this->form->addText("invoice_title");

		$this->addInvoiceQueue();

		if($this->type == "regular"){
			$this->form->addText("invoice_no");
			
			$this->form->addHidden("invoice_remain_charge");

			$this->form->addText("invoice_established");

			$this->form->addText("invoice_expire");

			$this->form->addText("invoice_due");

			$this->form->addSelect("invoice_status", null, $this->presenter->invoicesService->settings["statusOptions"]);
		} else {
			$this->form->addText("invoice_expire")
				->addRule(Form::NUMERIC, "Počet dnů splatnosti dokladu musí být číslo");

			$this->form->addText("invoice_due")
				->addRule(Form::NUMERIC, "Počet dnů zdanitelného plnění dokladu musí být číslo");

			$this->form->addSelect("invoice_repeat", null, $this->presenter->invoicesService->settings["repeatOptions"]);

			$this->form->addCheckbox("invoice_autosend");

			$this->form->addText("template_starts_at");

			$this->form->addText("template_ends_at")
				->addRule(array($this, "validateTemplateDuringRange"), "Termín ukončení platnosti šablony musí být větší než termín zahájení");
		}

		$this->form->addText("invoice_vs");

		$this->form->addText("invoice_ks");

		$this->form->addText("invoice_ss");

		$this->addInvoiceBankAccount();

		$this->form->addHidden("invoice_type");

		$this->addPaymentSelect();

		$this->addCustomerFields();

		$this->addItems();

		$this->form->addCheckbox("invoice_delegate_vat");

		$this->addCustomerValidation();

		$this->form->addSubmit("save")
			->onClick[] = callback($this, "save");
	}

	protected function addCustomerFields()
	{
		//if($this->presenter->user->isInRole("admin") || $this->presenter->user->isInRole("admin"))
		//	$this->form->addSelect("user_id", null, $this->presenter->usersService->getCustomerOptionsByClient());
		//else
			$this->form->addHidden("user_id");

		$this->form->addHidden("contact_id");
		
		$this->form->addText("invoice_customer_name");
		
		$this->form->addText("invoice_customer_id");
		
		$this->form->addText("invoice_customer_vat");
		
		$this->form->addText("invoice_customer_phone");

		$this->form->addText("invoice_customer_email");

		$this->form->addText("invoice_bill_address_street");
		
		$this->form->addText("invoice_bill_address_no");

		$this->form->addText("invoice_bill_address_postcode");
		
		$this->form->addText("invoice_bill_address_city");

		$this->addCountrySelect();
	}

	protected function addInvoiceBankAccount()
	{
		if(count($this->presenter->getBankAccountOptions()) > 1)
			$this->form->addSelect("invoice_bank_account", null, $this->presenter->getBankAccountOptions());
		else
			$this->form->addHidden("invoice_bank_accunt");
	}

	protected function addCountrySelect()
	{
		if($this->presenter->client["selectCountry"]){
			$this->form->addSelect("invoice_bill_address_country", null, $this->presenter->regionsService->getCountries());
		} else {
			$this->form->addHidden("invoice_bill_address_country");
		}
	}

	protected function addPaymentSelect()
	{
		$paymentOptions = $this->presenter->invoicesService->settings["paymentOptions"][$this->values["invoice_type"]];

		if(count($paymentOptions) > 1)
			$this->form->addSelect("invoice_payment", null, $paymentOptions);
		else
			$this->form->addHidden("invoice_payment");
	}
}