<?php

namespace CP\Forms;

class InvoiceForm extends \CP\Forms\Base
{
	protected $itemsPriceIncludeVatSum = 0;

	protected $formTypes = array("regular", "template");

	protected $type = "regular";

	public $flashMessage = array(
		"regular" => array(
			"success" => "Doklad byl uložen",
			"error" => "Při ukládání dokladu došlo k chybě!"
		),
		"template" => array(
			"success" => "Šablona dokladu byla uložena",
			"error" => "Při ukládání šablony dokladu došlo k chybě!"
		)
	);

	//Nastavení zda-li se jedná o formulář dokladu či šablony
	public function setType($type)
	{
		if(in_array($type, $this->formTypes))
			$this->type = $type;
		else
			throw new \Exception("Zadaný typ formuláře '{$type}' není podporován! Dostupné: " . implode(",", $this->formTypes));

		return $this;
	}

	//Zaslání součtu položek dokladu pro porovnání sumy před a po editaci formulářem pro aktualizaci imvoice_remain_charge
	public function setItemsPriceIncludeVatSum($sum)
	{
		$this->itemsPriceIncludeVatSum = $sum;

		return $this;
	}

	public function setDefaults()
	{
		parent::setDefaults();

		if(!$this->values["invoice_bill_address_country"]){
			$this->form["invoice_bill_address_country"]->setDefaultValue($this->presenter->client["defaultCountry"]);
		}
	}

	public function create()
	{
		$this->addFields();
		
		$this->setDefaults();

		return $this->form;
	}

	use \CP\Forms\InvoiceForm\Fields;

	use \CP\Forms\InvoiceForm\Queue;

	use \CP\Forms\InvoiceForm\Validation;

	use \CP\Forms\InvoiceForm\Save;

	use \CP\Forms\InvoiceForm\Items;
}