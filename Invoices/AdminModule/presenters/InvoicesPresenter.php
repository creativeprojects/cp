<?php

namespace CP\AdminModule\Presenters;

use Nette;
use CP\Forms\InvoiceForm;
use CP\Forms\EmailForm;
use Nette\Application\BadRequestException;

trait InvoicesPresenter
{
	/** @var \CP\Services\AddressBook @inject */
	public $addressBookService;
	
	public function renderDefault()
	{
		$invoicesGrid = $this->invoicesService->getActiveGridByClient();

		if ($invoicesGrid) {
			$invoicesGrid->paginator->itemsPerPage = 20;

			$invoicesGrid->create($this);

			$this->template->invoicesGrid = $invoicesGrid;

			$this->template->statusOptions = $this->invoicesService->settings["statusOptions"];

			$this->template->queueOptions = $this->invoicesService->settings["queues"];
		} else
			throw new BadRequestException;
	}

	public function renderArchive()
	{
		$invoicesGrid = $this->invoicesService->getArchiveGridByClient();

		if ($invoicesGrid) {
			$invoicesGrid->paginator->itemsPerPage = 30;

			$invoicesGrid->create($this);

			$this->template->invoicesGrid = $invoicesGrid;

			$this->template->statusOptions = $this->invoicesService->settings["statusOptions"];

			$this->template->queueOptions = $this->invoicesService->settings["queues"];
		} else
			throw new BadRequestException;
	}
	
	public $invoice;

	public function initFormItems($form)
	{
		/*$itemsCount = 0;

		foreach ($form["items"]->components AS $key => $item) {
			if (is_numeric($key)) {
				$itemsCount++;
			}
		}*/

		/*if (!$itemsCount) {
			dump($this->invoiceForm);

			die;
			$this->form->createItem($form["items"]);
			//$form["items"]->createOne();
		}*/
	}

	public function actionDetail($invoice_id)
	{
		if ($invoice = $this->invoicesService->getOneById($invoice_id)) {
			$this->template->invoice = $invoice;

			$this->template->document = $this->invoicesService->getDocument($invoice);

			$this->template->html = $this->invoicesService->getHtml($invoice);
		} else
			throw new BadRequestException;
	}

	public function actionEdit($invoice_id)
	{
		if ($this->invoice = $this->invoicesService->getOneById($invoice_id)) {
			$this->template->invoice = $this->invoice;
		} else
			throw new BadRequestException;
	}

	public function handleAdd($invoice_type)
	{
		if (in_array($invoice_type, array_keys($this->invoicesService->settings["typeOptions"]))) {
			$invoice = array(
				"invoice_type" => $invoice_type
			);

			$invoice = $this->invoicesService->create($invoice);

			$this->redirect('Invoices:add', $invoice["invoice_id"]);
		} else
			throw new BadRequestException;
	}

	public function actionAdd($invoice_id)
	{
		$this->actionEdit($invoice_id);
	}

	protected function createComponentInvoiceForm()
	{
		$invoiceForm = new InvoiceForm($this, "invoiceForm");

		$invoiceForm->setValues($this->invoice);

		$invoiceForm->setItemsPriceIncludeVatSum($this->invoicesService->getItemsPriceIncludeVatSum($this->invoice["items"]));

		return $invoiceForm->create();
	}

	public function handleRemove($invoice_id)
	{
		if ($invoice = $this->invoicesService->getOneById($invoice_id)) {
			$this->invoicesService->remove($invoice_id);

			$this->flashMessage("Vybraný doklad byl odebrán", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Invoices:default");
		} else {
			$this->flashMessage("Doklad nebyl nalezen!", "alert-danger");

			$this->redirect("this");
		}
	}

	public function handleCreateMain($invoice_id)
	{
		if ($invoice = $this->invoicesService->createMain($invoice_id)) {
			$this->flashMessage("Daňový doklad byl vytvořen", "alert-success");

			$this->redirect("Invoices:edit", array("invoice_id" => $invoice["invoice_id"]));
		} else {
			$this->flashMessage("Vybraný doklad byl odebrán", "alert-success");
		}
	}

	public function handleDuplicate($invoice_id)
	{
		if ($invoice = $this->invoicesService->duplicate($invoice_id)) {
			$this->flashMessage("Vybraný doklad byl duplikován", "alert-success");

			$this->redirect("Invoices:edit", array("invoice_id" => $invoice["invoice_id"]));
		} else {
			$this->flashMessage("Vybraný doklad se nepodařilo duplikovat", "alert-danger");

			$this->redirect("this");
		}
	}

	protected function createComponentStatusForm()
	{
		$form = new \CP\Forms\Form;

		$form->addHidden("invoice_id");

		$form->addSelect("invoice_status", null, $this->invoicesService->settings["statusOptions"]);

		$form->addSubmit("set");

		$form->onSuccess[] = $this->statusFormSubmit;

		return $form;
	}

	public function statusFormSubmit($form)
	{
		$values = $form->getValues();

		if ($this->invoicesService->repository->invoices->saveField($values["invoice_id"], "invoice_status", $values["invoice_status"])) {
			$this->flashMessage("Stav dokladu byl uložen", "alert-success");
		} else {
			$this->flashMessage("Stav dokladu se nepodařilo uložit!", "alert-danger");
		}

		$this->redirect("this");
	}

	public function actionSend($invoice_id)
	{
		if ($invoice = $this->invoicesService->getOneById($invoice_id)) {
			$this->invoice = $invoice;

			$this->template->invoice = $this->invoice;
		} else
			throw new BadRequestException;
	}

	protected function createComponentEmailForm()
	{
		$form = new EmailForm($this);

		$form->setEditable("email_to", true)
			->setEditable("email_subject", true)
			->setEditable("email_message", true)
			->setEditable("email_attachments", true)
			->setLabel("send", "Odeslat doklad emailem")
			->setRequired("email_to", "Zadejte email příjemce")
			->setSubjectTemplate("templates/Invoices/emails/{$this->translator->getLocale()}/{$this->invoice["invoice_type"]}Subject.latte", $this->invoice)
			->setMessageTemplate("templates/Invoices/emails/{$this->translator->getLocale()}/{$this->invoice["invoice_type"]}Message.latte", $this->invoice)
			->setHTMLMessage(true)
			->addAttachment($this->invoice["invoice_document_path"])
			->addTo($this->invoice["invoice_customer_email"], $this->invoice["invoice_customer_name"])
			->setFrom($this->client["client_email"], $this->client["client_title"])
			->setFlashMessage("Doklad byl odeslán", "Doklad se napodařilo odeslat. Zkuste to prosím znovu.")
			->setSentCallback(array($this, "sentCallback"), $this->invoice);

		return $form->create();
	}

	public function sentCallback($invoice)
	{
		$this->invoicesService->updateSentTimestampById($invoice["invoice_id"]);

		if ($this->backlink) {
			$this->restoreRequest($this->backlink);
		} else {
			$this->redirect("Invoices:detail", array("invoice_id" => $invoice["invoice_id"]));
		}
	}

	public function getBankAccountOptions()
	{
		$options = array();

		foreach($this->invoicesService->settings["bankAccounts"] AS $key => $account){
			$options[$key] = "{$account["title"]} ({$account["no"]}/{$account["bank"]})";
		}

		return $options;
	}

	public function getQueueOptions()
	{
		$options = array();

		foreach($this->invoicesService->settings["queues"] AS $key => $queue){
			$options[$key] = "{$queue["title"]}";
		}

		return $options;
	}

	/* Templates */

	public function renderTemplates()
	{
		$invoicesGrid = $this->invoicesService->getTemplatesGridByClient();

		if ($invoicesGrid) {
			$invoicesGrid->paginator->itemsPerPage = 20;

			$invoicesGrid->create($this);

			$this->template->invoicesGrid = $invoicesGrid;

			$this->template->queueOptions = $this->invoicesService->settings["queues"];
		} else
			throw new BadRequestException;
	}

	public function actionAddTemplate($invoice_id)
	{
		$this->actionEditTemplate($invoice_id);
	}

	public function handleAddTemplate($invoice_type)
	{
		if (in_array($invoice_type, array_keys($this->invoicesService->settings["typeOptions"]))) {
			$invoice = array(
				"invoice_type" => $invoice_type
			);

			$invoice = $this->invoicesService->createTemplate($invoice);

			$this->redirect('Invoices:addTemplate', array($invoice["invoice_id"], "backlink" => $this->backlink));
		} else
			throw new BadRequestException;
	}

	public function actionEditTemplate($invoice_id)
	{
		if ($this->invoice = $this->invoicesService->getOneTemplateById($invoice_id)) {
			$this->template->invoice = $this->invoice;
		} else
			throw new BadRequestException;
	}

	public function handleCreateRegular($invoice_id)
	{
		if ($invoice = $this->invoicesService->createRegular($invoice_id)) {
			$this->flashMessage("Doklad ze šablony byl vytvořen", "alert-success");

			$this->redirect("Invoices:edit", array("invoice_id" => $invoice["invoice_id"]));
		} else {
			$this->flashMessage("Doklad se ze šablony nepodařilo vytvořit!", "alert-danger");

			$this->redirect("this");
		}
	}

	public function handleDuplicateTemplate($invoice_id)
	{
		if ($invoice = $this->invoicesService->duplicateTemplate($invoice_id)) {
			$this->flashMessage("Vybraná šablona byla duplikována", "alert-success");

			$this->redirect("Invoices:editTemplate", array("invoice_id" => $invoice["invoice_id"]));
		} else {
			$this->flashMessage("Vybranou šablonu se nepodařilo duplikovat", "alert-danger");

			$this->redirect("this");
		}
	}

	public function handleRemoveTemplate($invoice_id)
	{
		if ($invoice = $this->invoicesService->getOneTemplateById($invoice_id)) {
			$this->invoicesService->remove($invoice_id);

			$this->flashMessage("Šablona dokladu byla odebrána", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Invoices:templates");
		} else {
			$this->flashMessage("Šablona dokladu nenalezena!", "alert-danger");

			$this->redirect("this");
		}
	}

	protected function createComponentTemplateForm()
	{
		$form = new InvoiceForm($this);

		$form->setValues($this->invoice);

		$form->setType("template");

		$form->setItemsPriceIncludeVatSum($this->invoicesService->getItemsPriceIncludeVatSum($this->invoice["items"]));

		return $form->create();
	}
}