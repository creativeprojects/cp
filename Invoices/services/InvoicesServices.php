<?php

namespace CP\Services;

class Invoices extends \CP\Base\Service
{
	var $settings = array(
		"defaultVat" => 21,
		"customerRequiredFrom" => 10000,
		"default" => array(
			"invoice_type" => "main",
			"invoice_expire" => 14,
			"invoice_due" => 0,
			"invoice_repeat" => "norepeat",
		),
		"typeOptions" => array(
			"proforma" => "Výzva k platbě",
			"main" => "Daňový doklad"
		),
		"paymentOptions" => array(
			"main" => array(
				"cash" => "Hotově",
				"card" => "Platební kartou",
				"payu" => "Online platbou",
				"ondelivery" => "Dobírkou"
			),
			"proforma" => array(
				"gate" => "Převodem"
			)
		),
		"statusOptions" => array(
			"active" => "Vystaveno",
			"paid" => "Uhrazeno",
			"cancelled" => "Stornováno"
		),
		"repeatOptions" => array(
			"norepeat" => "Neopakovat",
			"daily" => "Každý den",
			"weekly" => "Každý týden",
			"monthly" => "Každý měsíc",
			"annualy" => "Každý rok"
		),
		"repeatTresholds" => array(
			"norepeat" => false,
			"daily" => "-1 day",
			"weekly" => "-1 week",
			"monthly" => "-1 month",
			"annualy" => "-1 year"
		),
		"pdfDir" => "files/invoices",
		"queues" => array(
			0 => "Výchozí"
		),
		"bankAccounts" => array()
	);

	use \CP\Services\Invoices\Grids;

	use \CP\Services\Invoices\GetOne;

	use \CP\Services\Invoices\CRUD;

	use \CP\Services\Invoices\Document;

	use \CP\Services\Invoices\SyncTransactions;

	use \CP\Services\Invoices\Send;

	use \CP\Services\Invoices\Repeated;
}