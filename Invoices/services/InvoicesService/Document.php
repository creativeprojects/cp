<?php

namespace CP\Services\Invoices;

use Joseki\Application\Responses\PdfResponse;
use Nette\Utils\Strings;

trait Document
{
	protected function getInvoice($invoice)
	{
		if (is_numeric($invoice))
			$invoice = $this->getOneByIdLocale($invoice);

		return $invoice;
	}

	public function createPdf($invoice)
	{
		$invoice = $this->getInvoice($invoice);

		$document = $this->getDocument($invoice);

		$parameters = array(
			"client" => $this->client,
			"document" => $document
		);

		$html = $this->template
			->create()
			->setDir("templates/Invoices/documents")
			->setParameters($parameters)
			->setFile("pdf.latte")
			->process();

		$pdf = new PdfResponse($html);

		$pdf->setSaveMode(PdfResponse::INLINE);

		$pdfName = Strings::webalize($invoice["invoice_no"]);

		$pdfPath = $this->getPDFDir();

		$path = $pdf->save(WWW_DIR . "/{$pdfPath}/", $pdfName);

		return "{$pdfPath}/{$pdfName}.pdf";
	}

	public function savePdf($invoice)
	{
		$invoice = $this->getInvoice($invoice);

		$invoice_document_path = $this->createPdf($invoice);

		$this->repository->invoices->saveField($invoice["invoice_id"], "invoice_document_path", $invoice_document_path);

		return $invoice_document_path;
	}

	public function getHtml($invoice)
	{
		if (is_numeric($invoice))
			$invoice = $this->getOneByIdLocale($invoice);

		$document = $this->getDocument($invoice);

		$parameters = array(
			"client" => $this->client,
			"document" => $document
		);

		$html = $this->template
			->create()
			->setDir("templates/Invoices/documents")
			->setParameters($parameters)
			->setFile("pdf.latte")
			->process();

		return $html;
	}

	private function getPDFDir()
	{
		$pdfDir = $this->client["dir"] . "/" . $this->settings["pdfDir"];

		$this->filesService->validateDir($pdfDir);

		return $pdfDir;
	}

	public function getDocument($invoice)
	{
		$parameters = $this->getDocumentParameters($invoice);

		$html = $this->template
			->create()
			->setDir("templates/Invoices/documents")
			->setParameters($parameters)
			->setFile($invoice["invoice_type"])
			->process();

		return $html;
	}

	public function getDocumentParameters($invoice)
	{
		$parameters = array(
			"currency" => $this->regionsService->getCurrency(),
			"countries" => $this->regionsService->getCountryOptions(),
			"invoice" => $invoice,
			"paymentOptions" => $this->settings["paymentOptions"],
			"client" => $this->client,
		);

		return $parameters;
	}
}