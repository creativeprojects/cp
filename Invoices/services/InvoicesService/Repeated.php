<?php

namespace CP\Services\Invoices;

trait Repeated
{
	protected $invoice_repeatIntervals = array(
		"daily" => "-1 day",
		"weekly" => "-1 week",
		"monthly" => "-1 month",
		"annualy" => "-1 year"
	);

	public function checkRepeated()
	{
		$log = array();

		$query = array(
			"where" => array(
				"client_id" => $this->client["id"],
				"invoice_repeat!" => "norepeat",
				"template_starts_at IS NULL OR template_starts_at <= CURDATE()",
				"template_ends_at IS NULL OR template_ends_at >= CURDATE()",
			),
			"fields" => array(
				"invoice_id",
				"invoice_title",
				"invoice_repeat",
				"invoice_autosend"
			),
			"additionalFields" => array(
				"last_established" => "(SELECT invoice_established FROM {$this->repository->invoices} WHERE template_invoice_id = {$this->repository->invoicesTemplates}.invoice_id ORDER BY invoice_type DESC, invoice_established DESC LIMIT 1) AS last_established"
			)
		);
		
		$templates = $this->repository->invoicesTemplates->getAllBy($query);

		$templatesCount = count($templates);

		$log[] = alert("Nalezeno <strong>{$templatesCount}</strong> aktivních šablon pravidelných dokladů");

		foreach($templates AS $template){
			$lastEstablishedTime = strtotime($template["last_established"]);

			$repeatTreshold = strtotime($this->invoice_repeatIntervals[$template["invoice_repeat"]]);

			//$log[] = alert("Vystaveno: " . date("j.n.Y", $lastEstablishedTime) . ", treshold: " . date("j.n.Y", $repeatTreshold));

			if($lastEstablishedTime <= $repeatTreshold){
				$invoice = $this->createRegular($template["invoice_id"]);

				if($invoice)
					$log[] = alert("Vytvořen doklad č. <strong>{$invoice["invoice_no"]} - {$invoice["invoice_title"]}</strong>", true);
				else
					$log[] = alert("Doklad <strong>{$template["invoice_title"]}</strong> se nepodařilo vytvořit", false);

				if($template["invoice_autosend"]){
					$result = $this->send($invoice);

					if($result)
						$log[] = alert("Doklad <strong>{$invoice["invoice_no"]} - {$invoice["invoice_title"]}</strong> byl automaticky odeslán odběrateli", true);
					else
						$log[] = alert("Doklad <strong>{$invoice["invoice_no"]} - {$invoice["invoice_title"]}</strong> se nepodařilo automaticky odeslat odběrateli", false);
				}
			} else {
				$log[] = alert("Doklad dle šablony <strong>{$template["invoice_title"]}</strong> je aktuálně vystaven");
			}
		}

		return $log;
	}
}