<?php

namespace CP\Services\Invoices;

use Nette\Utils\Validators;

trait Send
{
	public function send($invoice)
	{
		$invoice = $this->getInvoice($invoice);

		$locale = in_array(@$invoice["invoice_customer_locale"], $this->translator->getAvailableLocales()) ? $invoice["invoice_customer_locale"] : $this->translator->getLocale();

		$invoiceType = strtolower($invoice["invoice_type"]);

		$result = $this->emailsService->message()
			//->setLocale($locale)
			->setTo($invoice["invoice_customer_email"])
			->setFrom($this->client['client_email'], $this->client['client_title'])
			->setSubjectTemplate(APP_DIR . "/templates/Invoices/emails/{$locale}/{$invoiceType}Subject.latte", $invoice)
			->setMessageTemplate(APP_DIR . "/templates/Invoices/emails/{$locale}/{$invoiceType}Message.latte", $invoice)
			->addAttachment($invoice["invoice_document_path"])
			->save();

		if(Validators::is(@$this->settings["sendCopyTo"], "email")){
			$this->emailsService->message()
				//->setLocale($locale)
				->setTo($this->client['client_email'])
				->setFrom($this->client['client_email'], $this->client['client_title'])
				->setSubjectTemplate(APP_DIR . "/templates/Invoices/emails/{$locale}/{$invoiceType}ForClientSubject.latte", $invoice)
				->setMessageTemplate(APP_DIR . "/templates/Invoices/emails/{$locale}/{$invoiceType}ForClientMessage.latte", $invoice)
				->addAttachment($invoice["invoice_document_path"])
				->save();
		}

		return $result;
	}

	public function updateSentTimestampById($invoice_id)
	{
		if($invoice_id){
			$this->repository->invoices->saveField($invoice_id, "invoice_sent", now());

			return true;
		}

		return false;
	}
}