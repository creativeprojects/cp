<?php

namespace CP\Services\Invoices;

trait SyncTransactions
{
	protected $syncLog;

	protected $account;

	public function syncTransactions($reset = false)
	{
		$this->syncLog = array();

		foreach($this->settings["bankAccounts"] AS $account){
			
			$this->account = $account;

			switch($account["bank"])
			{
				case 2010: $this->syncFioTransactions($reset); break;
				default: $this->syncLog[] = alert("Pro kód banky {$account["bank"]} není vytvořen synchronizační algorytmus", false);
			}
		}

		return $this->syncLog;
	}

	protected function syncFioTransactions($reset = false)
	{
		$this->syncLog[] = alert("Synchronizace příchozích plateb pro FIO účet {$this->account["title"]} ({$this->account["no"]}/{$this->account["bank"]})");
			
		$fio = new \Fio\Api($this->account["token"]);

		//Reset se používá pouze při testování, jinak se automaticky nastavuje počáteční datum od data poslední stažení dat
		if($reset){
			$fio->reset($reset);
		}

		$incomes = $fio->getIncomes();

		$incomesCount = count($incomes);

		$this->syncLog[] = alert("Načteno {$incomesCount} příchozích plateb");

		foreach($incomes AS $income){

			//TODO - kontrolovat platbu podle $income["ident"]

			if($invoice = $this->getOneActiveByVs($income["vs"])){
				$this->setPaid($invoice, $income["castka"]);
			}
			else
				$this->syncLog[] = alert("Příchozí platba: {$income["popis_interni"]}, VS: {$income["vs"]}, {$income["castka"]} {$income["mena"]} nebyla spárována s žádným dokladem"); 
		}
	}

	protected function getOneActiveByVs($invoice_vs)
	{
		//TODO - jak řešit více faktur se stejným VS z různými částkami
		//Asi by se měli otevřít všechny a zkontrolovat částky a najít tu správnou, pokud ani jedna částka neodpovídá, použít fakturu se nejbližší vykrytou částkou
		if(strlen($invoice_vs)){
			$by = array(
				"client_id" => $this->client["id"],
				"invoice_vs" => $invoice_vs,
				"invoice_status" => "active",
				"invoice_queue" => $this->account["invoice_queue"],
			);
			
			$invoice = $this->getOneByLocale($by);

			return $invoice;
		}

		return false;
	}

	protected function setPaid($invoice, $paidSum = null)
	{
		if($paidSum == null || $this->checkSumsByVs($invoice, $paidSum)){
			$invoice = array_extend(
				$invoice, 
				array(
					"invoice_status" => "paid",
					"invoice_paid" => "NOW()"
				)
			);

			$result = $this->repository->invoices->save($invoice);

			$this->syncLog[] = alert("Doklad {$invoice["invoice_title"]}, VS: {$invoice["invoice_vs"]} nastaven jako zaplacený", true);

			if($invoice["invoice_type"] == "proforma"){
				$this->createMain($invoice["invoice_id"]);

				$this->send($invoice);
			}

			return $result;
		}

		return null;
	}

	protected function checkSumsByVs($invoice, $paidSum)
	{
		//Kolik zbývá uhradit
		$invoice["invoiceSum"] = $invoice["invoice_remain_charge"];
		
		//Kolik bylo současnou platbou uhrazeno
		$invoice["paidSum"] = $paidSum;

		$this->updateRemainCharge($invoice);

		if($invoice["invoiceSum"] > $invoice["paidSum"]){
			//Nebyla uhrazena odstatečná částka
			
			$this->syncLog[] = alert("Pro doklad VS: {$invoice["invoice_vs"]} bylo uhrazeno {$invoice["paidSum"]}, zatímco bylo požadováno {$invoice["invoiceSum"]}", false);

			$this->createPaidNotification($invoice, "invoicesPaidLess");

			$result = false;
		} elseif ($invoice["invoiceSum"] == $invoice["paidSum"]) {
			//Bylo uhrazeno přesně tolik kolik mělo
			
			$this->syncLog[] = alert("Pro doklad VS: {$invoice["invoice_vs"]} bylo uhrazeno {$invoice["paidSum"]}, přesně jak bylo požadováno", true);

			$this->createPaidNotification($invoice, "invoicesPaid");
			
			$result = true;
		} else {
			//Bylo uhrazeno více než mělo být
			
			$this->syncLog[] = alert("Pro doklad VS: {$invoice["invoice_vs"]} bylo uhrazeno {$invoice["paidSum"]}, což je více než bylo požadováno: {$invoice["invoiceSum"]}", false);

			$this->createPaidNotification($invoice, "invoicesPaidMore");

			$result = true;
		}

		return $result;
	}

	protected function updateRemainCharge($invoice)
	{
		$invoice = array_extend(
			$invoice, 
			array(
				"invoice_remain_charge" => $invoice["invoiceSum"] - $invoice["paidSum"]
			)
		);

		$this->repository->invoices->save($invoice);
	}

	protected function createPaidNotification($invoice, $notification_key)
	{
		$parameters = array(
			"invoice_title" => $invoice["invoice_title"],
			"invoice_no" => $invoice["invoice_no"],
			"invoice_vs" => $invoice["invoice_vs"],
			"invoiceSum" => $invoice["invoiceSum"],
			"paidSum" => $invoice["paidSum"],
			"invoice_remain_charge" => $invoice["invoice_remain_charge"]
		);

		$this->notificationsService->create()
			->setUrl('/admin/invoices/detail/{$invoice["invoice_id"]}')
			->setKey($notification_key)
			->setTemplate(CP_DIR . "/Invoices/templates/notifications/{$notification_key}.latte", $parameters)
			->setRole(array('admin', 'superAdmin'))
			->save();
	}	
}