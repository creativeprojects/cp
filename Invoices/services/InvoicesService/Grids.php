<?php

namespace CP\Services\Invoices;

use CP\Grid\Grid;

trait Grids
{
	public function getActiveGridByClient()
	{
		$query = array(
			"where" => array(
				"invoice_status" => "invoice_status IN ('active')"
			)
		);

		$invoicesGrid = $this->getGridByClient($query);

		return $invoicesGrid;
	}

	public function getArchiveGridByClient()
	{
		$query = array(
			"where" => array(
				"invoice_status" => "invoice_status IN ('paid','cancelled')"
			)
		);

		$invoicesGrid = $this->getGridByClient($query);

		return $invoicesGrid;
	}

	public function getGridByClient($query = array())
	{
		$invoicesGrid = new Grid($this->database);

		$invoicesGrid->query = array_extend(
			array(
			'fields' => array(
				'v.*',
				"(SELECT invoice_no FROM {$this->repository->invoices} WHERE invoice_id = v.proforma_invoice_id) AS proforma_invoice_no",
				"(SELECT SUM(i.item_price_main * i.item_amount) FROM {$this->repository->invoicesItems} AS i WHERE i.invoice_id = v.invoice_id AND i.removed IS NULL) as invoice_price_sum"
			),
			'from' => "{$this->repository->invoices} AS v",
			'limit' => null,
			'where' => array(
				"client_id" => "v.client_id = " . $this->client['id'],
				"main_invoice_id" => "v.main_invoice_id IS NULL",
				"removed" => "v.removed IS NULL"
			)), $query
		);

		//$invoicesGrid->itemsCallback = callback($this, "listItemValues");

		return $invoicesGrid;
	}

	/*public function listItemValues($values)
	{
		return $values;
	}*/

	public function getTemplatesGridByClient($query = array())
	{
		$invoicesGrid = new Grid($this->database);

		$invoicesGrid->query = array_extend(
			array(
			'fields' => array(
				'v.*',
				"(SELECT SUM(i.item_price_main * i.item_amount) FROM {$this->repository->invoicesTemplatesItems} AS i WHERE i.invoice_id = v.invoice_id AND i.removed IS NULL) as invoice_price_sum"
			),
			'from' => "{$this->repository->invoicesTemplates} AS v",
			'limit' => null,
			'where' => array(
				"client_id" => "v.client_id = " . $this->client['id'],
				"removed" => "v.removed IS NULL"
			)), $query
		);

		//$invoicesGrid->itemsCallback = callback($this, "listItemValues");

		return $invoicesGrid;
	}
}