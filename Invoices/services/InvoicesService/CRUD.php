<?php

namespace CP\Services\Invoices;

trait CRUD
{
	public function create($invoice = array())
	{
		$invoice = array_extend(
			array(
				"invoice_status" => "active",
				"client_id" => $this->client["id"],
				"invoice_delegate_vat" => false
			),
			$invoice
		);

		$this->setValidInvoiceType($invoice);

		$this->setDates($invoice);

		$this->setDefaultItems($invoice);

		$this->setDefaultBankAccount($invoice);

		$this->setDefaultQueue($invoice);

		$invoice["invoice_no"] = $this->getInvoiceNo($invoice);
		$invoice["invoice_vs"] = $this->getInvoiceVs($invoice);

		return $this->save($invoice);
	}

	public function createMain($invoice_id)
	{
		if ($invoice_id) {
			if ($invoice = $this->getOneById($invoice_id)) {
				if ($invoice["invoice_type"] == "proforma") {
					$this->repository->invoices->saveField($invoice["invoice_id"], "invoice_status", "paid");

					$invoice["proforma_invoice_id"] = $invoice["invoice_id"];
					$invoice["invoice_repeat"] = "norepeat";
					$invoice["invoice_id"] = null;
					$invoice["invoice_type"] = "main";
					$invoice["invoice_no"] = $this->getInvoiceNo($invoice);
					$invoice["invoice_vs"] = $this->getInvoiceVs($invoice);
					$invoice["invoice_status"] = "paid";
					$invoice["invoice_established"] = now();
					$invoice["invoice_expire"] = now();
					$invoice["invoice_due"] = now();

					foreach ($invoice["items"] AS & $item) {
						$item["id"] = null;
					}

					$invoice = $this->save($invoice);

					$this->repository->invoices->saveField($invoice["proforma_invoice_id"], "main_invoice_id", $invoice["invoice_id"]);

					return $invoice;
				}
			}
		}

		return false;
	}

	public function duplicate($invoice_id)
	{
		if ($invoice_id) {
			if ($invoice = $this->getOneById($invoice_id)) {
				
				$invoice["invoice_id"] = null;
				$invoice["invoice_no"] = $this->getInvoiceNo($invoice);
				//TODO - jak řešit když chci mít pro duplikovanou fakturu stejný VS jako pro originál
				$invoice["invoice_vs"] = $this->getInvoiceVs($invoice);
				$invoice["invoice_status"] = "active";

				$this->setDates($invoice);

				foreach ($invoice["items"] AS & $item) {
					$item["id"] = null;
				}

				$invoice = $this->save($invoice);

				return $invoice;
			}
		}

		return false;
	}

	public function save($invoice)
	{
		if($invoice["invoice_status"] == "paid")
			$invoice["invoice_remain_charge"] = $this->getItemsPriceIncludeVatSum($invoice["items"]);

		$invoice = $this->repository->invoices->save($invoice);

		$invoice["items"] = $this->saveItems($invoice);

		$invoice["invoice_document_path"] = $this->savePdf($invoice["invoice_id"]);

		return $invoice;
	}

	protected function saveItems($invoice)
	{
		return $this->_saveItems($invoice, $this->repository->invoicesItems);
	}

	public function remove($invoice_id)
	{
		$this->repository->invoices->removeOne($invoice_id);

		$this->repository->invoicesItems->removeBy(array("invoice_id" => $invoice_id));

		return true;
	}

	/* Templates */

	public function createTemplate($invoice = array())
	{
		$invoice["invoice_type"] = $this->getValidInvoiceType(@$invoice["invoice_type"]);

		$invoice = array_extend(
			array(
			"client_id" => $this->client["id"],
			"invoice_expire" => $this->settings["default"]["invoice_expire"],
			"invoice_due" => $this->settings["default"]["invoice_due"],
			"invoice_repeat" => $this->settings["default"]["invoice_repeat"],
			"invoice_delegate_vat" => false
			), $invoice
		);

		$this->setDefaultItems($invoice);

		$this->setDefaultBankAccount($invoice);

		$this->setDefaultQueue($invoice);

		return $this->saveTemplate($invoice);
	}

	public function saveTemplate($invoice)
	{
		$invoice = $this->repository->invoicesTemplates->save($invoice);

		$invoice["items"] = $this->saveTemplateItems($invoice);

		return $invoice;
	}

	protected function saveTemplateItems($invoice)
	{
		return $this->_saveItems($invoice, $this->repository->invoicesTemplatesItems);
	}

	public function removeTemplate($invoice_id)
	{
		$this->repository->invoicesTemplates->removeOne($invoice_id);

		$this->repository->invoicesTemplatesItems->removeBy(array("invoice_id" => $invoice_id));

		return true;
	}

	public function createRegular($invoice_id)
	{
		if ($invoice_id) {
			if ($template = $this->getOneTemplateById($invoice_id)) {
				
				$invoice = $template;

				$invoice["template_invoice_id"] = $invoice["invoice_id"];
				
				$invoice["invoice_id"] = null;
				$invoice["invoice_no"] = $this->getInvoiceNo($invoice);
				$invoice["invoice_vs"] = $invoice["invoice_vs"] ? $invoice["invoice_vs"] : $this->getInvoiceVs($invoice);
				$invoice["invoice_status"] = "active";
				
				$this->setDates($invoice);

				$this->setInvoiceTitle($invoice, $template);

				foreach ($invoice["items"] AS & $item) {
					$item["id"] = null;
				}

				$invoice = $this->save($invoice);

				return $invoice;
			}
		}

		return false;
	}

	protected function setInvoiceTitle(& $invoice, $template)
	{
		$replace = array(
			"#ROK#" => date("Y", strtotime($invoice["invoice_established"])),
			"#MESIC#" => date("n", strtotime($invoice["invoice_established"])),	
			"#DEN#" => date("j", strtotime($invoice["invoice_established"])),
			"#CISLO#" => $invoice["invoice_no"],
			"#PORADOVE#" => $this->repository->invoices->getCountBy(array("template_invoice_id" => $template["invoice_id"])) + 1,
		);

		$invoice["invoice_title"] = str_replace(array_keys($replace), $replace, $invoice["invoice_title"]);
	}

	public function duplicateTemplate($invoice_id)
	{
		if ($invoice_id) {
			if ($invoice = $this->getOneTemplateById($invoice_id)) {
				
				$invoice["invoice_id"] = null;

				$this->setDates($invoice);
				
				foreach ($invoice["items"] AS & $item) {
					$item["id"] = null;
				}

				$invoice = $this->save($invoice);

				return $invoice;
			}
		}

		return false;
	}

	/* Utils */

	public function getInvoiceNo($invoice)
	{
		$invoiceNumber = $this->getInvoiceNumber($invoice);

		$invoice_no = ($invoice["invoice_type"] == "proforma" ? "ZAL" : null) . $invoiceNumber;

		return $invoice_no;
	}

	public function getInvoiceVs($invoice)
	{
		$invoiceNumber = $this->getInvoiceNumber($invoice);

		$invoice_vs = $invoiceNumber;

		return $invoice_vs;
	}

	protected function getInvoiceNumber($invoice)
	{
		$year = date("Y");

		$where = array(
			"YEAR(invoice_established) = {$year}",
			"client_id" => $this->client["id"],
			"invoice_type" => $invoice["invoice_type"],
			"invoice_queue" => $invoice["invoice_queue"],
		);

		//Pokud hledám číslo pro již vytvořenou fakturu, ignorovat samu sebe
		if(isset($invoice["invoice_id"])){
			$where["NOT invoice_id"] = $invoice["invoice_id"];
		}

		$maxNo = $this->database->table((string) $this->repository->invoices)->where($where)->max("invoice_no");

		//Předchozí doklad nenalezen, nový rok apod.
		if(!$maxNo)
			$maxNo = $year . sprintf("%03d", 0);

		$number = filter_var($maxNo, FILTER_SANITIZE_NUMBER_INT) + 1; 

		return $number;
	}

	protected function createDateByDays($days)
	{
		$date = date("Y-m-d", strtotime("+" . $days . " days"));

		return $date;
	}

	protected function setValidInvoiceType(& $invoice)
	{
		$invoice["invoice_type"] = $this->getValidInvoiceType($invoice["invoice_type"]);
	}

	public function getValidInvoiceType($invoice_type = null)
	{
		$invoice_type = (in_array($invoice_type, array_keys($this->settings["typeOptions"])) ? $invoice_type : $this->settings["default"]["invoice_type"]);

		return $invoice_type;
	}

	protected function setDefaultItems(& $invoice)
	{
		if(!isset($invoice["items"]) || is_array($invoice["items"]))
			$invoice["items"] = array();

		if(!count($invoice["items"]))
			array_push($invoice["items"], $this->getNewItem($invoice)); 

	}

	public function getNewItem($invoice)
	{
		$item = array(
			"id" => null,
			"item_price_vat" => $this->settings["defaultVat"]
		);

		return $item;
	}

	protected function setDates(& $invoice)
	{
		$invoice["invoice_established"] = now();
		$invoice["invoice_expire"] = $this->createDateByDays($this->settings["default"]["invoice_expire"]);
		$invoice["invoice_due"] = $this->createDateByDays($this->settings["default"]["invoice_due"]);
	}

	protected function setDefaultBankAccount(& $invoice)
	{
		$bankAccountKeys = array_keys($this->settings["bankAccounts"]);
		
		$invoice["invoice_bank_account"] = reset($bankAccountKeys);	
	}

	protected function setDefaultQueue(& $invoice)
	{
		$queueKeys = array_keys($this->settings["queues"]);
		
		$invoice["invoice_queue"] = reset($queueKeys);	
	}

	protected function _saveItems($invoice, $repository)
	{
		$savedIds = array();

		foreach ($invoice["items"] AS & $item) {
			$item["invoice_id"] = $invoice["invoice_id"];

			if($invoice["invoice_delegate_vat"])
				$item["item_price_vat"] = 0;

			$item = $repository->save($item);

			$savedIds[] = $item["id"];
		}

		$removeBy = array(
			"invoice_id" => $invoice["invoice_id"],
			"id NOT" => $savedIds
		);

		$repository->removeBy($removeBy);

		return $invoice["items"];
	}
}