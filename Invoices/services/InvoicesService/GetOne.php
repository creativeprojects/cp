<?php

namespace CP\Services\Invoices;

trait GetOne
{
	public function getOneById($invoice_id)
	{
		if ($invoice_id) {
			$by = array(
				"client_id" => $this->client["client_id"],
				"invoice_id" => $invoice_id
			);

			$invoice = $this->repository->invoices->getOneBy($by);

			if($invoice)
				$invoice = $this->sanitizeOutput($invoice);

			return $invoice;
		}

		return false;
	}

	public function getOneByIdLocale($invoice_id)
	{
		if($invoice_id){
			$by = array(
				"client_id" => $this->client["client_id"],
				"invoice_id" => $invoice_id
			);

			$invoice = $this->getOneByLocale($by);

			return $invoice;
		}

		return false;
	}

	public function getOneByLocale(array $by)
	{
		$invoice = $this->repository->invoices->getOneByLocale($by);

		if ($invoice)
			$invoice = $this->sanitizeOutput($invoice);

		return $invoice;
	}

	public function getItemsPriceIncludeVatSum(array $items)
	{
		$sum = 0;

		foreach($items AS $item){
			$sum += $item["item_price_main"] * $item["item_amount"] * (1 + ($item["item_price_vat"] / 100));
		}

		return $sum;
	}

	/* Templates */

	public function getOneTemplateById($invoice_id)
	{
		if ($invoice_id) {
			$by = array(
				"client_id" => $this->client["client_id"],
				"invoice_id" => $invoice_id
			);

			$invoice = $this->getOneTemplateBy($by);

			return $invoice;
		}

		return false;
	}

	public function getOneTemplateBy(array $by)
	{
		$invoice = $this->repository->invoicesTemplates->getOneBy($by);

		if ($invoice) {
			$invoice["items"] = $this->repository->invoicesTemplatesItems->getAllBy(array("invoice_id" => $invoice["invoice_id"]));
		}

		return $invoice;
	}

	/* Utils */

	protected function sanitizeOutput($invoice)
	{
		$invoice["items"] = $this->repository->invoicesItems->getAllByLocale(array("invoice_id" => $invoice["invoice_id"]));

		/*$items = $this->repository->invoicesItems->getAllByLocale(array("invoice_id" => $invoice["invoice_id"]));

		$invoice["items"] = array();

		foreach($items AS $item){
			$invoice["items"][] = $item;
		}*/

		$invoice["invoice_bank_accountFormatted"] = $this->getFormattedBankAccount($invoice["invoice_bank_account"]);

		if ($invoice["proforma_invoice_id"]) {
			$invoice["proforma"] = $this->getOneByIdLocale($invoice["proforma_invoice_id"]);
		}

		return $invoice;
	}

	public function getFormattedBankAccount($invoice_bank_account)
	{
		$formatted = "";

		if(array_key_exists($invoice_bank_account, $this->settings["bankAccounts"]))
			$formatted = $this->settings["bankAccounts"][$invoice_bank_account]["no"]  . "/" . $this->settings["bankAccounts"][$invoice_bank_account]["bank"];

		return $formatted;
	}
}