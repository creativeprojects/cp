<?php

namespace Repository;

class invoicesTemplatesItems extends \Repository\BaseRepository
{
	public $name = "invoices_templates_items";

	public $fields = array(
		"id" => "primary",
		"invoice_id" => "int",
		"client_id" => "int",
		"item_id" => "int",
		"item_amount" => "float",
		"item_amount_unit" => "varchar",
		"item_title" => "varchar",
		"item_description" => "text",
		"item_price_main" => "float",
		"item_price_vat" => "float"
	);

	public $validateTable = true;
}