<?php

namespace Repository;

class invoicesItems extends \Repository\BaseRepository
{
	public $name = "invoices_items";

	public $fields = array(
		//TODO - předělat na item_id
		"id" => "primary",
		"invoice_id" => "int",
		"client_id" => "int",
		"item_amount" => "float",
		"item_amount_unit" => "varchar",
		"item_title" => "varchar",
		"item_description" => "text",
		"item_price_main" => "float",
		"item_price_vat" => "float"
	);

	public $validateTable = true;
}