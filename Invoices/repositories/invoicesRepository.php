<?php

namespace Repository;

class invoices extends \Repository\BaseRepository
{
	public $name = "invoices";

	public $fields = array(
		"invoice_id" => "primary",
		"proforma_invoice_id" => "int",
		"main_invoice_id" => "int",
		"client_id" => "int",
		"order_id" => "int",
		"invoice_type" => "varchar",
		"invoice_title" => "varchar",
		"invoice_no" => "varchar",
		"invoice_vs" => "varchar",
		"invoice_ks" => "varchar",
		"invoice_ss" => "varchar",
		"invoice_status" => "varchar",
		"template_invoice_id" => "int",
		"user_id" => "int",
		"invoice_payment" => "varchar",
		"contact_id" => "int",
		"invoice_customer_name" => "varchar",
		"invoice_customer_id" => "varchar",
		"invoice_customer_vat" => "varchar",
		"invoice_customer_phone" => "varchar",
		"invoice_customer_email" => "varchar",
		"invoice_customer_locale" => "locale",
		"invoice_bill_address_street" => "varchar",
		"invoice_bill_address_no" => "varchar",
		"invoice_bill_address_postcode" => "varchar",
		"invoice_bill_address_city" => "varchar",
		"invoice_bill_address_country" => "varchar",
		"invoice_established" => "date",
		"invoice_expire" => "date",
		"invoice_due" => "date",
		"invoice_document_path" => "varchar",
		"invoice_queue" => array(
			"type" => "int",
			"notNull" => true
		),
		"invoice_bank_account" => array(
			"type" => "int",
			"notNull" => true,
		),
		"invoice_remain_charge" => array(
			"type" => "float",
			"notNull" => true
		),
		"invoice_paid" => "timestamp",
		"invoice_sent" => "timestamp",
		"invoice_delegate_vat" => "bool"
	);

	public $validateTable = true;
}