<?php

namespace Repository;

class invoicesTemplates extends \Repository\BaseRepository
{
	public $name = "invoices_templates";

	public $fields = array(
		"invoice_id" => "primary",
		"client_id" => "int",
		"order_id" => "int",
		"invoice_type" => "varchar",
		"invoice_title" => "varchar",
		"invoice_vs" => "varchar",
		"invoice_ks" => "varchar",
		"invoice_ss" => "varchar",
		"invoice_status" => "varchar",
		"invoice_repeat" => "varchar",
		"user_id" => "int",
		"invoice_payment" => "varchar",
		"contact_id" => "int",
		"invoice_customer_name" => "varchar",
		"invoice_customer_id" => "varchar",
		"invoice_customer_vat" => "varchar",
		"invoice_customer_phone" => "varchar",
		"invoice_customer_email" => "varchar",
		"invoice_customer_locale" => "locale",
		"invoice_bill_address_street" => "varchar",
		"invoice_bill_address_no" => "varchar",
		"invoice_bill_address_postcode" => "varchar",
		"invoice_bill_address_city" => "varchar",
		"invoice_bill_address_country" => "varchar",
		"invoice_expire" => array(
			"type" => "int",
			"notNull" => true
		),
		"invoice_due" => array(
			"type" => "int",
			"notNull" => true
		),
		"invoice_queue" => array(
			"type" => "int",
			"notNull" => true
		),
		"invoice_bank_account" => array(
			"type" => "varchar",
			"notNull" => true
		),
		"invoice_autosend" => "bool",
		"invoice_delegate_vat" => "bool",
		"template_starts_at" => "date",
		"template_ends_at" => "date"
	);

	public $validateTable = true;
}