<?php

namespace CP\FrontModule\Presenters;

trait InvoicesPresenter
{
	/** @var \CP\Services\Invoices @inject */
	public $invoicesService;

	public function actionSyncTransactions($reset = null)
	{
		$this->template->log = $this->invoicesService->syncTransactions($reset);
	}

	public function actionCheckRepeated()
	{
		$this->template->log = $this->invoicesService->checkRepeated();
	}
}