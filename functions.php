<?php

function alert($content, $class = null)
{
	$class = $class === true ? "alert-success" : ($class === false ? "alert-danger" : "alert-info");

	$html = "<div class=\"alert {$class}\">{$content}</div>";

	return \Nette\Utils\Html::el()->setHtml($html);
}

function array_extend($defaults, $values = null, $replace_null = false, $use_only_defaults_keys = false)
{
	if ($use_only_defaults_keys)
		$result = array();
	else
		$result = $values;

	if (is_array($values)) {
		$defaults = is_array($defaults) ? $defaults : array();

		foreach ($defaults AS $key => $value) {
			if (@is_array($value) && count($value) && @is_array($values[$key])) {
				$result[$key] = array_extend($value, $values[$key], $replace_null, $use_only_defaults_keys);
			} else {
				$result[$key] = ($replace_null ? array_key_exists($key, $values) : isset($values[$key])) ? $values[$key] : $value;
			}
		}

		return $result;
	}

	return $defaults;
}

function array_force($record)
{
	return is_array($record) ? $record : (empty($record) ? array() : array($record));
}

function array_by_key($input, $key)
{
	$array = array();

	foreach ($input AS $item)
		$array[] = $item[$key];

	return $array;
}

function now()
{
	return date("Y-m-d H:i:s");
}

function join_fields(array $tables, $output = "string")
{
	$fields = array();

	foreach ($tables AS $alias => $table) {
		foreach (array_keys($table->fields) AS $field)
			$fields[$field] = (is_numeric($alias) ? $table : $alias) . ".{$field}";
	}

	switch ($output) {
		case "array":
			return $fields;
		case "string":
			return implode(", ", $fields);
	}
}

function arrayToNull(& $array)
{
	if (is_array($array)) {
		foreach ($array AS & $value) {
			if ($value == array())
				$value = null;
		}
	}
}

function recursive_implode(array $array, $glue = ',', $include_keys = false, $trim_all = false)
{
	$glued_string = '';

	// Recursively iterates array and adds key/value to glued string
	array_walk_recursive($array, function($value, $key) use ($glue, $include_keys, &$glued_string) {
		$include_keys and $glued_string .= $key . $glue;
		$glued_string .= $value . $glue;
	});

	// Removes last $glue from string
	strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));

	// Trim ALL whitespace
	$trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);

	return (string)$glued_string;
}

function is_localhost()
{
	$whitelist = array('127.0.0.1', '::1');
	if (in_array($_SERVER['REMOTE_ADDR'], $whitelist))
		return true;
}

function decToDms($latitude, $longitude)
{
	$latitudeDirection = $latitude < 0 ? 'S' : 'N';
	$longitudeDirection = $longitude < 0 ? 'W' : 'E';

	$latitudeNotation = $latitude < 0 ? '-' : '';
	$longitudeNotation = $longitude < 0 ? '-' : '';

	$latitudeInDegrees = floor(abs($latitude));
	$longitudeInDegrees = floor(abs($longitude));

	$latitudeDecimal = abs($latitude) - $latitudeInDegrees;
	$longitudeDecimal = abs($longitude) - $longitudeInDegrees;

	$_precision = 3;
	$latitudeMinutes = round($latitudeDecimal * 60, $_precision);
	$longitudeMinutes = round($longitudeDecimal * 60, $_precision);

	return sprintf('%s%s° %s %s %s%s° %s %s', $latitudeNotation, $latitudeInDegrees, $latitudeMinutes, $latitudeDirection, $longitudeNotation, $longitudeInDegrees, $longitudeMinutes, $longitudeDirection
	);
}

function array_to_xml($array, &$xml)
{
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			if (!is_numeric($key)) {
				$subnode = $xml->addChild("$key");
				array_to_xml($value, $subnode);
			} else {
				$subnode = $xml->addChild("item$key");
				array_to_xml($value, $subnode);
			}
		} else {
			$xml->addChild("$key", htmlspecialchars("$value"));
		}
	}
}

function remove_accents($text)
{
	$search = Array(
		'ä', 'Ä', 'á', 'Á', 'à', 'À', 'ã', 'Ã', 'â', 'Â', 'č', 'Č', 'ć', 'Ć', 'ď', 'Ď', 'ě', 'Ě', 'é', 'É', 'ë', 'Ë', 'è', 'È', 'ê', 'Ê', 'í', 'Í', 'ï', 'Ï', 'ì', 'Ì', 'î', 'Î', 'ľ', 'Ľ', 'ĺ', 'Ĺ', 'ń', 'Ń', 'ň', 'Ň', 'ñ', 'Ñ', 'ó', 'Ó', 'ö', 'Ö', 'ô', 'Ô', 'ò', 'Ò', 'õ', 'Õ', 'ő', 'Ő', 'ř', 'Ř', 'ŕ', 'Ŕ', 'š', 'Š', 'ś', 'Ś', 'ť', 'Ť', 'ú', 'Ú', 'ů', 'Ů', 'ü', 'Ü', 'ù', 'Ù', 'ũ', 'Ũ', 'û', 'Û', 'ý', 'Ý', 'ž', 'Ž', 'ź', 'Ź'
	);

	$replace = Array(
		'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'c', 'C', 'c', 'C', 'd', 'D', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'l', 'L', 'l', 'L', 'n', 'N', 'n', 'N', 'n', 'N', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'r', 'R', 'r', 'R', 's', 'S', 's', 'S', 't', 'T', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'y', 'Y', 'z', 'Z', 'z', 'Z'
	);

	return str_replace($search, $replace, $text);
}

function stringTemplate($string, $vars)
{
	foreach($vars AS $key => $var){
		if(!is_array($var)){
			$string = str_replace("{".$key."}", $var, $string);
		}
	}

	return $string;
}
?>