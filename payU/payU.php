<?php

namespace CP\PayU;

use CP;

class Service extends CP\Base\Service
{
	/** @var \App\Services\Orders @inject */
	public $ordersService;

	public $urlPayU = "https://secure.payu.com/paygw/";

	public $server = "secure.payu.com";

	public $kodovani;

	public $key1;

	public $key2;

	public $pos_auth_key;

	public $pos_id;

	public function setup()
	{
		$this->kodovani = "UTF";
		$this->key1 = $this->client["payu_key1"];
		$this->key2 = $this->client["payu_key2"];
		$this->pos_auth_key = $this->client["payu_pos_auth_key"];
		$this->pos_id = $this->client["payu_pos_id"];
	}

	public function newPayment($order_id)
	{
//		$order = $this->repository->orders->getOneBy(array("order_id" => $order_id));

		$order = $this->ordersService->getOneById($order_id);

		if ($order["order_status"] != "waiting_for_payment") {
			return FALSE;
		}

		$order["ts"] = time();
		$order["session_id"] = $order["order_id"] . $order["ts"];
		$payment = $this->repository->payU->insert(array(
			"order_id" => $order["order_id"],
			"session_id" => $order["session_id"]
		));

		$order["desc"] = "Platba za objednávku č. {$order["order_no"]} na {$this->client["title"]}";

		$order["sig"] = $this->newPaymentSig($order);

		$order["pos_id"] = $this->pos_id;
		$order["pos_auth_key"] = $this->pos_auth_key;
		return $order;
	}

	public function getAnswer()
	{
		$server_script = "/paygw/ISO/Payment/get";
		// returns array with values:
		// "code" (numerical transaction status or false in case of error), "message" (status description or error description)
		// some parameters are missing
		if (!isset($_POST["pos_id"]) || !isset($_POST["session_id"]) || !isset($_POST["ts"]) || !isset($_POST["sig"]))
			die("ERROR: EMPTY PARAMETERS");

		$answer = $_POST;
		// received POS ID is different than expected
		if ($_POST["pos_id"] != $this->pos_id)
			die("ERROR: INCORRECT POS ID");
		// verification of received signature
		$sig = md5($answer["pos_id"] . $answer["session_id"] . $answer["ts"] . $this->key2);
		// incorrect signature
		if ($_POST["sig"] != $sig)
			die("ERROR: INCORRECT SIGNATURE");
		// signature that will be sent to PayU with request
		$ts = time();
		$sig = md5($this->pos_id . $answer["session_id"] . $ts . $this->key1);

		// preparing parameters string to be sent to PayU
		$parameters = "pos_id=" . $this->pos_id . "&session_id=" . $answer["session_id"] . "&ts=" . $ts . "&sig=" . $sig;

		$payu_response = $this->socketRequest($server_script, $parameters);

		// parsing PayU response
		$result = false;
		if (preg_match("/<trans>.*<pos_id>([0-9]*)<\/pos_id>.*<session_id>(.*)<\/session_id>.*<order_id>(.*)<\/order_id>.*" .
				"<amount>([0-9]*)<\/amount>.*<status>([0-9]*)<\/status>.*<desc>(.*)<\/desc>.*<ts>([0-9]*)<\/ts>.*<sig>([a-z0-9]*)" .
				"<\/sig>.*<\/trans>/is", $payu_response, $parts)) {
			$result = $this->get_status($parts);
		}

		// recognised status of transaction
		if ($result["code"]) {
			// $this->saveChanges($parts);
			$pos_id = $parts[1];
			$session_id = $parts[2];
			$order_id = $parts[3];
			$amount = $parts[4]; // v halerich
			$status = $parts[5];
			$desc = $parts[6];
			$ts = $parts[7];
			$sig = $parts[8];

			// TODO:
			// zmena statusu transakce v systemu shopu
			// change of transaction status in system of the shop


			if ($result["code"] == "99") {
				$this->repository->payU->updateBy(array(
					"session_id" => $parts[2]
					), array(
					"status" => $parts[5]
				));

				$this->repository->orders->update(array(
					"order_id" => $order_id,
					"order_status" => "new"
				));
				// platba je uspesna, takĹľe posilĂˇme zpĂˇtky OK
				// payment sucessful so we send back OK
				echo "OK";
				exit;
			} elseif ($result["code"] == "2") {
				$this->repository->payU->updateBy(array(
					"session_id" => $parts[2]
					), array(
					"status" => $parts[5]
				));
				// transakce zrusena, muzeme rovnez transakci zrusit
				// transaction cancelled, we can also cancell transaction
			} else {
				$this->repository->payU->updateBy(array(
					"session_id" => $parts[2]
					), array(
					"status" => $parts[5]
				));
				// jine akce
				// other actions
			}


			// pokud jsou vsechny operace ukoncene, posilame nazpet OK, v opacnem pripade vygenerujeme error
			// if all operations are done then we send back OK, in other case we generate error
			// if (everything_ok)
			// {
			echo "OK";
			exit;
			// } else {
			//
			// }
		} else {
			// TODO:
			// sprava plateb se statusem error
			// error transaction status managment

			// $this->saveChanges("ERROR: Data error ....\n");
			// $this->saveChanges("code=" . $result["code"] . " message=" . $result["message"] . "\n");
			// $this->saveChanges($result);
			// $this->saveChanges($payu_response);
			// informace o zmene statusu bude z secure.payu.com odeslana znovu, muzeme zapsat informaci do logu (logs)....
			// information about changing a status will be send again from secure.payu.com, we can write information to logs....
		}
	}

	private function get_status($parts)
	{
		// chybne cislo POS ID v odpovedi
		// incorrect POS ID number specified in response
		if ($parts[1] != $this->pos_id)
			return array("code" => false, "message" => "incorrect POS number");

		// vypocet podpisu pro porovnani se sig odeslanym ze strany PayU
		// calculating signature for comparison with sig sent by PayU
		$sig = md5($parts[1] . $parts[2] . $parts[3] . $parts[5] . $parts[4] . $parts[6] . $parts[7] . $this->key2);

		// chybnĂ˝ podpis v odpovedi v porovnani s podpisem spocitanzm lokalne
		// incorrect signature in response in comparison to locally calculated one
		if ($parts[8] != $sig)
			return array("code" => false, "message" => "incorrect signature");

		// ruzne zpravy dle statusu transakce. Popisy jednotlivĂ˝ch statusu jsou uvedeny v technicke dokumentaci
		// different messages depending on transaction status. For status description, see documentation
		switch ($parts[5]) {
			case 1: return array("code" => $parts[5], "message" => "new");
			case 2: return array("code" => $parts[5], "message" => "cancelled");
			case 3: return array("code" => $parts[5], "message" => "rejected");
			case 4: return array("code" => $parts[5], "message" => "started");
			case 5: return array("code" => $parts[5], "message" => "awaiting receipt");
			case 6: return array("code" => $parts[5], "message" => "no authorization");
			case 7: return array("code" => $parts[5], "message" => "payment rejected");
			case 99: return array("code" => $parts[5], "message" => "payment received - ended");
			case 888: return array("code" => $parts[5], "message" => "incorrect status");
			default: return array("code" => false, "message" => "no status");
		}
	}

	private function socketRequest($server_script, $parameters)
	{
		$fp = @fsockopen("ssl://" . $this->server, 443, $errno, $errstr, 30);

		// sending request via socket
		$header = "POST " . $server_script . " HTTP/1.0" . "\r\n" .
			"Host: " . $this->server . "\r\n" .
			"Content-Type: application/x-www-form-urlencoded"
			. "\r\n" . "Content-Length: " .
			strlen($parameters) . "\r\n" . "Connection: close" . "\r\n\r\n";
		@fputs($fp, $header . $parameters);
		$payu_response = "";
		while (!@feof($fp)) {
			$res = @fgets($fp, 1024);
			$payu_response .= $res;
		}
		@fclose($fp);
		return $payu_response;
	}

	public function newPaymentSig($order)
	{
		$name = explode(" ", $order["order_customer_name"]);
		$string = $this->pos_id . // pos_id
			"t" . // pay_type
			$order["session_id"] . // session_id
			$this->pos_auth_key . //pos_auth_key
			$order["price"] . // amount
			$order["desc"] . // desc
			"" . // desc2
			$order["order_no"] . // order_id
			$name[0] . //first_name
			$name[1] . // last_name
			"" . // street
			"" . // street_hn
			"" . // street_an
			"" . // city
			"" . // post_code
			"" . // country
			$order["order_customer_email"] . // email
			"" . // phone
			"cs" . // language
			$_SERVER["REMOTE_ADDR"] . // client_ip
			$order["ts"] . // ts
			$this->key1; // key1
		return md5($string);
	}
}