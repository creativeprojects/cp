<?php

namespace Repository;

class reviews extends \Repository\BaseRepository
{
	public $name = "reviews";

	public $fields = array(
		"review_id" => "primary",
		"client_id" => "int",
		"owner_table" => "varchar",
		"owner_id" => "int",
		"review_author_name" => "varchar",
		"review_author_email" => "varchar",
		"review_author_city" => "varchar",
		"review_text" => "text",
		"review_rating" => "float",
		"user_id" => "int",
	);

	public $validateTable = true;
}