<?php

namespace CP\Presenters;

use Nette;
use CP\Forms\ReviewForm;
use Nette\Application\BadRequestException;

trait ReviewsPresenter
{
	/** @var \CP\Services\Reviews @inject */
	public $reviewsService;

	public $label = array(
		"saveSuccess" => "Hodnocení bylo uloženo",
		"saveError" => "Hodnocení se nepodařilo uložit!",
		"removeSuccess" => "Hodnocení bylo odebráno",
		"removeError" => "Hodnocení se nepodařilo odebrat!"

	);

	public function actionDefault()
	{
		if ($this->user->isAllowed("Admin:Reviews", "default")) {
			$this->template->reviewsGrid = $this->reviewsService->getGridByClient();

			$this->template->reviewsGrid->paginator->itemsPerPage = 20;

			$this->template->reviewsGrid->create($this);
		} else {
			throw new BadRequestException;
		}
	}
	
	private $project;

	public function handleAdd()
	{
		$review = $this->reviewsService->create();

		$this->redirect("Reviews:add", $review["review_id"]);
	}

	public function actionAdd($review_id)
	{
		$this->actionEdit($review_id);
	}

	public function actionEdit($review_id)
	{
		$review = $this->reviewsService->getOneById($review_id);

		if ($review) {
			$this->template->review = $review;
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentReviewForm()
	{
		$form = new ReviewForm($this);

		$form->setValues($this->template->review);

		$form->redirect = "Reviews:default";

		return $form->create();
	}

	public function handleRemove($review_id)
	{
		if ($this->reviewsService->remove($review_id)) {
			$this->flashMessage($this->message["removeSuccess"], "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Reviews:default");
		} else
			$this->flashMessage($this->message["removeError"], "alert-danger");
	}
}