<?php

namespace CP\Services;

use App;
use CP\Grid\Grid;
use Nette\Utils\Strings;

class Reviews extends \CP\Base\Service
{
	public $settings = array(
		"ratingRange" => array(1, 5)
	);

	public function getGridByClient($query = array())
	{
		$grid = new Grid($this->database);

		$grid->query = array_extend(
			array(
			'fields' => array(
				"*"
			),
			'from' => "{$this->repository->reviews} AS r",
			"order_by" => "created DESC",
			'where' => array(
				"client_id" => "r.client_id = " . $this->client['id'],
				"removed" => "r.removed IS NULL"
			)), $query
		);

		return $grid;
	}

	public function getOneById($review_id)
	{
		if ($review_id) {
			$by = array(
				"client_id" => $this->client["client_id"],
				"review_id" => $review_id
			);

			$review = $this->repository->reviews->getOneBy($by);

			return $review;
		}

		return false;
	}

	public function create()
	{
		$review = array(
		);

		return $this->save($review);
	}

	public function save($values)
	{
		$values = $this->sanitizeInputValues($values);

		$values = $this->repository->reviews->save($values);

		return $values;
	}

	protected function sanitizeInputValues($values)
	{
		$values["client_id"] = $this->client["id"];

		return $values;
	}

	public function remove($review_id)
	{
		$this->repository->reviews->removeOne($review_id);

		return true;
	}

	public function removeByOwner()
	{

	}
}