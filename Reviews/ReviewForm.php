<?php

namespace CP\Forms;

use CP\Forms\Base;
use Exception;

class ReviewForm extends Base
{
	public $flashMessage = array(
		"success" => "Vaše hodnocení bylo uloženo, děkujeme",
		"error" => "Při ukládání hodnocení došlo k chybě!"
	);

	public $redirect;

	public function create()
	{
		if($this->redirect){
			$this->form->addHidden("review_id");

			$this->form->addText("review_author_name");

			$this->form->addText("review_author_email");

			$this->form->addText("review_author_city");

			$this->form->addTextArea("review_text");

			$this->form->addText("created");

			$this->form->addSubmit("save")
				->onClick[] = array($this, "save");

			$this->setDefaults();

			return $this->form;
		} else {
			throw new Exception('Redirect not set for reviewForm!');
		}
	}

	public function save($button)
	{
		$review = $button->parent->getValues(true);

		if ($this->presenter->reviewsService->save($review)) {
			$this->presenter->flashMessage($this->flashMessage["success"], "alert-success");

			$this->presenter->restoreRequest($this->presenter->backlink);
			$this->presenter->redirect($this->redirect);
		} else {
			$this->presenter->flashMessage($this->flashMessage["error"], "alert-danger");
		}
	}
}