<?php

namespace Repository;

class menuItems extends \Repository\BaseRepository
{
	public $name = "menu_items";

	public $fields = array(
		"item_id" => "primary",
		"client_id" => "int",
		"menu" => "varchar",
		"item_rank" => "int",
		"slug_id" => "int",
	);

	protected $validateTable = true;
}