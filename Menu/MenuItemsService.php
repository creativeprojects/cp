<?php

namespace CP\Menu;

use Nette;
use CP;
use Nette\Utils\Strings;

class Service extends CP\Base\Service
{
	public function getAllByClient($query = array())
	{
		$by = array(
			"where" => array(
				"client_id" => $this->client["id"]
			),
			"order" => array(
				"menu" => "ASC",
				"item_rank" => "ASC"
			)
		);

		$items = $this->repository->menuItems->getAllBy($by);

		$slug_ids = array();

		foreach($items AS $item) $slug_ids[] = $item["slug_id"];

		$slugs = $this->repository->slugs->getAllByLocale(array("slug_id" => $slug_ids));

		$menuItems = array();

		foreach($items AS $item){
			if(!isset($menuItems[$item["menu"]]))
				$menuItems[$item["menu"]] = array(
					"title" => $item["menu"],
					"items" => array()
				);

			$menuItems[$item["menu"]]["items"][$item["item_id"]] = array_extend($item, $slugs[$item["slug_id"]]);
		}

		return $menuItems;
	}

	public function getOneById($item_id)
	{
		$by = array(
			"client_id" => $this->client["id"],
			"item_id" => $item_id
		);

		$item = $this->repository->menuItems->getOneBy($by);

		return $item;
	}

	public function add($item)
	{
		$item["item_rank"] = $this->database->table($this->repository->menuItems->getName())->where("client_id", $item["client_id"])->where("menu", $item["menu"])->max("item_rank") + 1;

		return $this->repository->menuItems->save($item);
	}

	public function remove($item_id)
	{
		return $this->repository->menuItems->removeOne($item_id);
	}

	public function move($item_id, $value)
	{
		$source = $this->repository->menuItems->getOne($item_id);

		if($source && $source["item_rank"]){
			$destinationQuery = array(
				"where" => array(
					"client_id" => $this->client["id"],
					"menu" => $source["menu"],
					"item_rank " . ($value > 0 ? " > " : " < ") . "{$source["item_rank"]}"
				),
				"order" => array(
					"item_rank" => $value > 0 ? "ASC" : "DESC"
				)
			);

			if($destination = $this->repository->menuItems->getOneByLocale($destinationQuery)){
				$this->repository->menuItems->saveField($source["item_id"], "item_rank", $destination["item_rank"]);

				$this->repository->menuItems->saveField($destination["item_id"], "item_rank", $source["item_rank"]);
			}
		}
	}
}