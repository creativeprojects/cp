<?php

namespace Repository;

class menus extends \Repository\BaseRepository
{
	public $name = "menus";

	public $fields = array(
		"menu_id" => "primary",
		"client_id" => "int",
		"menu" => "varchar",
		"menu_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"menu_description" => array(
			"type" => "text",
			"locale" => true
		)
	);

	protected $validateTable = true;
}