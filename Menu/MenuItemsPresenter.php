<?php

namespace CP\Menu;

use Nette;
use CP\Forms\Form;
use Nette\Application\BadRequestException;

trait MenuItemsPresenter
{
	/** @var \CP\Menu\Service @inject */
	public $menuItemsService;

	public $menuItems;

	public function actionDefault()
	{
		if ($this->user->isInRole('superAdmin')) {
			$this->menuItems = $this->menuItemsService->getAllByClient();

			$this->template->menuItems = $this->menuItems;
		} else
			throw new BadRequestException;
	}

	protected function createComponentMenuForm()
	{
		$form = new Form;

		$form->addHidden("client_id")
			->setDefaultValue($this->client["id"]);

		$form->addText("menu");

		$form->addSelect("slug_id", null, $this->menuItemsService->slugsService->getAvailableOptions());

		$form->addSubmit("add");

		$form->onSuccess[] = callback($this, "menuFormAdd");

		return $form;
	}

	public function menuFormAdd($form)
	{
		$values = $form->getValues();

		if($this->menuItemsService->add($values)){
			$this->flashMessage("Položka byla přidána do menu", "alert-success");

			$this->redirect("this");
		} else {
			$this->flashMessage("Při přidávání položky do menu došlo k chybě!", "alert-danger");
		}
	}

	public function handleRemove($item_id)
	{
		$item = $this->menuItemsService->getOneById($item_id);

		if($item){
			$this->menuItemsService->remove($item["item_id"]);

			$this->flashMessage("Položka byla odebrána z menu", "alert-success");

			$this->redirect("this");
		} else
			throw new BadRequestException;
	}

	public function handleMoveUp($item_id)
	{
		$item = $this->menuItemsService->getOneById($item_id);

		if($item){
			$this->menuItemsService->move($item["item_id"], -1);

			$this->flashMessage("Položka menu byla posunuta nahoru", "alert-success");

			$this->redirect("this");
		} else
			throw new BadRequestException;
	}

	public function handleMoveDown($item_id)
	{
		$item = $this->menuItemsService->getOneById($item_id);

		if($item){
			$this->menuItemsService->move($item["item_id"], 1);

			$this->flashMessage("Položka menu byla posunuta dolů", "alert-success");

			$this->redirect("this");
		} else
			throw new BadRequestException;
	}
}