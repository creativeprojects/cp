<?php

namespace CP\Macros;

use Nette\Utils\Html;

class Map
{
	private $googleApiLoaded = false;

	private $presenter;

	function __construct($presenter)
	{
		$this->presenter = $presenter;
	}

	public function render()
	{
		$args = func_get_args()[0];

		$args["class"] = isset($args["class"]) ? $args["class"] : "map img-thumbnail";
		$args["classWrapper"] = isset($args["classWrapper"]) ? $args["classWrapper"] : "map-wrapper";
		$args["locale"] = isset($args["locale"]) ? $args["locale"] : $this->presenter->translator->getLocale();

		$html = "";

		if(!$this->googleApiLoaded){
			$html .= "<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false&amp;language={$args["locale"]}\"></script>";
			
			$this->googleApiLoaded = true;
		}

		$html .= "<div class=\"{$args["classWrapper"]}\">";

		$html .= "<div class=\"{$args["class"]}\" ";

		if(isset($args["address"]))
			$html .= "data-address=\"{$args["address"]}\"";
		elseif(isset($args["latitude"]) && isset($args["longitude"]))
			$html .= "data-latitude=\"{$args["latitude"]}\" data-longitude=\"{$args["longitude"]}\"";
		else{
			dump("Neither address or lantitude and longitude not set for map macro!");
			dump($args);
		}

		$html .= "></div>";
		$html .= "</div>";

		return Html::el()->setHtml($html);
	}
}