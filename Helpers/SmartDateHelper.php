<?php

namespace CP\Helpers;

use Nette\Utils\Html;

class SmartDate{
	protected $settings = array(
		"formats" => array(
			"default" => array(
				"time" => "H:i",
				"timeSec" => "H:i:s",
				"date" => "Y/m/d",
				"dateTime" => "Y/m/d H:i",
				"dateTimeSec" => "Y/m/d H:i:s"
			),
			"cs" => array(
				"time" => "H:i",
				"timeSec" => "H:i:s",
				"date" => "j.n.Y",
				"dateTime" => "H:i j.n.Y",
				"dateTimeSec" => "H:i:s j.n.Y"
			),
			"en" => array(
				"time" => "H:i",
				"timeSec" => "H:i:s",
				"date" => "Y/m/d",
				"dateTime" => "Y/m/d H:i",
				"dateTimeSec" => "Y/m/d H:i:s"
			)
		)
	);
	
	function __construct($presenter){
		$this->presenter = $presenter;
	}

	public function smartdate($input, $empty = "Nezadáno")
	{
		$timestamp = strtotime($input);

		$now = strtotime(now());

		$last_midnight = $now - ($now % (24*60*60));

		$string = ($timestamp >= $last_midnight ? $this->presenter->translator->translate("Dnes") : ($timestamp >= ($last_midnight-(24*60*60)) ? $this->presenter->translator->translate("Včera") : date("j.n."))) . " " . $this->presenter->translator->translate("v") . " ";

		$html = $timestamp ? $string . date("H:i", strtotime($input)) : "<em>" . $this->presenter->translator->translate($empty) . "</em>";

		return Html::el()->setHtml($html);
	}

	public function date($input, $empty = null)
	{
		return $this->create("date", $input, $empty);
	}

	public function dateTime($input, $empty = null)
	{
		return $this->create("dateTime", $input, $empty);
	}

	public function dateTimeSec($input, $empty = null)
	{
		return $this->create("dateTimeSec", $input, $empty);
	}

	public function time($input, $empty = null)
	{
		return $this->create("time", $input, $empty);
	}

	public function timeSec($input, $empty = null)
	{
		return $this->create("timeSec", $input, $empty);
	}

	protected function create($format, $input, $empty = null)
	{
		$timestamp = strtotime($input);

		$locale = array_key_exists($this->presenter->translator->getLocale(), $this->settings["formats"]) ? $this->presenter->translator->getLocale() : "default";

		$return = $timestamp ? date($this->settings["formats"][$locale][$format], strtotime($input)) : ($empty ? "<em>" . $this->presenter->translator->translate($empty) . "</em>" : null);

		return $return;
	}
}