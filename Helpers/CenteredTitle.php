<?php

namespace CP\Macros;

use Nette\Utils\Html;

class CenteredTitle
{

	function __construct($presenter)
	{
		$this->presenter = $presenter;
	}

	public function render()
	{
		$args = func_get_args()[0];

		$tag = isset($args[0]) ? $args[0] : (isset($args["tag"]) ? $args["tag"] : "h1");
		$small = isset($args["small"]) ? $args["small"] : false;
		$title = isset($args["title"]) ? $args["title"] : (isset($args["label"]) ? $args["label"] : "");
		$icon = isset($args["icon"]) ? $args["icon"] : false;
		$iconTag = isset($args["iconTag"]) ? $args["iconTag"] : "i";

		if(mb_strlen($title) <= 18){
			$sizes = array(
				"padding" => array(
					"xs" => 1,
					"sm" => 4,
					"md" => 4
				),
				"title" => array(
					"xs" => 10,
					"sm" => 4,
					"md" => 4
				)
			);
		} elseif(mb_strlen($title) > 18 && mb_strlen($title) <= 35){
			$sizes = array(
				"padding" => array(
					"xs" => 1,
					"sm" => 2,
					"md" => 3
				),
				"title" => array(
					"xs" => 10,
					"sm" => 8,
					"md" => 6
				)
			);
		} else {
			$sizes = array(
				"padding" => array(
					"xs" => 1,
					"sm" => 1,
					"md" => 2
				),
				"title" => array(
					"xs" => 10,
					"sm" => 10,
					"md" => 8
				)
			);
		}

		$html = '
			<div class="row centered-title">
				<div class="hidden-xxs col-xs-' . $sizes["padding"]["xs"] . ' col-sm-' . $sizes["padding"]["sm"] . ' col-md-'. $sizes["padding"]["md"] . ' centered-title-padding centered-title-padding-left">
				</div>
				<div class="col-xxs-12 col-xs-' . $sizes["title"]["xs"] . ' col-sm-' . $sizes["title"]["sm"] . ' col-md-' . $sizes["title"]["md"] . ' text-center centered-title-content">
					<' . $tag . '>' .
					($icon ? '<' . $iconTag .' class="' . $icon . '"></' . $iconTag .'> ' : null) . 
					'<span>' . $this->presenter->translator->translate($title) . '</span>' .
					($small ? '<small>' . $this->presenter->translator->translate($small) . '</small>' : null) . 
					'</' . $tag . '>	
				</div>
				<div class="hidden-xxs col-xs-' . $sizes["padding"]["xs"] . ' col-sm-' . $sizes["padding"]["sm"] . ' col-md-'. $sizes["padding"]["md"] . ' centered-title-padding centered-title-padding-right">
				</div>
			</div>
		';

		return Html::el()->setHtml($html);
	}
}