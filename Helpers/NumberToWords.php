<?php

namespace CP\Helpers;

class NumberToWords
{

	function __construct($presenter)
	{
		$this->presenter = $presenter;
	}

	public function render($number, $language = NULL)
	{
		require_once(CP_DIR . '/Utils/Numbers/Words.php');

		$words = new \Numbers_Words();

		$string = $words->toWords($number, $language ? $language : $this->presenter->translator->getLocale());

		return substr($string, 0, 1) == "-" ? substr($string, 1) : $string;
	}
}