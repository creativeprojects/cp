<?php

namespace CP\Helpers;

use CP;

class Helpers
{

	static function register($parent)
	{
		//Helpery
		
		$smartDateHelper = new CP\Helpers\SmartDate($parent);

		$parent->template->registerHelper('smartdate', array($smartDateHelper, 'smartdate'));

		$parent->template->registerHelper('date', array($smartDateHelper, 'date'));

		$parent->template->registerHelper('datetime', array($smartDateHelper, 'dateTime'));

		$parent->template->registerHelper('time', array($smartDateHelper, 'time'));

		$postCodeHelper = new CP\Helpers\PostCode($parent);

		$parent->template->registerHelper('postcode', array($postCodeHelper, 'render'));

		$numberToWordsHelper = new CP\Helpers\NumberToWords($parent);

		$parent->template->registerHelper('numberToWords', array($numberToWordsHelper, 'render'));

		//Makra

		$parent->macros["centeredTitle"] = new CP\Macros\CenteredTitle($parent);

		$parent->macros["map"] = new CP\Macros\Map($parent);

		$set = new \Nette\Latte\Macros\MacroSet($parent->template->getLatte()->getCompiler());

		$set->addMacro(
			'centeredTitle', 
			'echo $presenter->macros["centeredTitle"]->render(%node.array)'
		);

		$set->addMacro(
			'map', 
			'echo $presenter->macros["map"]->render(%node.array)'
		);
	}
}