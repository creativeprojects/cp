<?php

namespace CP\Helpers;

use Nette\Utils\Html;

class PostCode
{

	function __construct($presenter)
	{
		$this->presenter = $presenter;
	}

	public function render($postCode)
	{
		$postCode = str_replace(" ", "", $postCode);

		$postCode = substr($postCode, 0, 3) . "&nbsp;" . substr($postCode, 3);

		return Html::el()->setHtml($postCode);
	}
}