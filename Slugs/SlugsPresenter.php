<?php

namespace CP\Presenters;

use Nette;
use CP\Forms\SlugForm;
use Nette\Application\BadRequestException;

trait SlugsPresenter
{
	private $slugsGrid;

	public function actionDefault()
	{
		if($this->user->isAllowed("Admin:Slugs", "default")){

			$this->slugsGrid = $this->slugsService->getGridByClient();

			$this->slugsGrid->paginator->itemsPerPage = 20;

			$this->slugsGrid->create($this);

			$this->template->slugsGrid = $this->slugsGrid;
		} else{
			throw new BadRequestException;
		}
	}

	private $slug;

	public function actionAdd()
	{

	}

	public function actionEdit($slug_id)
	{
		$slug = $this->slugsService->getOneById($slug_id);

		if($slug){
			$this->slug = $slug;

			$this->template->slug = $this->slug;
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentSlugForm()
	{
		$form = new SlugForm($this);

		$form->setValues($this->slug);
		
		return $form->create();
	}

	public function handleSetAvailability($slug_id, $value)
	{
		if ($this->slugsService->saveAvailability($slug_id, $value)) {
			$this->flashMessage("Dostupnost pro menu byla uložena", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Slugs:default");
		} else
			$this->flashMessage("Při ukládání dostupnosti pro menu došlo k chybě!", "alert-danger");
	}	

	public function handleRemove($slug_id)
	{
		if ($this->slugsService->remove($slug_id)) {
			$this->flashMessage("URL adresa byla odbrána", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect("Slugs:default");
		} else
			$this->flashMessage("Při odebírání URL adresy došlo k chybě!", "alert-danger");
	}	
}