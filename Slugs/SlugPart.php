<?php

namespace CP\Forms\SlugForm;

use CP\Forms\Form;

trait SlugPart
{
	protected function addSlugFields($container)
	{
		$slug_url = $container->addContainer("slug_url");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$slug_url->addText($locale);
		}

		$container->addCheckbox("slug_available");

		$slug_label = $container->addContainer("slug_label");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$slug_label->addText($locale)
				->addConditionOn($container["slug_available"], Form::EQUAL, TRUE)
					->setRequired("Prosím zadejte požadovaný název v menu");
		}

		$slug_title = $container->addContainer("slug_title");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$slug_title->addText($locale);
		}

		$slug_meta_keywords = $container->addContainer("slug_meta_keywords");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$slug_meta_keywords->addText($locale);
		}

		$slug_meta_description = $container->addContainer("slug_meta_description");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$slug_meta_description->addText($locale);
		}

		$container->addText("slug_meta_author");
	}

	protected function createSlugPart()
	{
		if ($this->presenter->user->isAllowed("Admin:Slugs", "edit")) {
			$slug = $this->form->addContainer("slug");

			$this->addSlugFields($slug);
		}
	}

	protected function addSlugPart()
	{
		$this->createSlugPart();
	}
}