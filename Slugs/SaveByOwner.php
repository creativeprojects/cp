<?php

namespace CP\Services\Slugs;

use Nette\Utils\Strings;

class SaveByOwner
{
	protected $slug = array(
		"client_id" => null,
		"owner_table" => null,
		"owner_id" => null,
		"slug_action" => null,
		"slug_parameters" => array(),
		"slug_available" => 0,
		"slug_url" => array(),
		"slug_label" => array(),
		"slug_title" => array()
	);

	public function __construct($slugsService)
	{
		$this->slugsService = $slugsService;

		$this->slug["client_id"] = $this->slugsService->client["id"];
	}

	public function setOwner($owner_table, $owner_id)
	{
		$this->slug["owner_table"] = (string) $owner_table;

		$this->slug["owner_id"] = $owner_id;
	}

	public function setAction($slug_action)
	{
		$this->slug["slug_action"] = $slug_action;

		return $this;
	}

	public function setParameters($slug_parameters)
	{
		$this->slug["slug_parameters"] = $slug_parameters;

		return $this;
	}

	public function setAvailable($slug_available)
	{
		$this->slug["slug_available"] = $slug_available ? 1 : 0;

		return $this;
	}

	public function setUrl(array $slug_url)
	{
		foreach ($this->slugsService->translator->getAvailableLocales() AS $locale) {
			$this->slug["slug_url"][$locale] = Strings::webalize($slug_url[$locale], "/");
		}

		return $this;
	}

	public function setLabel(array $slug_label)
	{
		$this->slug["slug_label"] = $slug_label;

		return $this;
	}

	public function setTitle(array $slug_title)
	{
		$this->slug["slug_title"] = $slug_title;

		return $this;
	}

	public function createFrom($label, $default = null)
	{
		foreach ($this->slugsService->translator->getAvailableLocales() AS $locale) {
			if(!isset($this->slug["slug_url"][$locale]) /*|| $this->slug["slug_url"][$locale] == $this->translator->translate($default, null, null, $locale)*/){
				$this->slug["slug_url"][$locale] = Strings::webalize($label[$locale], "/");
				$this->slug["slug_title"][$locale] = $label[$locale];
			}
		}

		return $this;
	}

	public function save($values = null)
	{
		$slug = $this->slugsService->getOneByOwner($this->slug["owner_table"], $this->slug["owner_id"]);

		$slug = array_extend($slug, $this->slug);
		
		if(isset($values["slug"])){
			foreach ($this->slugsService->translator->getAvailableLocales() AS $locale) {
				$values["slug"]["slug_url"][$locale] = Strings::webalize($values["slug"]["slug_url"][$locale], "/");
			}

			$slug = array_extend($slug, $values["slug"]);
		}

		return $this->slugsService->save($slug);
	}
}