<?php

namespace CP\Forms;

use Nette,
	CP\Forms\Form;

class SlugForm extends \CP\Forms\Base
{
	use \CP\Forms\SlugForm\SlugPart;

	public function create()
	{
		$this->form->addHidden("slug_id");

		$this->form->addText("client_id")
			->addCondition(Form::FILLED)
				->addRule(Form::NUMERIC, "ID klienta musí být prázdná hodnota nebo číslo");

		$this->form->addText("owner_table");

		$this->form->addText("owner_id");

		$this->form->addText("slug_action");

		$this->form->addText("slug_parameters");

		$this->addSlugFields($this->form);

		$this->form->addSubmit("save")
			->onClick[] = array($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	protected function sanitizeInputValues()
	{
		$this->values["slug_parameters"] = @is_array($this->values["slug_parameters"]) ? json_encode($this->values["slug_parameters"]) : null;
	}

	protected function sanitizeOutputValues($values)
	{
		$values["slug_parameters"] = $values["slug_parameters"] ? json_decode($values["slug_parameters"], true) : null;

		return $values;
	}

	public function save($button)
	{
		$slug = $button->parent->getValues(true);

		$slug = $this->sanitizeOutputValues($slug);

		if ($this->presenter->slugsService->save($slug)) {
			$this->presenter->flashMessage("URL adresa byla uložena", "alert-success");

			$this->presenter->restoreRequest($this->presenter->backlink);
			$this->presenter->redirect("Slugs:default");
		} else {
			$this->presenter->flashMessage("Při ukládání URL adresy došlo k chybě!", "alert-danger");
		}
	}
}