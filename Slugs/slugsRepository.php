<?php

namespace Repository;

class slugs extends \Repository\BaseRepository
{
	public $name = "slugs";

	public $fields = array(
		"slug_id" => "primary",
		"client_id" => "int",
		"owner_table" => "varchar",
		"owner_id" => "int",
		"slug_action" => "varchar",
		"slug_parameters" => array(
			"type" => "json",
		//	"notNull" => true
		),
		"slug_available" => "bool",
		"slug_url" => array(
			"type" => "varchar",
			"locale" => true
		),
		"slug_label" => array(
			"type" => "varchar",
			"locale" => true
		),
		"slug_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"slug_meta_keywords" => array(
			"type" => "varchar",
			"locale" => true
		),
		"slug_meta_description" => array(
			"type" => "varchar",
			"locale" => true
		),
		"slug_meta_author" => "varchar",
		"slug_hash" => "varchar"
	);

	public $validateTable = true;
}