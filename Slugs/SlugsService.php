<?php

namespace CP\Services;

use Nette;
use CP;
use CP\Grid\Grid;
use Nette\Utils\Strings;

class Slugs extends CP\Service
{
	protected $filesService;

	protected $masks = array();

	use CP\Services\Slugs\Cache;

	use CP\Services\Slugs\CRUD;

	use CP\Services\Slugs\GetAll;

	use CP\Services\Slugs\Router;

	use CP\Services\Slugs\GetOne;

	public function __construct(
		Nette\Database\Context $database, 
		Nette\Security\IUserStorage $user, 
		Nette\Localization\ITranslator $translator, 
		CP\Files\Service $filesService,
		CP\Services\Clients $clientsService,
		CP\Regions\Service $regionsService
	)
	{
		parent::__construct($database, $user, $translator, $clientsService);
		
		$this->filesService = $filesService;

		$this->regionsService = $regionsService;

		$this->serviceClient($clientsService);

		$this->loadCachedMasks();
	}

	public function addSlugUrl(& $form)
	{
		$slug_url = $form->addContainer("slug_url");

		foreach($this->translator->getAvailableLocales() AS $locale)
			$slug_url->addText($locale);
	}

	public function addMask($mask, $params = array())
	{
		$this->masks[$mask] = $params;

		return $this;
	}
}