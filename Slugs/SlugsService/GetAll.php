<?php

namespace CP\Services\Slugs;

use CP\Grid\Grid;

trait GetAll
{
	public function getGridByClient($query = array())
	{
		$slugsGrid = new Grid($this->database);

		$slugsGrid->query = array_extend(
			array(
			'fields' => array(
				"s.*",
				"l.slug_url",
				"l.slug_label",
				"l.slug_title",
			),
			'from' => "{$this->repository->slugs} AS s LEFT JOIN {$this->repository->slugs->getNameLocale()} AS l ON l.slug_id = s.slug_id",
			'where' => array(
				"client_id" => "(s.client_id IS NULL OR s.client_id = " . $this->client['id'] . ")",
				"removed" => "s.removed IS NULL"
			)), $query
		);

		return $slugsGrid;
	}

	public function getMenu($menu)
	{
		$items = $this->database->query("SELECT l.*, s.slug_action, s.slug_parameters FROM {$this->repository->menuItems->getName()} AS m LEFT JOIN {$this->repository->slugs->getName()} AS s ON s.slug_id = m.slug_id LEFT JOIN {$this->repository->slugs->getNameLocale()} AS l ON l.slug_id = s.slug_id WHERE m.removed IS NULL AND m.client_id = {$this->client["id"]} AND m.menu = '{$menu}' ORDER BY m.item_rank ASC")->fetchAll();

		foreach ($items AS & $item) {
			$item = (array) $item;
			$item["slug_parameters"] = (array)json_decode($item["slug_parameters"], true);
		}

		return $items;
	}

	public function getAvailableOptions()
	{
		$by = array(
			"slug_available" => 1,
			"client_id = {$this->client["id"]} OR client_id IS NULL"
		);

		$slugs = $this->repository->slugs->getAllByLocale($by);

		$options = array();

		foreach($slugs AS $slug)
			$options[$slug["slug_id"]] = $slug["slug_label"];

		return $options;
	}
}