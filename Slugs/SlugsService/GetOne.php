<?php

namespace CP\Services\Slugs;

use Nette\Utils\Strings;

trait GetOne
{

	public function getDefault($slug_url, $slug_label, $slug_title = null)
	{
		$slug = array(
			"slug_url" => array(),
			"slug_label" => array(),
			"slug_title" => array()
		);

		foreach ($this->translator->getAvailableLocales() AS $locale) {
			$slug["slug_url"][$locale] = Strings::webalize($this->regionsService->getLocalizedString($slug_url, $locale), "/");
			$slug["slug_label"][$locale] = $this->regionsService->getLocalizedString($slug_label, $locale);
			$slug["slug_title"][$locale] = $slug_title ? $this->regionsService->getLocalizedString($slug_title, $locale) : null;
		}
		
		return $slug;
	}

	public function getOneById($slug_id)
	{
		$by = array(
			"slug_id" => $slug_id
		);

		$slug = $this->getOneBy($by);

		return $slug;
	}

	public function getOneBy($by)
	{
		return $this->repository->slugs->getOneBy($by);
	}

	public function getByOwner($owner_table, $owner_id)
	{
		return $this->getOneByOwner($owner_table, $owner_id);
	}

	public function getOneByOwner($owner_table, $owner_id)
	{
		$slug = $this->getOneBy(array(
			"owner_table" => $owner_table,
			"owner_id" => $owner_id
		));

		return $slug;
	}

	public function getSqlSlugLabelByOwnerLocale($owner_table, $owner_id, $as = "slug_label")
	{
		return $this->getSqlFieldByOwnerLocale("slug_label", $owner_table, $owner_id, $as);
	}

	public function getSqlSlugTitleByOwnerLocale($owner_table, $owner_id, $as = "slug_title")
	{
		return $this->getSqlFieldByOwnerLocale("slug_title", $owner_table, $owner_id, $as);
	}

	public function getSqlSlugUrlByOwnerLocale($owner_table, $owner_id, $as = "slug_url")
	{
		return $this->getSqlFieldByOwnerLocale("slug_url", $owner_table, $owner_id, $as);
	}

	public function getSqlSlugUrlLocale($owner_table, $owner_id, $as = "slug_url")
	{
		return $this->getSqlFieldByOwnerLocale("slug_url", $owner_table, $owner_id, $as);
	}

	public function getSqlFieldByOwnerLocale($field, $owner_table, $owner_id, $as = null)
	{
		return "(SELECT {$field} FROM {$this->repository->slugs} AS s LEFT JOIN {$this->repository->slugs->getNameLocale()} AS l ON l.slug_id = s.slug_id WHERE owner_table = '{$owner_table}' AND owner_id = {$owner_id} LIMIT 1)" . ($as ? " AS {$as}" : null);
	}

	public function getSlugUrlByOwner($owner_table, $owner_id)
	{
		$slug = $this->getSlugByOwner($owner_table, $owner_id);

		return $slug["slug_url"];
	}

	public function getSlugByOwner($owner_table, $owner_id)
	{
		$by = array(
			"owner_table" => $owner_table,
			"owner_id" => $owner_id
		);

		$slug = $this->repository->slugs->getOneBy($by);

		return $slug;
	}
}
