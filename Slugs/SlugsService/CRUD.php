<?php

namespace CP\Services\Slugs;

use CP\Services\Slugs\SaveByOwner;
use Nette\Utils\Strings;

trait CRUD
{
	public function save($slug)
	{
		$slug = $this->sanitizeInputValues($slug);

		$slug = $this->repository->slugs->save($slug);

		$this->cache($slug);

		return $slug;
	}

	protected function sanitizeInputValues($values)
	{
		$values["slug_url"] = $this->getUniqueSlugUrl($values);

		$values["slug_available"] = @$values["slug_available"] ? 1 : 0;

		$values["slug_hash"] = $this->getHash($values, $values["client_id"]);

		return $values;
	}

	public function saveAvailability($slug_id, $value)
	{
		return $this->repository->slugs->saveField($slug_id, "slug_available", $value);
	}

	public function refactorAllSlugHashes($forced = false)
	{
		$by = array(
			"client_id = {$this->client["id"]} OR client_id IS NULL"
		);

		if(!$forced)
			$by["slug_hash"] = null;

		$slugs = $this->repository->slugs->getAllBy($by);

		foreach($slugs AS $slug){
			$slug["slug_hash"] = $this->getHash($slug);

			//dump($slug);

			$this->repository->slugs->save($slug);
		}
	}

	protected function saveSlugHash($slug)
	{
		$slug_hash = $this->getHash($slug);

		$this->repository->slugs->saveField($slug["slug_id"], "slug_hash", $slug_hash);
	}

	public function saveByOwner($owner_table, $owner_id)
	{
		$saveByOwner = new SaveByOwner($this);

		$saveByOwner->setOwner($owner_table, $owner_id);

		return $saveByOwner;
	}

	private function getUniqueSlugUrl($values)
	{
		$iterator = 1000;

		foreach ($this->translator->getAvailableLocales() AS $locale) {
			$i = 1;

			do {
				$slug_url[$locale] = Strings::webalize($values["slug_url"][$locale], "/") . ($i > 1 ? "-{$i}" : null);

				$i++;

				$where = array(
					"slug_url" => $slug_url[$locale],
					"slug_id !" => (@$values["slug_id"] ? $values["slug_id"] : 0),
					"client_id" => $this->client["id"]
				);

				$iterator--;
			} while ($iterator && $this->repository->slugs->getCountBy($where, $locale));
		
			$slug_url[$locale] = strlen($slug_url[$locale]) ? $slug_url[$locale] : null;
		}

		return $slug_url;
	}

	public function remove($slug_id)
	{
		$by = array(
			"slug_id" => $slug_id
		);

		return $this->removeBy($by);
	}

	public function removeBy($by)
	{
		return $this->repository->slugs->removeBy($by);
	}

	public function removeByOwner($owner_table, $owner_id)
	{
		$by = array(
			"owner_table" => $owner_table,
			"owner_id" => $owner_id
		);

		return $this->removeBy($by);
	}
}