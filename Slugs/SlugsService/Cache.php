<?php

namespace CP\Services\Slugs;

use Nette\Utils\Strings;

trait Cache
{
	protected function loadCachedMasks()
	{
		$cache = $this->initCache("masks");

		$this->masks = (array) $cache->load("masks");
	}

	protected function cacheMask($mask, $params = array())
	{
		$cache = $this->initCache("masks");

		$masks = array_merge(array($mask => $params), $this->masks);

		$cache->save("masks", $masks);
	}

	public function getFromCacheBySlugUrl($slug_url)
	{
		$cache = $this->initCache($this->client["client_id"]);

		$slug = $cache->load($slug_url);

		return $slug;
	}

	public function getFromCacheBySlugHash($slug_hash)
	{
		$slug = false;

		if($this->settings["cache"]){
			$cache = $this->initCache($this->client["client_id"]);

			$slug = $cache->load($slug_hash);
		}

		return $slug;
	}

	protected function cache($slug)
	{
		if($this->settings["cache"]){
			$cache = $this->initCache($this->client["client_id"]);

			$cache->save($slug["slug_hash"], $slug);

			foreach($this->translator->getAvailableLocales() AS $locale)
				$cache->save($slug["slug_url"][$locale], $slug);
		}
	}

	protected $cache = array();

	protected function initCache($dir = null)
	{
		$dir = Strings::webalize(($dir ? $dir : "default"));

		if(!isset($this->cache[$dir])){
			$cacheDir = TEMP_DIR . "/cache/slugs/" . $dir;
			
			$this->filesService->validateDir($cacheDir, true);
			
			$storage = new \Nette\Caching\Storages\FileStorage($cacheDir);

			$this->cache[$dir] = new \Nette\Caching\Cache($storage);
		}

		return $this->cache[$dir];
	}
}