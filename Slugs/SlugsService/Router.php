<?php

namespace CP\Services\Slugs;

trait Router
{

	public function filterPageIn($params)
	{
		if ($this->client["id"]) {
			$route = $this->getRoute($params['slug']);

			if ($route) {
				return array_merge($params, $route);
			}
		}

		return NULL;
	}

	public function filterPageOut($params)
	{
		if ($this->client["id"]) {
			$params = $this->getSlug($params);

			return $params;
		}
		return NULL;
	}

	public function getUnmaskedSlug($slug)
	{
		//dump($slug);

		$where = array(
			"slug_action = " . $this->quote($slug["slug_action"]),
			"client_id = {$this->client["id"]} OR client_id IS NULL"
		);

		$addWhere = array();

		$count = array();

		foreach ($slug["slug_parameters"] AS $param => $value) {
			$temp = "\"{$param}\":" . (is_numeric($value) ? $value : (is_array($value) ? json_encode($value) : "\"{$value}\""));

			$tempNot = "\"{$param}\"";

			$addWhere[] = "IFNULL(slug_parameters, '') LIKE ('%{$temp}%') OR IFNULL(slug_parameters, '') NOT LIKE ('%{$tempNot}%')";

			$count[] = "(SELECT COUNT(*) FROM {$this->repository->slugs->getNameLocale()} WHERE (" . implode(") AND (", $where) . ") AND IFNULL(slug_parameters, '') LIKE ('%{$temp}%'))";
		}

		$query = array(
			"where" => array_merge($where, $addWhere),
			"additionalFields" => array(
				(count($count) ? "(" . implode(" + ", $count) . ")" : 0) . " AS relevance"
			),
			"order" => array(
				"relevance" => "DESC"
			)
		);

		$slug = $this->repository->slugs->getOneBy($query);

		//dump($slug);

		if ($slug) {
			$this->cacheMask($slug["slug_action"], array_keys((array)$slug["slug_parameters"]));

			$this->saveSlugHash($slug);

			return $slug;
		}

		return NULL;
	}

	public function getSlug($params)
	{
		$slug = array();

		$slug["slug_action"] = $slug_action = $params['presenter'] . ':' . $params['action'];
		$slug["client_id"] = $this->client["id"];

		unset($params['presenter'], $params['action'], $params["slug"]);

		$slug["slug_parameters"] = (array)$params;

		if (array_key_exists($slug["slug_action"], $this->masks)) {
			$slug["slug_parameters"] = array();

			$maskParams = (array)$this->masks[$slug["slug_action"]];

			foreach ($maskParams AS $param) {
				if (array_key_exists($param, $params))
					$slug["slug_parameters"][$param] = $params[$param];
			}

			//dump($slug);
		} else {
			$unmaskedSlug = $this->getUnmaskedSlug($slug);

			if ($unmaskedSlug)
				$slug = $unmaskedSlug;

			if (DEBUG_MODE)
				$this->refactorAllSlugHashes();
		}

		//dump($slug);

		$slugHashes = array(
			$this->getHash($slug),
			$this->getHash($slug, false)
		);

		//dump($slugHashes);
		//foreach ($slugHashes AS $slug_hash)
		//	if ($slug = $this->getFromCacheBySlugHash($slug_hash))
		//		break;

		//$this->refactorAllSlugHashes(true);


		$slug = false;
		if (!$slug) {
			//dump($slug_action);
			//die;
			//dump($slugHashes);
			//dump($slug_hash);

			$properties = array(
				"where" => array(
					"slug_hash" => $slugHashes,
					"client_id = {$this->client["id"]} OR client_id IS NULL"
				),
				"order" => array(
					"IF(client_id IS NOT NULL, 1, 0)" => "DESC"
				)
			);

			$slug = $this->repository->slugs->getOneBy($properties);

			//dump($slug);

			if (!$slug) {
				return null;
			}

			$this->cache($slug);
		}


		if (is_array($slug["slug_parameters"]))
			foreach (array_keys($slug["slug_parameters"]) AS $param)
				unset($params[$param]);

		$params["slug"] = $slug["slug_url"][$this->translator->getLocale()];

		return $params;
	}

	protected function getHash($slug, $includeClient = true)
	{
		$slug["slug_parameters"] = (array)$slug["slug_parameters"];

		ksort($slug["slug_parameters"]);

		$slug["slug_parameters"] = json_encode($slug["slug_parameters"], JSON_NUMERIC_CHECK);

		$stringToHash = $slug["slug_action"] . $slug["slug_parameters"] . ($includeClient ? $slug["client_id"] : null);

		//dump($stringToHash);

		$slug_hash = md5($stringToHash);

		//dump($slug_hash);

		return $slug_hash;
	}

	public function getRoute($slug_url)
	{
		$slug = $this->getSlugForRoute($slug_url);

		if ($slug) {
			$route = array(
				"slug" => $slug
			);

			list($route["presenter"], $route["action"]) = explode(':', $slug["slug_action"]);

			if ($slug["slug_parameters"])
				$route = array_extend($route, $slug["slug_parameters"]);

			return $route;
		}

		return NULL;
	}

	public function getSlugForRoute($slug_url)
	{
		$slug = $this->getFromCacheBySlugUrl($slug_url);

		if ($slug) {
			foreach ($this->translator->getAvailableLocales() AS $locale) {
				if ($slug_url == $slug["slug_url"][$locale] && $locale != $this->translator->getLocale()) {
					$this->translator->setLocale($locale);
				}
			}
		} else {
			$by = array(
				"slug_url" => $slug_url,
				"client_id = {$this->client["id"]} OR client_id IS NULL"
			);

			$slug = $this->repository->slugs->getOneByLocale($by);

			if (!$slug) {

				foreach ($this->translator->getAvailableLocales() AS $locale) {
					if ($locale != $this->translator->getLocale()) {
						$slug = $this->repository->slugs->getOneByLocale($by, $locale);

						if ($slug) {
							$this->translator->setLocale($locale);

							break;
						}
					}
				}
			}
		}

		return $slug;
	}
}