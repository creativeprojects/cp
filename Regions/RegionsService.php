<?php

namespace CP\Regions;

use Nette;
use CP;
use Nette\Utils\Neon;
use Nette\Utils\Strings;

class Service extends CP\Service
{
	public $settings = array(
		"defaultCountry" => "CZ",
		"defaultCurrency" => "Kč"
	);

	protected $country;

	public function setCountry($country = null)
	{
		if (!$country)
			$country = $this->settings["defaultCountry"];

		$this->country = $country;
	}

	public function getCountry()
	{
		if (!$this->country)
			$this->setCountry();

		return $this->country;
	}
	protected $countries;

	public function getCountries()
	{
		return $this->getCountryOptions();
	}

	public function getCountryOptions()
	{
		if (!$this->countries)
			$this->countries = Neon::decode(file_get_contents(CP_DIR . "/Regions/countries.neon"));

		return $this->countries;
	}
	protected $currencies;

	public function getCurrencies()
	{
		if (!$this->currencies)
			$this->currencies = Neon::decode(file_get_contents(CP_DIR . "/Regions/currencies.neon"));

		return $this->currencies;
	}

	public function getCurrency()
	{
		$currencies = $this->getCurrencies();

		$currency = array_key_exists($this->getCountry(), $currencies) ? $currencies[$this->getCountry()] : $this->settings["defaultCurrency"];

		return $currency;
	}

	public function getLocalizedString($input, $locale)
	{
		if (is_array($input)) {
			if (array_key_exists($locale, $input)){
				$output = $input[$locale];
			} else {
				$output = $input;//$this->translator->translate(reset($input), NULL, array(), NULL, $locale);
			}
		} else {
			$output = $input;//$this->translator->translate($input, NULL, array(), NULL, $locale);
		}

		return $output;
	}
}