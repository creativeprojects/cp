<?php

namespace Repository;

class users extends \Repository\BaseRepository
{
	public $name = "users";

	public $fields = array(
		"user_id" => "primary",
		"client_id" => "int",
		"role" => "varchar",
		"user_email" => "varchar",
		"user_password" => "varchar",
		"user_title" => "varchar",
		"user_lastloged" => "timestamp",
		"user_lastloged_ip" => "varchar",
		"user_activation_code" => "varchar",
		"user_recovery_code" => "varchar",
		"user_redirect_onlogin" => "varchar",
		"locale" => array(
			"type" => "varchar",
			"deprecated" => "language_key"
		),
		"facebook_id" => "varchar",
		"google_id" => "varchar",
		"twitter_id" => "varchar"
	);

	protected $defaultRows = array(
		array(
			"user_id" => 1,
			"client_id" => 1,
			"role" => "superAdmin",
			"user_email" => "info@creativeprojects.cz",
			"user_password" => '$2y$10$bXcec4xXFgjkiFQAytBuheX13CYplpUKY6mFpVMnEsxFN9PiSf9y2',
			"user_title" => "Creative Projects s.r.o"
		)
	);

	protected $validateTable = true;
}