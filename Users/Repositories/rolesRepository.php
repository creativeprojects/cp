<?php

namespace Repository;

class roles extends \Repository\BaseRepository
{
	public $name = "roles";

	public $fields = array(
		"role" => array(
			"type" => "varchar",
			"key" => "primary",
			"notNull" => true,
			"length" => 30
		),
		"parent_role" => array(
			"type" => "varchar",
			"length" => 30
		),
		"role_title" => array(
			"type" => "varchar",
			"locale" => true
		)
	);

	protected $defaultRows = array(
		array(
			"role" => "superAdmin",
			"parent_role" => null,
			"role_title" => array(
				"cs" => "Programátor",
				"en" => "Programmer"
			)
		),
		array(
			"role" => "admin",
			"parent_role" => "superAdmin",
			"role_title" => array(
				"cs" => "Správce",
				"en" => "Admin"
			)
		)
	);

	//protected $forceRowsValidation = true;

	protected $validateTable = true;
}