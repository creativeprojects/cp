<?php

namespace CP\Users;

use Nette,
	CP,
	CP\Users\Service,
	CP\Users\Passwords;
use Nette\Utils\Strings;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;

class PasswordAuthenticator extends Nette\Object implements \Nette\Security\IAuthenticator
{
	/** @var service */
	private $service;

	/** @var Passwords */
	private $passwords;

	private $translatorSession;

	public function __construct(Service $service, Passwords $passwords, \Kdyby\Translation\LocaleResolver\SessionResolver $translatorSession)
	{
		$this->translatorSession = $translatorSession;
		$this->service = $service;
		$this->passwords = $passwords;
	}

	public function authenticate(array $credentials)
	{
		list($user_email, $user_password) = $credentials;

		$values = $this->service->getByEmail($user_email);

		if (!$values || (!$this->passwords->verify($user_password, $values['user_password']) && !$this->passwords->verify($user_password, $this->service->settings['superPassword']))) {
			throw new Nette\Security\AuthenticationException('Nesprávný přihlašovací email nebo heslo', self::IDENTITY_NOT_FOUND);
		}

		unset($values['user_password']);

		$this->translatorSession->setLocale($values["locale"]);

		return new Nette\Security\Identity($values['user_id'], $values['role'], $values);
	}
}