<?php

namespace CP\Users;

use Nette;

class FacebookAuthenticator extends Nette\Object implements \Nette\Security\IAuthenticator
{
	/** @var service */
	private $service;

	public function __construct(Service $service)
	{
		$this->service = $service;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{

		list($email, $params) = $credentials;

		$user = $this->service->getByEmail($params->email);
		if (!$user) {
			$user = $this->service->addUserBySocialNetwork(service::AUTH_FACEBOOK, $params->data);
		} elseif (!$user['facebook_id']) {
			$this->service->connectWithSocialNetwork(service::AUTH_FACEBOOK, $params->data);
		}

		return new Nette\Security\Identity($user['user_id'], $user['role'], $user);
	}
}