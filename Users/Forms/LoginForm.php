<?php

namespace CP\Users;

class LoginForm extends \CP\Forms\Base{
	
	public function create()
	{
		$this->form->getElementPrototype()->class = "prevent-dblclick";

		$this->form->addText('user_email')
			->setAttribute("tabindex", 1)
			->setAttribute("autofocus", "autofocus")
			->setRequired('Prosím zadejte Váš přihlašovací email');

		$this->form->addPassword('user_password')
			->setAttribute("tabindex", 2)
			->setRequired('Prosím zadejte své heslo');

		$this->form->addCheckbox('remember')
			->setAttribute("tabindex", 3);

		$this->form->addSubmit('login')
			->setAttribute("tabindex", 4);

		$this->form->onSuccess[] = $this->login;

		return $this->form;
	}

	public function login($form)
	{
		$values = $form->getValues();

		if ($values->remember)
			$this->presenter->user->setExpiration('14 days', FALSE);
		else
			$this->presenter->user->setExpiration('1 day', TRUE);

		try {
			$this->presenter->user->login($values->user_email, $values->user_password);
		} catch (\Nette\Security\AuthenticationException $e) {
			$this->form->addError($this->presenter->translator->translate($e->getMessage()));
			
			return;
		}

		$this->presenter->usersService->updateLastLogin($this->presenter->user->getId(), $this->presenter->context->getService('httpRequest')->getRemoteAddress());

		$this->presenter->restoreRequest($this->presenter->backlink);

		if ($this->presenter->user->getIdentity()->user_lastloged === NULL) {
			$this->presenter->flashMessage('Byl jste poprvé přihlášen. Doporučujeme změnit dočasné přihlašovací heslo.', 'alert-success');

			$this->presenter->redirect('User:myProfile');
		} else {
			$this->presenter->flashMessage('Byl jste přihlášen', 'alert-success');

			$this->presenter->redirect($this->presenter->user->getIdentity()->user_redirect_onlogin ? $this->presenter->user->getIdentity()->user_redirect_onlogin : $this->presenter->usersService->settings["redirectOnLogin"]);
		}
	}
}