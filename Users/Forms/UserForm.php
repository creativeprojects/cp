<?php

namespace CP\Users;

use CP\Forms\Form;

class UserForm extends \CP\Forms\Base
{

	public function create()
	{
		$this->addUserFields();

		$this->form->addSubmit('save')
			->onClick[] = callback($this, "save");

		$this->form->setValues($this->values);

		return $this->form;
	}

	protected function addUserFields()
	{
		$this->addLocale();

		$this->addRole();

		$this->addUserId();

		$this->addUserTitle();

		$this->addUserEmail();
	}

	protected function addLocale()
	{
		if ($this->presenter->multiLocale) {
			$this->form->addSelect("locale", null, $this->presenter->usersService->regionsService->settings["locales"]);
		} else {
			$this->form->addHidden("locale")->setDefaultValue($this->presenter->translator->getLocale());
		}
	}

	protected function addRole()
	{
		if ($this->presenter->user->isInRole("superAdmin")) {
			$this->form->addSelect('role', null, $this->presenter->usersService->getRoleOptions());
		} else {
			$this->form->addHidden("role");
		}
	}

	protected function addUserId()
	{
		$this->form->addHidden("user_id");
	}

	protected function addUserTitle($name = "user_title")
	{
		$this->form->addText($name)
			->setOption("required", true)
			->setRequired('Prosím zadejte jméno svého účtu');
	}

	protected function addUserEmail($name = "user_email")
	{
		$this->form->addText($name)
			->setOption("required", true)
			->addRule(Form::EMAIL, 'Zadejte platnou emailovou adresu')
			->addRule(callback($this, "isEmailUnique"), 'Tento email je již obsazen', $this->values["user_id"])
			->setRequired('Prosím zadejte přihlašovací email ke svému účtu');
	}

	public function isEmailUnique($userEmailInput, $user_id)
	{
		$user_email = $userEmailInput->getValue();

		return $this->presenter->usersService->isEmailUnique($user_email, $user_id);
	}

	public function save($button)
	{
		$profile = $button->parent->getValues();

		$signUp = $profile["user_id"] ? false : true;

		if ($profile = $this->presenter->usersService->save($profile)) {
			if ($profile["user_id"] != $this->presenter->usersService->user["user_id"] && ($this->presenter->user->isInRole("seller") || $this->presenter->user->isInRole("admin") || $this->presenter->user->isInRole("superAdmin"))) {
				if ($signUp) {
					$this->presenter->flashMessage("Uživatelský účet byl vytvořen. Přihlašovací údaje byly odeslány na email uživatele.", 'alert-success');
				} else {
					$this->presenter->flashMessage("Změny v uživatelském účtu byly uloženy", 'alert-success');
				}

				$this->redirect("Users:default");
			} else {
				if ($signUp) {
					$this->presenter->flashMessage("Váš uživatelský účet byl vytvořen. Přihlašovací údaje byly odeslány na Váš email", 'alert-success');

					$this->redirect("User:signedUp");
				} else {
					$this->presenter->flashMessage("Změny ve Vašem uživatelském účtu byly uloženy", 'alert-success');

					$this->presenter->user->getIdentity()->user_title = $profile["user_title"];

					$this->presenter->user->getIdentity()->locale = $profile["locale"];

					$this->redirect("User:myProfile");
				}
			}
		} else {
			if ($signUp) {
				$this->presenter->flashMessage("Při vytváření uživatelského účtu došlo k chybě!", 'alert-danger');
			} else {
				$this->presenter->flashMessage("Při ukládání změn uživatelského účtu došlo k chybě!", 'alert-danger');
			}
		}
	}

	protected function redirect($redirect)
	{
		$this->presenter->redirect($redirect);
	}
}