<?php

namespace CP\Users;

use CP\Forms\Form;

class PasswordForm extends \CP\Forms\Base
{
	protected function newPasswordFields()
	{
		$this->form->addPassword('newPassword')
			->setRequired('Prosím zadejte požadované heslo');

		$this->form->addPassword('verifyPassword')
			->setOmitted()
			->setRequired('Prosím opiště požadované heslo')
			->addRule(Form::EQUAL, 'Požadované heslo a jeho kontrola se neshodují', $this->form['newPassword']);
	}

	public function createSuperAdminChange($user_id)
	{
		$this->form->addHidden("user_id")
			->setDefaultValue($user_id);
		
		$this->newPasswordFields();

		$this->form->addSubmit('save');

		$this->form->onSuccess[] = $this->superAdminChangeSucceeded;

		return $this->form;
	}

	public function createChange($user_id = null)
	{
		$this->form->addPassword('user_password')
		->setRequired('Prosím zadejte Vaše současné heslo');
		
		$this->newPasswordFields();

		$this->form->addSubmit('save');

		$this->form->onSuccess[] = $this->changeSucceeded;

		return $this->form;
	}

	public function createForgotten()
	{
		$this->form->addText('user_email')
			->setDefaultValue(@$this->values["user_email"])
			->setRequired('Zadejte svůj přihlašovací email');

		$this->form->addSubmit('reset');

		$this->form->onSuccess[] = $this->forgottenSucceeded;

		return $this->form;
	}

	public function createReset()
	{
		$this->newPasswordFields();

		$this->form->addHidden("user_recovery_code");

		$this->form->addSubmit('save');

		$this->form->onSuccess[] = $this->resetSucceeded;

		$this->form->setValues($this->values);

		return $this->form;
	}

	public function changeSucceeded($form)
	{
		$values = $form->getValues(true);

		$user_id = $this->presenter->user->getId();

		$currentPassword = $this->presenter->usersService->getPassword($user_id);
		
		if (Passwords::verify($values['user_password'], $currentPassword)) {
			if ($this->presenter->usersService->changePassword($user_id, $values['newPassword'])) {
				$this->presenter->flashMessage('Vaše heslo bylo uloženo', 'alert-success');

				$this->presenter->redirect('User:myProfile');
			} else {
				$this->presenter->flashMessage('Vaše heslo se nepodařilo uložit!', 'alert-danger');
			}
		} else {
			$this->presenter->flashMessage('Vaše současné heslo nebylo zadáno správně', 'alert-danger');
		}
	}

	public function superAdminChangeSucceeded($form)
	{
		$values = $form->getValues(true);

		if ($this->presenter->usersService->changePassword($values["user_id"], $values['newPassword'])) {
				$this->presenter->flashMessage('Heslo uživatele bylo uloženo', 'alert-success');

				$this->presenter->restoreRequest($this->presenter->backlink);
				$this->presenter->redirect("this");
		} else {
			$this->presenter->flashMessage('Heslo uživatele se nepodařilo uložit!', 'alert-danger');
		}
	}

	public function resetSucceeded($form)
	{
		$values = $form->getValues();

		if ($this->presenter->usersService->resetPassword($values['user_recovery_code'], $values['newPassword'])) {
			$this->presenter->flashMessage('Vaše heslo bylo uloženo', 'alert-success');

			$this->presenter->redirect('User:Login');
		} else {
			$this->presenter->flashMessage('Vaše heslo se nepodařilo uložit!', 'alert-danger');
		}
	}

	public function forgottenSucceeded($form)
	{
		$values = $form->getValues();

		if ($values = $this->presenter->usersService->getByEmail($values['user_email'])) {
			$this->presenter->usersService->createRecoveryCode($values['user_id']);

			$this->presenter->flashMessage("Žádost o obnovení hesla byla zaslána na Váš email", 'alert-success');

			$this->presenter->redirect('User:Login');
		} else {
			$this->presenter->flashMessage("Uživatelský účet nebyl nalezen!", 'alert-danger');
		}
	}
}