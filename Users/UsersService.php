<?php

namespace CP\Users;

use Nette,
	Table,
	Nette\Utils\Strings,
	CP;
use CP\Grid\Grid;
use Nette\Utils\Validators;

abstract class Service extends CP\Base\Service
{
	const
		AUTH_CUSTOM = 1,
		AUTH_FACEBOOK = 2,
		AUTH_GOOGLE = 3,
		AUTH_TWITTER = 4;

	public $settings = array(
		"signUp" => false,
		"redirectOnLogin" => "Homepage:",
		"default" => array(
			"user_id" => 0,
			"role" => "user",
		),
		"superPassword" => '$2y$10$Atdy.kfSIyYmEQL4YdxG6edX62d8J9blt8l1p5X8zNz8HWHFfgoYi'
	);

	/*protected function contruct()
	{
		$this->slugsService
			->addMask("User:login")
			->addMask("User:forgottenPassword")
			->addMask("User:resetPassword")
			->addMask("User:logout")
			->addMask("User:signUp");
	}*/

	protected function callback($operation, & $user)
	{
		if ($user['role']) {
			$roleCallback = lcfirst($operation) . ucfirst($user['role']);

			if (method_exists($this, $roleCallback)) {
				$userCustom = callback($this, $roleCallback)->invokeArgs(array($user));

				if ($userCustom)
					$user = array_extend($user, $userCustom);
			}

			return $user;
		} else {
			throw new Exception("Role not set for user callback!");
		}
	}

	public function getNew($profile = array())
	{
		$profile = array_extend(
			$this->settings["default"], array_extend(
				array(
			"locale" => $this->translator->getLocale()
				), $profile
			)
		);

		$profile["client_id"] = $this->client["id"];

		$this->callback("getNew", $profile);

		return $profile;
	}

	public function getGrid($query = array())
	{
		$profilesGrid = new Grid($this->database);

		$profilesGrid->query = array_extend(array(
			'fields' => array(
				'r.role_title',
				'u.*',
			),
			'from' => "{$this->repository->users} AS u LEFT JOIN {$this->repository->roles->getNameLocale()} AS r ON (u.role = r.role)",
			'limit' => null,
			'where' => array(
				"u.removed IS NULL",
				"u.client_id = " . $this->client['id']
			)
			), $query);

		$profilesGrid->itemsCallback = callback($this, "gridItemCallback");

		return $profilesGrid;
	}

	public function gridItemCallback($profile)
	{
		$profile["editable"] = $this->isMeOrMySubordinate($profile["user_id"]);

		return $profile;
	}

	public function getOneById($user_id)
	{
		return $this->getById($user_id);
	}

	public function getById($user_id)
	{
		if ($user_id) {
			$by = array(
				'user_id' => $user_id
			);

			return $this->getOneBy($by);
		}

		return false;
	}

	public function getByEmail($user_email)
	{
		if (Validators::isEmail($user_email)) {
			$by = array(
				'user_email' => $user_email
			);

			return $this->getOneBy($by);
		}

		return false;
	}

	public function getOneBy($by)
	{
		if (isset($this->client["id"]))
			$by[] = "client_id = {$this->client["id"]} OR client_id IS NULL";

		$user = $this->repository->users->getOneBy($by);

		if ($user) {
			$this->callback("getOne", $user);

			return $user;
		}

		return false;
	}

	public function getByIdLocale($user_id)
	{
		return $this->getOneByIdLocale($user_id);
	}

	public function getOneByIdLocale($user_id)
	{
		if ($user_id) {
			$by = array(
				"user_id" => $user_id
			);

			$user = $this->getOneByLocale($by);

			return $user;
		}

		return false;
	}

	protected function getOneByLocale($by)
	{
		$by[] = "client_id = {$this->client["id"]} OR client_id IS NULL";

		$user = $this->repository->users->getOneByLocale($by);

		if ($user) {
			$this->callback("getOneLocale", $user);

			return $user;
		}

		return false;
	}

	public function getRoleTitle($role)
	{
		$role = $this->repository->roles->getOneByLocale(array(
			'role' => $role
		));

		return $role['role_title'];
	}

	public function getMe()
	{
		return $this->getById($this->user["user_id"]);
	}

	public function getMeLocale()
	{
		$by = array(
			"user_id" => $this->user["user_id"]
		);

		return $this->getOneByLocale($by);
	}

	public function getOneByTwitterId($twitter_id)
	{
		$by = array(
			"twitter_id" => $twitter_id
		);

		$user = $this->getOneBy($by);

		return $values;
	}

	public function getRoleOptions()
	{
		$role = $this->user['role'];

		$roles = $this->database->query(""
				. "SELECT r.*, l.role_title FROM {$this->repository->roles->getName()} AS r "
				. "LEFT JOIN {$this->repository->roles->getNameLocale()} AS l ON r.role = l.role")
			->fetchPairs('role');

		$subRoles = $this->getSubRoles($role, $roles);

		return $subRoles;
	}

	public function getPassword($user_id)
	{
		$values = $this->repository->users->getOne($user_id);

		return $values['user_password'];
	}

	public function existsBy($by)
	{
		$by["client_id"] = $this->client["id"];

		$count = $this->repository->users->getCountBy($by);

		return $count ? true : false;
	}

	public function isEmailUnique($user_email, $user_id = null)
	{
		$by = array(
			"user_email" => $user_email
		);

		if ($user_id)
			$by[] = "user_id != {$user_id}";

		return !$this->existsBy($by);
	}

	public function userEmailExists($user_email)
	{
		$by = array(
			"user_email" => $user_email
		);

		$user = $this->getOneBy($by);

		return $user["user_id"];
	}

	public function verifyRecoveryCode($user_email, $user_recovery_code)
	{
		$by = array(
			'user_email' => $user_email,
			'user_recovery_code' => $user_recovery_code
		);

		$user = $this->getOneBy($by);

		return $user;
	}

	public function isMeOrMySubordinate($user_id)
	{
		if ($user_id == $this->user["user_id"])
			return true;
		else
			return $this->user["user_id"] ? $this->isMySubordinate($user_id) : false;
	}

	public function isMySubordinate($user_id)
	{
		$me = $this->getMe();

		$subordinate = $this->getById($user_id);

		return ($me["role"] != $subordinate["role"]) && in_array($subordinate["role"], array_keys($this->getRoleOptions()));
	}

	protected function insert($user)
	{
		if (isset($user["user_password"])) {
			$user["user_password"] = Passwords::hash($user["user_password"]);
		}

		$user = $this->repository->users->insert($user);

		return $user;
	}

	protected function updateBy($by, $user)
	{
		$user = $this->repository->users->updateBy($by, $user);

		return $user;
	}

	public function save($user)
	{
		if ($user["user_id"]) {
			if ($this->isMeOrMySubordinate($user["user_id"])) {
				$by = array(
					"user_id" => $user["user_id"]
				);

				$user = $this->updateBy($by, $user);
			} else {
				return false;
			}
		} else {
			//TODO - proč je to tady?
			if (isset($user["user_email"])) {

				$user_password = $user["user_password"] = isset($user["user_password"]) ? $user["user_password"] : \Nette\Utils\Strings::random(6);

				$user["client_id"] = $this->client['id'];

				$user = $this->insert($user);

				$user["user_password"] = $user_password;

				$this->sendSignupEmail($user);

				$this->notificationsService->create()
					->setUrl('/admin/user/change-password/')
					->setLocale($user['locale'])
					->setKey('changePassword')
					->setTemplate('changePassword', NULL)
					->setUserId($user['user_id'])
					->setPersist()
					->save();
			} else {
				$user["client_id"] = $this->client['id'];

				$user = $this->insert($user);
			}
		}

		$this->callback("save", $user);

		return $user;
	}

	public function resetPassword($user_recovery_code, $user_password)
	{
		$values = array(
			'user_password' => Passwords::hash($user_password),
			'user_recovery_code' => null,
		);

		$by = array(
			'user_recovery_code' => $user_recovery_code
		);

		return $this->updateBy($by, $values);
	}

	public function changePassword($user_id, $user_password)
	{
		$values = array(
			'user_password' => Passwords::hash($user_password),
		);

		$by = array(
			'user_id' => $user_id
		);

		$result = $this->updateBy($by, $values);

		return $result;
	}

	public function createRecoveryCode($user_id)
	{
		$values = array(
			"user_recovery_code" => Nette\Utils\Strings::random(30, '0-9A-Za-z')
		);

		$by = array(
			'user_id' => $user_id
		);

		if ($this->updateBy($by, $values)) {
			$this->sendRecoveryCodeEmail($user_id);
		}
	}

	public function updateLastLogin($user_id, $ip)
	{
		$user = array(
			'user_lastloged' => now(),
			'user_lastloged_ip' => $ip
		);

		$by = array(
			"user_id" => $user_id
		);

		return $this->updateBy($by, $user);
	}

	public function remove($user_id)
	{
		if ($this->isMeOrMySubordinate($user_id) && $user = $this->getOneById($user_id)) {
			$this->callback("remove", $user);

			$this->repository->users->remove($user);

			return true;
		}

		return false;
	}

	public function restore($user_id)
	{
		if ($this->repository->users->exists($user_id, true)) {
			$by = array(
				"user_id" => $user_id
			);

			$values = array(
				"removed" => null
			);

			$this->updateBy($by, $values);

			$user = $this->getOneBy($by);

			$this->callback("restore", $user);

			return true;
		}

		return false;
	}

	protected function getSubRoles($role, $roles)
	{

		$subRoles[$role] = $roles[$role]['role_title'];

		foreach ($roles as $key => $value) {
			if ($value['parent_role'] == $role) {
				$subRoles[$value['role']] = $value['role_title'];
				$subRoles = array_replace($subRoles, $this->getSubRoles($value['role'], $roles));
			}
		}

		return $subRoles;
	}

	protected function sendSignupEmail($user)
	{

		$this->emailsService->message()
			->setLocale($user["locale"])
			->setTo($user["user_email"])
			->setFrom($this->client['client_email'], $this->client['client_title'])
			->setSubjectTemplate("usersSignupSubject", $user)
			->setMessageTemplate("usersSignupMessage", $user)
			->save();
	}

	protected function sendRecoveryCodeEmail($user_id)
	{
		$user = $this->getById($user_id);

		$email = $this->emailsService->message()
			->setLocale($user["locale"])
			->setFrom($this->client['client_email'], $this->client['client_title'])
			->setTo($user["user_email"])
			->setSubjectTemplate("usersResetPasswordSubject", $user)
			->setMessageTemplate("usersResetPasswordMessage", $user)
			->save();
	}

	protected function sendNewUserEmail($user)
	{
		$this->emailsService->message()
			->setLocale($user["locale"])
			->setTo($user["user_email"], $user["user_title"])
			->setSubjectTemplate("usersNewSubject", $user)
			->setMessageTemplate("usersNewMessage", $user)
			->save();
	}

	public function login($email, $password, $via, $presenter, $params = NULL)
	{
		switch ($via) {
			case self::AUTH_CUSTOM:
				$presenter->user->setAuthenticator(new CustomAuthenticator($this, $namespace));
				try {
					$presenter->user->login($email, $password);
				} catch (Nette\Security\AuthenticationException $e) {
					return $e->getMessage();
				}
				break;
			case self::AUTH_FACEBOOK:
				$presenter->user->setAuthenticator(new FacebookAuthenticator($this));
				try {
					$presenter->user->login($email, $params);
				} catch (Nette\Security\AuthenticationException $e) {
					return $e->getMessage();
				}
				break;
			case self::AUTH_GOOGLE:
				$presenter->user->setAuthenticator(new GoogleAuthenticator($this));
				try {
					$presenter->user->login($email, $params);
				} catch (Nette\Security\AuthenticationException $e) {
					return $e->getMessage();
				}
				break;
			case self::AUTH_TWITTER:
				$presenter->user->setAuthenticator(new TwitterAuthenticator($this));
				try {
					$presenter->user->login($email, $params);
				} catch (Nette\Security\AuthenticationException $e) {
					return $e->getMessage();
				}
				break;
		}

		$userIdentity = $presenter->user->getIdentity();

		return $userIdentity;
	}

	//TODO - zkontrolovat
	public function addUserBySocialNetwork($socialNetwork, $user)
	{
		$userInfo['user_id'] = NULL;

		$userInfo['role'] = 'user';

		$userInfo['user_title'] = $user['name'];

		switch ($socialNetwork) {
			case self::AUTH_FACEBOOK:

				$userInfo['facebook_id'] = $user['id'];
				$userInfo['user_email'] = $user['email'];
				$userInfo['locale'] = substr($user['locale'], 0, 2);

				break;
			case self::AUTH_GOOGLE:
				$userInfo['google_id'] = $user['id'];
				$userInfo['user_email'] = $user['email'];
				$userInfo['locale'] = substr($user['locale'], 0, 2);

				break;
			case self::AUTH_TWITTER:
				$userInfo['twitter_id'] = $user['id'];
				$userInfo['locale'] = $user['lang'];

				break;
		}

		return $this->save($userInfo);
	}

	public function connectWithSocialNetwork($socialNetwork, $user)
	{
		switch ($socialNetwork) {
			case self::AUTH_FACEBOOK:

				$values = array('facebook_id' => $user['id']);

				break;
			case self::AUTH_GOOGLE:

				$values = array('google_id' => $user['id']);

				break;
			case self::AUTH_TWITTER:

				$values = array('twitter_id' => $user['id']);

				break;
			default:

				$values = false;
		}

		if ($values) {
			$by = array(
				'user_email' => $user['email']
			);

			return $this->updateBy($by, $values);
		}

		return false;
	}

	public function getIdByRecoveryCode($user_recovery_code)
	{
		$by = array(
			"user_recovery_code" => $user_recovery_code
		);

		$user = $this->repository->users->getOneBy($by);

		if ($user)
			return $user["user_id"];
		else
			return false;
	}
}