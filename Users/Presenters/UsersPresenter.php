<?php

namespace CP\Presenters;

use Nette\Application\BadRequestException;
use CP\Users\PasswordForm;

trait UsersPresenter
{
	public $profilesGrid;

	public function actionDefault()
	{
		switch ($this->user->getIdentity()->data["role"]) {
			case "admin":
			case "superAdmin":
				$this->profilesGrid = $this->usersService->getGrid();

				$this->profilesGrid->paginator->itemsPerPage = 20;

				$this->profilesGrid->create($this);
				break;
		}
	}

	public function renderDefault()
	{
		if ($this->profilesGrid) {
			$this->template->profilesGrid = $this->profilesGrid;
		} else
			throw new BadRequestException;
	}
	public $profile;

	public function actionAdd($role = NULL)
	{
		$profile = array();

		if ($role)
			$profile["role"] = $role;

		$this->profile = $this->usersService->getNew($profile);
	}

	public function renderAdd()
	{
		$this->template->profile = $this->profile;
	}

	public function actionEdit($user_id)
	{
		if ($this->usersService->isMeOrMySubordinate($user_id) && $this->profile = $this->usersService->getOneById($user_id)) {
			$this->template->profile = $this->profile;
		} else
			throw new BadRequestException;
	}

	protected function createComponentUserForm()
	{
		$formClass = "\\App\\Forms\\Users\\" . ucfirst($this->profile["role"]) . "Form";

		$form = new $formClass($this);

		$form->setValues($this->profile);

		return $form->create();
	}

	public function handleRemove($user_id)
	{
		if ($this->usersService->isMeOrMySubordinate($user_id) && $this->profile = $this->usersService->getOneById($user_id)) {
			if ($this->usersService->remove($this->profile["user_id"])) {
				if ($this->profile["role"] == "broker")
					$this->removeFromEstatesServers($this->profile["user_id"]);

				if ($this->profile["user_id"] == $this->usersService->user["user_id"]) {
					$this->flashMessage('Váš uživatelský účet byl odebrán', 'alert-success');

					$this->getUser()->logout(true);

					$this->redirect('Homepage:');
				} else {
					$this->flashMessage("Uživatelský účet byl odebrán", 'alert-success');

					$this->restoreRequest($this->backlink);
					$this->redirect('Users:default');
				}
			} else {
				$this->flashMessage("Uživatelský účet se nepodařilo odebrat!", 'alert-danger');

				$this->restoreRequest($this->backlink);
				$this->redirect('this');
			}
		} else
			throw new BadRequestException;
	}
	protected $user_id;

	public function actionChangePassword($user_id)
	{
		if ($user_id)
			$this->user_id = $user_id;
		else
			throw new BadRequestException;
	}

	protected function createComponentChangePasswordForm()
	{
		$form = new PasswordForm($this);

		return $form->createSuperAdminChange($this->user_id);
	}
}