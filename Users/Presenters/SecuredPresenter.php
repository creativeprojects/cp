<?php

namespace CP\Users;

use Nette,
	App,
	CP;

abstract class SecuredPresenter extends App\Presenters\BasePresenter
{
	/** @var App\Services\Users @inject */
	public $usersService;

	public function startup()
	{
		parent::startup();

		$user = $this->getUser();

		if (!$user->isLoggedIn() AND ! $user->isAllowed($this->name, $this->action)) {
			if ($user->getLogoutReason() === Nette\Http\UserStorage::INACTIVITY) {
				$this->flashMessage('Z důvodu příliš dlouhé neaktivity jste byli odhlášeni', 'alert-warning');

				$this->redirect($this->usersService->settings['loginPage'], array('backlink' => $this->storeRequest()));
			}

			$this->redirect($this->usersService->settings['loginPage']);
		} else {
			if (!$user->isAllowed($this->name, $this->action)) {
				$this->flashMessage('Nemáte dostatečné oprávnění!', 'alert-danger');

				$this->redirect($this->usersService->settings['loginPage']);
			}
		}
	}
}