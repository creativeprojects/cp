<?php

namespace CP\Users;

use Nette,
	CP,
	App;

/**
 * Presenter that is handling all oauth requests and callbacks
 *
 * @author Michal Svec <pan.svec@gmail.com>
 */
class AuthPresenter extends App\Presenters\BasePresenter
{
	/** @var \CP\Users\Service @inject */
	public $service;

	/** @var NetteOpauth\NetteOpauth */
	protected $opauth;

	/**
	 * @param NetteOpauth\NetteOpauth
	 */
	public function injectOpauth(\NetteOpauth\NetteOpauth $opauth)
	{
		$this->opauth = $opauth;
	}

	/**
	 * Redirection method to oauth provider
	 *
	 * @param string|NULL $strategy strategy used depends on selected provider - 'fake' for localhost testing
	 */
	public function actionAuth($strategy = NULL)
	{
		$this->opauth->auth($strategy);

		if ($this->getParameter('code') OR $this->getParameter('oauth_token')) {
			$this->terminate();
		}
	}

	/**
	 * @param string
	 */
	public function actionCallback($strategy)
	{
		$identity = $this->opauth->callback($strategy);

		$opauth = unserialize(base64_decode($_POST['opauth']));

		switch ($opauth['auth']['provider']) {
			case 'Facebook':
				$via = CP\Users\Service::AUTH_FACEBOOK;
				break;
			case 'Google':
				$via = CP\Users\Service::AUTH_GOOGLE;
				break;
			case 'Twitter':
				$via = CP\Users\Service::AUTH_TWITTER;
				break;
		}

		$login = $this->service->login(NULL, NULL, $via, $this, $identity);

		$this->service->updateLastLogin($this->user->getId(), $this->context->getService('httpRequest')->getRemoteAddress());

		$this->translatorSession->setLocale($login->locale);

		$this->restoreRequest($this->backlink);

		$this->redirect("Homepage:default");
	}
}