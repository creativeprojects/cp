<?php

namespace CP\Presenters;

use	CP\Users\LoginForm;
use CP\Users\PasswordForm;
use Nette\Application\BadRequestException;

trait UserPresenter
{
	public function startup()
	{
		parent::startup();

		$user = $this->getUser();

		if (!$user->isLoggedIn() AND !$user->isAllowed($this->name, $this->action)) {
			if ($user->getLogoutReason() === \Nette\Http\UserStorage::INACTIVITY) {
				$this->flashMessage('Z důvodu příliš dlouhé neaktivity jste byli odhlášeni', 'alert-warning');

				$this->redirect("User:login", array('backlink' => $this->storeRequest()));
			}

			$this->redirect("User:login");
		} else {
			if (!$user->isAllowed($this->name, $this->action)) {
				$this->flashMessage('Nemáte dostatečné oprávnění!', 'alert-danger');

				$this->redirect("User:login");
			}
		}
	}

	public $profile;

	public function actionMyProfile()
	{
		if($this->profile = $this->usersService->getMeLocale()){
			$this->template->profile = $this->profile;
		} else
			throw new BadRequestException;
	}

	public function actionSignUp()
	{
		$this->profile = $this->usersService->getNew();

		$this->template->profile = $this->profile;
	}

	public function actionEdit()
	{
		if($this->profile = $this->usersService->getMe()){
			$this->template->profile = $this->profile;
		} else
			throw new BadRequestException;
	}

	protected function createComponentUserForm()
	{
		$formClass = "App\\Forms\\Users\\" . ucfirst($this->profile["role"]) . "Form";

		$form = new $formClass($this);

		$form->setValues($this->profile);

		return $form->create();
	}

	public function handleRemove()
	{
		if ($this->profile = $this->usersService->getMe()) {
			if($this->usersService->remove($this->profile["user_id"])) {
				$this->flashMessage('Váš uživatelský účet byl odebrán', 'alert-success');

				$this->actionLogOut();
			} else {
				$this->flashMessage('Při odebírání Vašeho uživatelského účtu došlo k chybě!', 'alert-danger');
			}
		} else 
			throw new BadRequestException;
	}

	public function actionChangePassword()
	{
		
	}
	
	protected function createComponentChangePasswordForm()
	{
		$form = new PasswordForm($this);

		return $form->createChange();
	}

	public function actionResetPassword($user_email, $user_recovery_code)
	{
		if ($profile = $this->usersService->verifyRecoveryCode($user_email, $user_recovery_code)) {
			$this->profile = $profile;
		} else
			throw new BadRequestException;
	}

	protected function createComponentForgottenPasswordForm()
	{
		$form = new PasswordForm($this);

		$form->setValues($_GET);

		return $form->createForgotten();
	}

	protected function createComponentResetPasswordForm()
	{
		$form = new PasswordForm($this);

		$form->setValues($this->profile);

		return $form->createReset();
	}

	protected function createComponentLoginForm()
	{
		$form = new LoginForm($this);

		return $form->create();
	}

	public function actionLogOut()
	{
		$this->getUser()->logout(true);

		$this->flashMessage('Byl jste odhlášen', 'alert-success');

		$this->restoreRequest($this->backlink);
		$this->redirect('Homepage:');
	}
}