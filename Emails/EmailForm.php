<?php

namespace CP\Forms;

use CP\Forms\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

class EmailForm extends \CP\Forms\Base
{
	protected $properties = array(
		"email_from_name" => array(
			"editable" => false,
			"visible" => false,
			"value" => null,
			"required" => "Uveďte jméno odesílatele",
			"label" => "Jméno odesílatele",
			"placeholder" => "Zadejte jméno odesílatele"
		),
		"email_from_email" => array(
			"editable" => false,
			"visible" => false,
			"value" => null,
			"required" => "Uveďte email odesílatele",
			"label" => "Email odesílatele",
			"placeholder" => "Zadejte email odesílatele"
		),
		"email_to" => array(
			"editable" => false,
			"visible" => false,
			"required" => false,
			"value" => array(),
			"label" => "Přijemce"
		),
		"email_cc" => array(
			"editable" => false,
			"visible" => false,
			"value" => array(),
			"required" => false,
			"label" => "Kopie"
		),
		"email_bcc" => array(
			"editable" => false,
			"visible" => false,
			"required" => false,
			"value" => array(),
			"label" => "Skrytá kopie"
		),
		"email_subject" => array(
			"editable" => false,
			"visible" => false,
			"value" => null,
			"required" => false,
			"label" => "Předmět",
			"placeholder" => "Uveďte předmět zprávy"
		),
		"email_message" => array(
			"editable" => false,
			"visible" => false,
			"value" => null,
			"label" => "Zpráva",
			"required" => "Uveďte text zprávu",
			"html" => false,
			"placeholder" => "Uveďte text zprávy"
		),
		"email_attachments" => array(
			"editable" => false,
			"visible" => false,
			"required" => false,
			"value" => array(),
			"label" => "Přílohy"
		),
		"send" => array(
			"class" => "btn btn-primary btn-lg",
			"icon" => "fa-envelope-o",
			"label" => "Odeslat zprávu"
		),
		"flashMessageSuccess" => "Email byl odeslán",
		"flashMessageError" => "Při odesílání email došlo k chybě!",
		"sentCallback" => null,
		"sentCallbackArguments" => null,
		"parameters" => array()
	);

	public function setSentCallback($callback, $arguments = null)
	{
		$this->properties["sentCallback"] = $callback instanceof \Nette\Callback ? $callback : callback($callback);

		$this->properties["sentCallbackArguments"] = $arguments;

		return $this;
	}

	public function setFlashMessage($success, $error)
	{
		$this->properties["flashMessageSuccess"] = $success;

		$this->properties["flashMessageError"] = $error;

		return $this;
	}

	public function setRequired($field, $value)
	{
		$this->properties[$field]["required"] = $value;

		return $this;
	}

	public function setHtmlMessage($value)
	{
		$this->properties["email_message"]["html"] = $value;

		return $this;
	}

	public function setLabel($field, $value)
	{
		$this->properties[$field]["label"] = $value;

		return $this;
	}

	public function setEditable($field, $value)
	{
		$this->properties[$field]["editable"] = $value;

		return $this;
	}

	public function setVisible($field, $value)
	{
		$this->properties[$field]["visible"] = $value;

		return $this;
	}

	public function setValue($field, $value)
	{
		$this->values[$field] = $value;

		return $this;
	}

	private function setRecipient($field, $email, $name = null)
	{
		$this->values[$field] = array();

		$this->addFormRecipient($field, $email, $name);
	}

	private function addRecipient($field, $email, $name = null)
	{
		$this->values[$field][] = array(
			"name" => $name,
			"email" => $email
		);
	}

	public function setTo($email, $name = null)
	{
		$this->setRecipient("email_to", $email, $name);

		return $this;
	}

	public function addTo($email, $name = null)
	{
		$this->addRecipient("email_to", $email, $name);

		return $this;
	}

	public function setCc($email, $name = null)
	{
		$this->setRecipient("email_cc", $email, $name);

		return $this;
	}

	public function addCc($email, $name = null)
	{
		$this->addRecipient("email_cc", $email, $name);

		return $this;
	}

	public function setBcc($email, $name = null)
	{
		$this->setRecipient("email_bcc", $email, $name);

		return $this;
	}

	public function addBcc($email, $name = null)
	{
		$this->addRecipient("email_bcc", $email, $name);

		return $this;
	}

	public function setFrom($email, $name = null)
	{
		$this->setValue("email_from_email", $email);

		$this->setValue("email_from_name", $name);

		return $this;
	}

	public function setSubject($subject)
	{
		$this->setValue("email_subject", $subject);

		return $this;
	}

	public function setMessage($message)
	{
		$this->setValue("email_message", $message);

		return $this;
	}

	protected $templatesDir;

	public function setTemplatesDir($templatesDir)
	{
		$this->templatesDir = $templatesDir;

		return $this;
	}

	public function setSubjectTemplate($template, $parameters = array())
	{
		$template = $this->presenter->emailsService->template->create()
			//->setDir($this->templatesDir)
			//->setLocale($this->presenter->translator->getLocale())
			->setFile($template)
			->setParameters($parameters)
			->process();

		$this->setValue('email_subject', $template);

		return $this;
	}

	public function setMessageTemplate($template, $parameters = array())
	{
		$template = $this->presenter->emailsService->template->create()
			//->setDir($this->templatesDir)
			//->setLocale($this->presenter->translator->getLocale())
			->setFile($template)
			->setParameters($parameters)
			->process();
		
		$this->setValue('email_message', $template);

		return $this;
	}

	public function setParameters($parameters)
	{
		$this->properties['parameters'] = $parameters;

		return $this;
	}

	public function setParameter($parameter, $value)
	{
		$this->properties['parameters'][$parameter] = $value;

		return $this;
	}

	public function addAttachment($attachment)
	{
		$this->values["email_attachments"][] = array(
			"file_src" => $attachment
		);

		return $this;
	}

	public function create()
	{
		$this->form->properties = $this->properties;

		$this->form->addHidden('user_id');

		if ($this->properties["email_from_name"]["editable"]) {
			$this->form->addText('email_from_name')
				->setRequired($this->properties["email_from_name"]["required"]);
		} else {
			$this->form->addHidden('email_from_name');
		}

		if ($this->properties["email_from_name"]["editable"]) {
			$this->form->addText('email_from_email')
				->setRequired($this->properties["email_from_email"]["required"]);
		} else {
			$this->form->addHidden('email_from_email');
		}
		
		$this->addRecipients("email_to", "Vyplňte email příjemce");

		$this->addRecipients("email_cc", "Vyplňte email příjemce");

		$this->addRecipients("email_bcc", "Vyplňte email příjemce");

		if ($this->properties["email_subject"]["editable"]) {
			$this->form->addText('email_subject')
				->setRequired($this->properties["email_subject"]["required"]);
		} else {
			$this->form->addHidden('email_subject');
		}

		if ($this->properties["email_message"]["editable"]) {
			$this->form->addTextArea('email_message')
				->setRequired($this->properties["email_message"]["required"]);
		} else {
			$this->form->addHidden('email_message');
		}

		if ($this->properties["email_attachments"]["editable"] || $this->properties["email_attachments"]["visible"]) {
			$this->form->addAttachments($this->presenter, "email_attachments");
		}

		$this->form->addSubmit('send')
			->onClick[] = array($this, "send");

		$this->form->setValues($this->values);

		return $this->form;
	}

	private function addRecipients($fieldName, $errorEmail)
	{
		$parent = $this;

		$container = $this->form->addDynamic($fieldName, function (Container $container) use ($parent, $fieldName, $errorEmail) {
			if ($parent->properties[$fieldName]["editable"]) {
				
				$container->addText('email')
					->setRequired($errorEmail);
						
				$container->addText('name');

				$container->addSubmit("remove")
					->setValidationScope(false)
					->onClick[] = callback($parent, "removeRecipientItem");

			} else {
				$container->addHidden("email");

				$container->addHidden("name");
			}
		});

		$container->addSubmit('add')
			->setValidationScope(false)
			->onClick[] = callback($this, "addRecipientItem");
	}

	public function addRecipientItem($button)
	{
		$recipient = array(
			"name" => "",
			"email" => ""
		);

		$button->parent->createOne()
			->setValues($recipient);
	}

	public function removeRecipientItem($button)
	{
		$recipient = $button->parent->parent;

		$recipient->remove($button->parent, TRUE);

		$this->presenter->flashMessage("Příjemce byl odebrán", "alert-success");
	}

	public function send($button)
	{
		// TODO - hlídání maximální velikosti příloh
		
		$values = $button->parent->getValues();

		$values = $this->sanitizeValues($values);

		$result = $this->presenter->emailsService->message($values)->save();

		if ($result) {
			$this->presenter->flashMessage($this->properties["flashMessageSuccess"], 'alert-success');

			if($this->properties["sentCallback"]){
				//TODO - přidat i argumenty formuláře
				$this->properties["sentCallback"]->invokeArgs(array($this->properties["sentCallbackArguments"]));
			} elseif ($this->presenter->backlink) {
				$this->presenter->restoreRequest($this->presenter->backlink);
			} else
				$this->presenter->redirect("this");


		} else {
			$this->presenter->flashMessage($this->properties["flashMessageError"], 'alert-danger');
		}
	}

	public function sanitizeValues($values)
	{
		$values = (array) $values;

		if(isset($values["email_attachments"])){
			unset($values["email_attachments"]["files"]);

			foreach($values["email_attachments"] AS & $attachment){
				$attachment = $attachment["file_src"];
			}
		}

		if($this->postProcessSubjectTemplate){
			$parameters = array_merge($this->postProcessSubjectParameters, $values);

			$values["email_subject"] = $this->presenter->emailsService->template->create()
				->setFile($this->postProcessSubjectTemplate)
				->setParameters($parameters)
				->process();
		}

		if($this->postProcessMessageTemplate){
			$parameters = array_merge($this->postProcessMessageParameters, $values);

			$values["email_message"] = $this->presenter->emailsService->template->create()
				->setFile($this->postProcessMessageTemplate)
				->setParameters($parameters)
				->process();
		}

		return $values;
	}

	protected $postProcessSubjectTemplate;
	protected $postProcessSubjectParameters = array();

	public function setPostProcessSubjectTemplate($templatePath, $parameters = array())
	{
		$this->postProcessSubjectTemplate = $templatePath;
		$this->postProcessSubjectParameters = (array) $parameters;

		return $this;
	}

	protected $postProcessMessageTemplate;
	protected $postProcessMessageParameters = array();

	public function setPostProcessMessageTemplate($templatePath, $parameters = array())
	{
		$this->postProcessMessageTemplate = $templatePath;
		$this->postProcessMessageParameters = (array) $parameters;

		return $this;
	}
}