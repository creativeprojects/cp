<?php

namespace CP\Emails;

use Nette,
	CP;

class Message
{
	protected $settings;

	protected $locale;

	private $service;

	public function __construct($service, $settings)
	{
		$this->service = $service;

		$this->settings = $settings;

		$this->load($this->settings["message"]);
	}

	public function load($message, $truncate = false)
	{
		if (isset($message["email_id"]))
			$this->setId($message["email_id"]);

		if (isset($message["user_id"]))
			$this->setUserId($message["user_id"]);

		if (isset($message["email_from_email"]))
			$this->setFrom($message["email_from_email"], @$message["email_from_name"]);

		if (isset($message["email_to"])) {
			$this->formatRecipients($message["email_to"]);

			if ($truncate)
				$this->truncateRecipients($this->email_to);

			foreach ($message["email_to"] AS $recipient) {
				$this->addTo($recipient["email"], $recipient["name"]);
			}
		}

		if (isset($message["email_cc"])) {
			$this->formatRecipients($message["email_cc"]);

			if ($truncate)
				$this->truncateRecipients($this->email_cc);

			foreach ($message["email_cc"] AS $recipient) {

				$this->addCc($recipient["email"], $recipient["name"]);
			}
		}

		if (isset($message["email_bcc"])) {
			$this->formatRecipients($message["email_bcc"]);

			if ($truncate)
				$this->truncateRecipients($this->email_bcc);

			foreach ($message["email_bcc"] AS $recipient) {
				$this->addBcc($recipient["email"], $recipient["name"]);
			}
		}

		if (isset($message["email_subject"]))
			$this->setSubject($message["email_subject"]);

		if (isset($message["email_message"]))
			$this->setMessage($message["email_message"]);

		if (isset($message["email_attachments"])) {
			if ($truncate)
				$this->truncateAttachments();

			foreach ($message["email_attachments"] AS $attachment) {
				$this->addAttachment($attachment);
			}
		}
	}

	public function setLocale($locale)
	{
		$this->locale = $locale;

		return $this;
	}

	protected function formatRecipients(& $recipients)
	{
		if (is_string($recipients)) {
			$recipients = array(
				"name" => null,
				"email" => $recipients
			);
		}

		if (!is_numeric(key($recipients)))
			$recipients = array($recipients);

		$recipients = (array)$recipients;

		foreach ($recipients AS & $recipient) {
			$recipient = (array)$recipient;
		}

		$recipients = array_filter($recipients);
	}

	public function setId($email_id)
	{
		$this->email_id = $email_id;
	}

	public function setUserId($user_id)
	{
		$this->user_id = $user_id;

		return $this;
	}

	public function setFrom($email_from_email, $email_from_name = null)
	{
		$this->email_from_email = $email_from_email;

		if (isset($email_from_name))
			$this->email_from_name = $email_from_name;

		return $this;
	}

	public function setTo($email, $name = null)
	{
		$this->setRecipient($this->email_to, $email, $name);

		return $this;
	}

	public function addTo($email, $name = null)
	{
		$this->addRecipient($this->email_to, $email, $name);

		return $this;
	}

	public function setCc($email, $name = null)
	{
		$this->setRecipient($this->email_cc, $email, $name);

		return $this;
	}

	public function addCc($email, $name = null)
	{
		$this->addRecipient($this->email_cc, $email, $name);

		return $this;
	}

	public function setBcc($email, $name = null)
	{
		$this->setRecipient($this->email_bcc, $email, $name);

		return $this;
	}

	public function addBcc($email, $name = null)
	{
		$this->addRecipient($this->email_bcc, $email, $name);

		return $this;
	}

	public function setSubject($email_subject)
	{
		$this->email_subject = $email_subject;

		return $this;
	}

	public function setMessage($email_message)
	{
		$this->email_message = $email_message;

		return $this;
	}

	/*protected $templatesDir;

	public function setTemplatesDir($templatesDir)
	{
		$this->templatesDir = $templatesDir;

		return $this;
	}*/

	public function setSubjectTemplate($templatePath, $values)
	{
		$this->setSubject($this->getTemplate($templatePath, $values));

		return $this;
	}

	public function setMessageTemplate($templatePath, $values)
	{
		$this->setMessage($this->getTemplate($templatePath, $values));

		return $this;
	}

	function addRecipient(& $property, $email, $name = null)
	{
		$property = (array)$property;

		$email = trim($email);

		$name = trim($name);

		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$recipient = array(
				"email" => $email,
				"name" => $name
			);
			array_push($property, $recipient);
		}
	}

	//Truncate recipients property, then add
	function setRecipient(& $property, $email, $name = null)
	{
		$this->truncateRecipients($property);

		$this->addRecipient($property, $email, $name);
	}

	function truncateRecipients(& $property)
	{
		$property = array();
	}

	public $email_attachments = array();

	public function addAttachment($attachment)
	{
		$this->email_attachments[] = $attachment;

		return $this;
	}

	private function truncateAttachments()
	{
		$this->email_attachments = array();

		return $this;
	}

	public function getTemplate($templatePath, $parameters)
	{
		$parameters['domain'] = "http://" . $this->service->client['domain'];

		$template = $this->service->template
			->create()
			->setParameters($parameters);

		if(strpos($templatePath, "/") === false){
			$template
				->setDir($this->settings["templatesDir"])
				->setLocale($this->locale);
		}

		$template->setFile($templatePath);

		return $template->process();
	}

	public function save()
	{
		return $this->service->save($this);
	}
}