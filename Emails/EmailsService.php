<?php

namespace CP\Emails;

use Nette,
	CP;

class Service extends CP\Service
{
	protected $form;

	protected $presenter;

	public $settings = array(
		"smtp" => array(
			'host' => 'jaguar.blueboard.cz',
			'username' => 'noreply@creativeprojects',
			'password' => 'cprcreativeprojects',
			'secure' => 'ssl',
		),
		"message" => array(
			"email_from_name" => "Creative Projects s.r.o.",
			"email_from_email" => "noreply@creativeprojects.cz",
			"email_to" => "noreply@creativeprojects.cz",
			"email_subject" => "Email message from Your website",
			"email_message" => "No message!"
		),
		"volume" => 2,
		"templatesDir" => "/templates/Emails/Messages",
		"attachmentsDir" => "/emails/attachments",
		"maxAttachmentsSize" => 15728640
	);

	public function message($load = array())
	{
		$message = new Message($this, $this->settings);

		$message->setLocale($this->translator->getLocale());

		$message->load($load, TRUE);

		return $message;
	}
	
	/*	 * **************************************************************************************************************************** */
	/* 													Send																	   */
	/*	 * **************************************************************************************************************************** */

	public function send($message)
	{
		$message = (object) $message;

		$mail = new Nette\Mail\Message;

		$mail->setFrom($message->email_from_name . ' <' . $message->email_from_email . '>');

		$mail->setSubject($message->email_subject);

		$HTMLBody = $this->template
			->create()
			->setDir($this->settings["templatesDir"])
			->setParameters($message)
			->setFile("@layout")
			->process();

		$mail->setHTMLBody($HTMLBody);

		if(isset($message->email_to)){
			foreach ($message->email_to AS $email_to)
				$mail->addTo($email_to["email"]);
		}

		if(isset($message->email_cc)){
			foreach ($message->email_cc AS $email_cc)
				$mail->addCc($email_cc["email"]);
		}

		if(isset($message->email_bcc)){
			foreach ($message->email_bcc AS $email_bcc)
				$mail->addBcc($email_bcc["email"]);
		}

		if(isset($message->email_attachments)){
			$attachmentsSize = 0;

			foreach ($message->email_attachments AS $attachment) {
				
				if($attachment) {

					$attachment = WWW_DIR . "/" . $attachment;

					if ($attachmentsSize <= $this->settings["maxAttachmentsSize"] && file_exists($attachment)) {
						$attachmentsSize += filesize($attachment);

						$mail->addAttachment($attachment);
					}
				}
			}
		}

		$mailer = new Nette\Mail\SendmailMailer;
		
		//$mailer = new Nette\Mail\SmtpMailer($this->settings["smtp"]);

		$mailer->send($mail);

		$this->markAsSent($message->email_id);

		return $this;
	}

	public function sendVolume()
	{
		$log = array();

		$messages = $this->getVolume();

		foreach ($messages as $message) {
			$log[] = "Odesílání emailu <strong>{$message["email_subject"]}</strong> pro <strong>" . print_r($message["email_to"], true) . "</strong>";
			$this->send($message);
		}

		if (!count($log))
			$log[] = "Všechny emaily jsou odeslané";

		return $log;
	}

	public function getVolume()
	{
		$by = array(
			"where" => array(
				"email_sent" => NULL
			),
			"order" => array(
				"created" =>  "ASC"
			),
			"limit" => $this->settings["volume"]
		);
		
		//$limit = $this->settings["volume"];

		//$messages = $this->database->table($this->repository->emails->getName())->select("*")->where(array("email_sent IS NULL"))->order('created ASC')->limit($limit)->fetchAll();
		
		$messages = $this->repository->emails->getAllBy($by);

		return $messages;
	}

	public function markAsSent($email_id)
	{
		$by = array(
			"email_id" => $email_id
		);

		$values = array(
			"email_sent" => now()
		);

		$this->repository->emails->updateBy($by, $values);

		return $this;
	}
	/*	 * **************************************************************************************************************************** */
	/* 													Add, Remove																   */
	/*	 * **************************************************************************************************************************** */

	public function removeById($email_id)
	{
		return $this->repository->emails->removeOne($email_id);
	}

	public function save($message)
	{
		$this->repository->emails->save((array)$message);

		return $this;
	}
	/*	 * **************************************************************************************************************************** */
	/* 													Outbox																	   */
	/*	 * **************************************************************************************************************************** */

	public function getMySent()
	{
		$emails = new \CP\Grid\Grid($this->database);

		$emails->query = array(
			"fields" => array(
				"*",
			),
			"from" => "{$this->repository->emails->getName()} as e",
			"where" => array(
				$this->repository->emails->sqlNotRemoved(),
				"user_id = {$this->user["user_id"]}"
			)
		);

		$emails->itemsCallback = callback($this->repository->emails, "sanitizeOutput");

		return $emails;
	}
}