<?php

namespace Repository;

class emails extends \Repository\BaseRepository
{
	public $name = "emails";

	public $fields = array(
		"email_id" => "primary",
		"user_id" => "int",
		"email_from_name" => "varchar",
		"email_from_email" => "varchar",
		"email_to" => "json",
		"email_cc" => "json",
		"email_bcc" => "json",
		"email_subject" => "varchar",
		"email_message" => "html",
		"email_attachments" => "json",
		"email_sent" => "timestamp",
		"email_priority" => "tinyint",
	);

	protected $validateTable = true;
}