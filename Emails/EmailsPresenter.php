<?php

namespace CP\Presenters;

trait EmailsPresenter
{
	public function actionSend() {
		$log = $this->emailsService->sendVolume();

		$this->template->log = $log;
	}
}
