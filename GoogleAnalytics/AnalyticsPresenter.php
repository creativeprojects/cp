<?php

namespace CP\Presenters;

use Nette;

trait AnalyticsPresenter
{
	/** @var \CP\Services\Analytics @inject */
	public $analyticsService;

	public function actionDefault()
	{
		$this->template->analyticsData = $this->analyticsService->get($_GET);
	}
}