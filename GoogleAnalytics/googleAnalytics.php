<?php

namespace CP\Services;

use CP;

class Analytics extends CP\Base\Service
{
	public $client;

	public function serviceSettings($settings, $key = null)
	{
		parent::serviceSettings($settings);
	}

	public function get($get)
	{
		if(isset($this->settings["profileId"])){
			$this->updateSettings($get);

			require_once(CP_DIR . '/Utils/google-api-php-client/src/Google/autoload.php');

			$client_id = $this->settings["client_id"];
			$service_account_name = $this->settings["service_account_name"];
			$key_file_name = $this->settings["key_file_name"];
			$key_file_location = '../app/keys/' . $key_file_name;

			$client = new \Google_Client();
	//		$client->setApplicationName("EstateApp");
			$client->setClassConfig('Google_Cache_File', 'directory', TEMP_DIR . "/cache");

		 	$service = new \Google_Service_Analytics($client);

			if (isset($_SESSION['service_token'])) {
			  $client->setAccessToken($_SESSION['service_token']);
			}
			$key = file_get_contents($key_file_location);
			$cred = new \Google_Auth_AssertionCredentials(
			    $service_account_name,
			    array('https://www.googleapis.com/auth/analytics'),
			    $key
			);
			$client->setAssertionCredentials($cred);
			if ($client->getAuth()->isAccessTokenExpired()) {
			  $client->getAuth()->refreshTokenWithAssertion($cred);
			}
			$_SESSION['service_token'] = $client->getAccessToken();

			$results = $this->getResults($service);

			return $this->formatResults($results);
		} else {
			return;
		}
	}

	private function updateSettings($get)
	{
		foreach ($get as $key => $value) {
			$this->settings[$key] = $value;
		}
	}

	private function formatResults($results, $metric = 1)
	{
		$total = $results["totalsForAllResults"];

		$results = $results->getRows();
		foreach ($results as $key => $result) {
			if ($this->settings["dimension"] == "ga:date") {
				$formatedResults[$key][0] = date("j.n.Y", strtotime($result[0]));
			} elseif ($this->settings["dimension"] == "ga:hour") {
				$formatedResults[$key][0] = $result[0] . ":00";
			}
			$formatedResults[$key][1] = (int)$result[1];
		}

		return array(
			"data" => json_encode($formatedResults),
			"total" => $total
		);
	}

	private function getResults(& $analytics)
	{
		return $analytics->data_ga->get(
				'ga:' . $this->settings["profileId"], 
				$this->settings["from"], 
				$this->settings["to"], 
				'ga:sessions, ga:users, ga:pageviews, ga:bounceRate', 
				array("dimensions" => $this->settings["dimension"])
			);
	}
}