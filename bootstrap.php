<?php

$development_ip = array(
	"::1",
	'127.0.0.1',
	'185.36.160.1',
	'109.238.220.87',
	'185.36.160.19',
	'185.36.160.99',
	"194.228.20.162",
	"185.36.160.106"
);

if (!defined("CP_DIR")){
	define("CP_DIR", APP_DIR . "/cp");
}

require __DIR__ . '/functions.php';

if (isset($_COOKIE["codeception"])) {
//	$databaseNeon = is_localhost() ? "nette.database.test.local.neon" : "nette.database.test.server.neon";
	$databaseNeon = "nette.database.test.local.neon";
	
	define("DEBUG_MODE", FALSE);
} else {
	$databaseNeon = is_localhost() ? "nette.database.local.neon" : "nette.database.server.neon";
	
	if (!defined("DEBUG_MODE")){
		define("DEBUG_MODE", in_array($_SERVER["REMOTE_ADDR"], $development_ip));
	}
}

require __DIR__ . '/instance.php';

@instance::setAppDomain($appDomain);

require(APP_DIR . "/localhostSettings.php");

$configurator->setDebugMode(DEBUG_MODE);

$configurator->addConfig(APP_DIR . '/config/config.neon');

$configPath = instance::getDir() . '/config.neon';

if (file_exists($configPath)) {
	$configurator->addConfig($configPath);
} elseif (instance::getName() == "www") {

} else {
	header("HTTP/1.0 404 Not Found");

	echo '404 page not found';

	die;
}

$configurator->enableDebugger(LOG_DIR);

$configurator->setTempDirectory(TEMP_DIR);

$configurator->createRobotLoader()
	->addDirectory(APP_DIR)
	->addDirectory(CP_DIR)
	->register();


$container = $configurator->createContainer();
