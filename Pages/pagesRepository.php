<?php

namespace Repository;

class pages extends \Repository\BaseRepository
{
	public $name = "pages";

	public $fields = array(
		"page_id" => "primary",
		"page_published" => "bool",
		"client_id" => "int",
		"page_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"page_slug" => array(
			"type" => "varchar",
			"locale" => true,
		),
		"page_html" => array(
			"type" => "html",
			"locale" => true
		),
		"page_template" => array(
			"type" => "varchar",
			"locale" => true
		),
	);

	public $validateTable = true;

}