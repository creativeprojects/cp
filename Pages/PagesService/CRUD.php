<?php

namespace CP\Services\Pages;

trait CRUD
{
	public function create($page = array())
	{
		$page = array_extend(
			array(
			"page_id" => NULL,
			"page_title" => $this->settings["defaults"]["page_title"],
			"page_published" => false,
			"user_id" => $this->user["user_id"],
			"client_id" => $this->client['id'],
			"blocks" => array(),
			), $page
		);

		foreach ($this->translator->getAvailableLocales() AS $locale) {
			$page["page_template"][$locale] = $this->settings["defaultTemplate"];
		}

		$page["slug"] = $this->slugsService->getDefault($this->settings["defaultSlug"], $page["page_title"]);

		$page = $this->save($page);

		return $page;
	}

	public function save(array $page)
	{
		$page = $this->repository->pages->save($page);

		$page["blocks"] = $this->saveBlocks($page["page_id"], $page["blocks"]);

		$this->saveImages($page);

		$page["slug"] = $this->saveSlug($page);

//		$this->saveStructure($page["page_id"], $page["structure"]);

		return $page;
	}

	protected function saveImages(& $page)
	{
		$albums = $this->getTemplateMacros("album", $page["page_template"]);

		foreach ($albums AS $key => $album) {
			if(isset($page[$key])){
				$page[$key] = $this->filesService->saveByOwner($page[$key], $this->repository->pages, $page["page_id"], $key);
			}
		}
	}

	public function setPublished($page_id, $page_published)
	{
		if ($page = $this->exists($page_id)) {
			return $this->repository->pages->saveField($page_id, "page_published", $page_published);
		}

		return false;
	}

	protected function saveSlug($values)
	{
		$slug = $this->slugsService->saveByOwner($this->repository->pages, $values["page_id"])
			->setAction("Pages:default")
			->setParameters(array("page_id" => (int)$values["page_id"]))
			->createFrom($values["page_title"], $this->settings["defaultSlug"])
			->save($values);

		return $slug;
	}

	protected function saveBlocks($page_id, $blocks)
	{
		$savedIds = array();

		foreach ($blocks AS $block_key => & $block) {
			$block = (array)$block;

			$block["page_id"] = $page_id;
			//$block["block_key"] = $block_key;

			foreach ($this->translator->getAvailableLocales() AS $locale) {
				$block["block_html"][$locale] = preg_replace_callback(
					'/{([^}]*)}/', function ($matches) {
					return str_replace("&gt;", ">", $matches[0]);
				}, $block["block_html"][$locale]
				);
			}

			$block = $this->repository->pagesBlocks->save($block);

			$savedIds[] = $block["block_id"];

			$this->saveBlockFiles($block);
		}

		$by = array_filter(array(
			"page_id" => $page_id,
			"block_id NOT" => $savedIds
		));

		$this->repository->pagesBlocks->removeBy($by);

		return $blocks;
	}

	public function saveBlockFiles($block)
	{
		return $this->checkBlockFiles($block, true);
	}
}