<?php

namespace CP\Pages;

use Nette,
	App,
	CP,
	CP\Pages\PageForm,
	Nette\Application\BadRequestException;

trait AdminPagesPresenter
{
	/** @var \CP\Services\Pages @inject */
	public $pagesService;

	public function startup()
	{
		parent::startup();

		$this->isUserAllowed();
	}

	public $pagesGrid;

	public function actionDefault()
	{
		switch($this->user->getIdentity()->data["role"]){
			case 'admin':
			case 'superAdmin':
				$this->pagesGrid = $this->pagesService->getByClient();

				break;
		}
	}

	public function renderDefault()
	{
		if ($this->pagesGrid) {
			$this->pagesGrid->paginator->itemsPerPage = 20;

			$this->pagesGrid->create($this);

			$this->template->pagesGrid = $this->pagesGrid;
		} else
			throw new BadRequestException;
	}
	
	public $page;

	public function handleAdd()
	{
		$page = $this->pagesService->create();

		$this->redirect('Pages:add', $page['page_id']);
	}

	public function actionAdd($page_id)
	{
		$this->actionEdit($page_id);
	}

	public function actionEdit($page_id)
	{
		$page = $this->pagesService->getById($page_id);

		if($page){
			$this->page = $page;

			$this->template->page = $page;
		} else
			throw new BadRequestException;
	}

	public function handleRemove($page_id, $redirect = "this")
	{
		$page = $this->pagesService->getById($page_id);
		
		if ($page) {
			$this->pagesService->remove($page["page_id"]);

			$this->flashMessage("Stránka byla odebrána", "alert-success");

			$this->restoreRequest($this->backlink);
			$this->redirect($redirect);
		}
	}

	public function handleSetPublished($page_id, $page_published)
	{
		$this->pagesService->setPublished($page_id, $page_published);

		$this->flashMessage(($page_published ? "Stránka byla publikována" : "Stránka byla skryta"), "alert-success");

		$this->restoreRequest($this->backlink);
		$this->redirect("this");
	}

	protected function createComponentPageForm()
	{
		$form = new PageForm($this);

		$form->setValues($this->page);

		return $form->create();
	}
}