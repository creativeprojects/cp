<?php

namespace CP\Pages;

use Nette,
	App,
	CP,
	Nette\Application\BadRequestException;

abstract class FrontPresenter extends \App\FrontModule\Presenters\BasePresenter
{
	/** @var \CP\Services\Pages @inject */
	public $pagesService;

	public $page;

	public function startup()
	{
		parent::startup();

		$set = new Nette\Latte\Macros\MacroSet($this->template->getLatte()->getCompiler());

		$set->addMacro(
			'htmlblock', 
			'echo $presenter->macroHtmlBlock(%node.array)'
		);

		$set->addMacro(
			'album', 
			'echo $presenter->macroAlbum(%node.array)',
			';'
		);
	}

	public function actionDefault($page_id)
	{
		if ($this->page = $this->pagesService->getByIdLocale($page_id)) {

			$lattePath = "templates/{$this->translator->getLocale()}/" . ($this->page['page_template'] ? $this->page['page_template'] : 'default');

			$this->setView($lattePath);

			$this->template->page = $this->page;
		} else
			throw new BadRequestException;
	}

	public function renderDefault($page_id)
	{
		$this->template->page = $this->page;
	}

	public function macroHtmlBlock($args)
	{
		$blockPath = $this->pagesService->settings["blocksDir"] . "/" . $this->translator->getLocale() . "/" . $this->page["blocks"][$args["name"]]->block_id . ".latte";

		$html = $this->pagesService->template
			->create()
			->setParameters($this->template->getParameters())
			->setFile($blockPath)
			->process();

		return $html;
	}

	public function macroAlbum($args)
	{
		//TODO - include šablony album.latte z CP
	}
}
?>