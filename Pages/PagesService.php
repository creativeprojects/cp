<?php

namespace CP\Services;

use Nette,
	CP,
	CP\Grid\Grid,
	CP\Files\Service;

class Pages extends CP\Base\Service
{
	public $templateDirs;

	public function serviceSettings($settings, $key = null)
	{
		$this->settings = array(
			"templatesDefaultDir" => APP_DIR . "/FrontModule/templates/Pages/templates",
			"templatesDir" => TEMP_DIR . "/cache/Pages",
			"clientTemplatesDir" => "templates/Pages/templates",
			"blocksDir" => TEMP_DIR . "/cache/Pages",
			"defaultSlug" => "nova-stranka",
			"defaultTemplate" => "default",
		);
		
		parent::serviceSettings($settings);

		$this->setupTemplateDirs();
	}

	public function setupTemplateDirs()
	{

		$this->templateDirs = array(
			WWW_DIR . "/" . $this->client["dir"] . "/" . $this->settings["clientTemplatesDir"],
			$this->settings["templatesDir"],
			$this->settings["templatesDefaultDir"]
		);

		foreach ($this->templateDirs AS $key => $dir) {
			if (!is_dir($dir))
				unset($this->templateDirs[$key]);
		}
	}

	public function getByClient()
	{
		$pages = new \CP\Grid\Grid($this->database);
		$pages->query = array(
			'fields' => array(
				'r.page_title',
				'r.page_slug',
				'p.*',
			),
			'from' => "{$this->repository->pages->getName()} AS p LEFT JOIN {$this->repository->pages->getNameLocale()} AS r ON (p.page_id = r.page_id)",
			'limit' => null,
			'where' => array(
				"p.client_id = " . $this->client['id'],
				"p.removed IS NULL"
			),
		);

		return $pages;
	}
	public $gridFilterRules = array();

	public function getBy($by)
	{
		$page = $this->repository->pages->getOneBy($by);

		if ($page) {
			$blocks = $this->repository->pagesBlocks->getAllBy(array("page_id" => $page["page_id"]));

			foreach ($blocks AS $block_id => $block) {
				$page["blocks"][$block["block_key"]] = new Block($block);
			}

			$this->getImages($page);

			/* $structures = $this->selectAll("SELECT * FROM {$this->repository->pagesStructure->getName()} WHERE page_id = {$page["page_id"]}");

			  foreach ($structures AS $structure) {
			  $page["structure"][$structure["structure_id"]] = new Structure($structure);
			  } */

			$page["slug"] = $this->slugsService->getOneByOwner($this->repository->pages, $page["page_id"]);

			return $page;
		}

		return false;
	}

	public function getById($page_id)
	{
		$by = array(
			"client_id" => $this->client["id"],
			"page_id" => $page_id
		);

		return $this->getBy($by);
	}

	public function getImages(& $page)
	{
		$images = $this->filesService->getImagesByOwner($this->repository->pages, $page["page_id"]);

		foreach ($images AS $image) {
			if (!isset($page[$image->file_rel]))
				$page[$image->file_rel] = array();

			$page[$image->file_rel][$image->file_id] = $image;
		}
	}

	public function getByIdLocale($page_id)
	{
		$by = array(
			"client_id" => $this->client["id"],
			"page_id" => $page_id
		);

		return $this->getByLocale($by);
	}

	public function getByLocale($by)
	{
		$page = $this->repository->pages->getOneByLocale($by);

		if ($page) {
			$blocks = $this->repository->pagesBlocks->getAllByLocale(array('page_id' => $page["page_id"]));

			foreach ($blocks AS $block) {
				$this->checkBlockFilesLocale($block);

				$page["blocks"][$block["block_key"]] = new Block($block);
			}

			$this->getImagesLocale($page);

			/* $structures = $this->selectAll("SELECT * FROM {$this->repository->pagesStructure->getName()} WHERE page_id = {$page["page_id"]}");

			  foreach($structures AS $structure){
			  $page["structure"][$structure["structure_id"]] = new Structure($structure);
			  } */

			return $page;
		}

		return false;
	}

	public function getImagesLocale(& $page)
	{
		$images = $this->filesService->getImagesByOwnerLocale($this->repository->pages, $page["page_id"]);

		foreach ($images AS $image) {
			if (!isset($page[$image->file_rel]))
				$page[$image->file_rel] = array();

			$page[$image->file_rel][$image->file_id] = $image;
		}
	}

	public function getBySlug($page_slug)
	{

		$page = $this->repository->getOneByLocale(array('page_slug' => $page_slug));
		if ($page) {
			$blocks = $this->repository->getAllByLocale(array('page_id' => $page["page_id"]));

			foreach ($blocks AS $block) {
				$page["blocks"][$block["block_key"]] = new Block($block);
			}

			$structures = $this->selectAll("SELECT * FROM {$this->repository->pagesStructure->getName()} WHERE page_id = {$page["page_id"]}");

			$structures = $this->repository->pagesStructure->getAllBy(array('page_id' => $page["page_id"]));

			foreach ($structures AS $structure) {
				$page["structure"][$structure["structure_id"]] = new Structure($structure);
			}

			return $page;
		}

		return false;
	}

	public function getAllChilds($parent_structure_id = null)
	{
		$pages = new Grid($this->db);

		$pages->query = array(
			"fields" => join_fields(array(
				"p" => $this->repository->pages->getName(),
				"l" => $this->repository->pages->getNameLocale(),
				"s" => $this->repository->pagesStructure->getName()
				), false),
			"from" => "{$this->repository->pages->getName()} AS p LEFT JOIN {$this->repository->pages->getNameLocale()} AS l ON l.page_id = p.page_id LEFT JOIN {$this->repository->pagesStructure->getName()} AS s ON s.page_id = p.page_id",
			"where" => array(
				"s.parent_structure_id " . ($parent_structure_id == NULL ? "IS NULL" : "= {$parent_structure_id}"),
				"p.page_published = 1",
				"p.{$this->repository->pages->sqlNotRemoved()}"
			)
		);

		return $pages;
	}

	public function getChilds($parent_structure_id)
	{
		$fields = join_fields(array(
			"p" => $this->repository->pages->getName(),
			"l" => $this->repository->pages->getNameLocale(),
			"s" => $this->repository->pagesStructure->getName()
		));

		$pages = $this->selectAll("SELECT {$fields}, (SELECT count(page_id) FROM {$this->repository->pagesStructure->getName()} WHERE parent_structure_id = s.structure_id) AS childs_count FROM {$this->repository->pages->getName()} AS p LEFT JOIN {$this->repository->pages->getNameLocale()} AS l ON l.page_id = p.page_id LEFT JOIN {$this->repository->pagesStructure->getName()} AS s ON s.page_id = p.page_id " . $this->where(array(
				"s.parent_structure_id " . ($parent_structure_id == NULL ? "IS NULL" : "= {$parent_structure_id}"),
				(in_array($this->user["role"], array("admin", "superAdmin")) ? null : "p.page_published = 1"),
				"p.{$this->repository->pages->sqlNotRemoved()}"
			))
		);

		foreach ($pages AS & $page) {
			if ($page["childs_count"])
				$page["childs"] = $this->getChilds($page["structure_id"]);
		}

		return $pages;
	}

	use CP\Services\Pages\CRUD;

	public function exists($page_id)
	{
		$by = array(
			"page_id" => $page_id,
		);

		$by["client_id"] = $this->client["id"];

		return $this->repository->pages->existsBy($by);
	}

	

	public function checkBlockFiles($block, $forced = false)
	{
		foreach ($this->translator->getAvailableLocales() AS $locale) {
			$this->checkBlockFilesLocale($block, $forced, $locale);
		}
	}

	public function checkBlockFilesLocale($block, $forced = false, $locale = null)
	{
		if(!$locale)
			$locale = $this->translator->getLocale();

		$path = $this->getBlockFilePath($block, $locale);

		$this->filesService->validateDir(dirname($path), TRUE);

		if ($forced || !is_file($path))
			file_put_contents($path, $block["block_html"]);
	}

	public function getBlockFilePath($block, $locale)
	{
		$path = "{$this->settings["blocksDir"]}/{$locale}/{$block["block_id"]}.latte";

		return $path;
	}
	/* function saveStructure($page_id, $structures)
	  {
	  $savedIds = array();

	  foreach ($structures AS $structure) {
	  $structure["page_id"] = $page_id;

	  $values = $this->repository->pagesStructure->save($structure);

	  array_push($savedIds, $values["structure_id"]);
	  }

	  $by = array(
	  "page_id" => $page_id,
	  (count($savedIds) ? "structure_id NOT IN (" . implode(", ", $savedIds) . ")" : null)
	  );

	  $this->repository->pagesStructure->removeBy($by);
	  } */

	public function remove($page_id)
	{
		$by = array(
			"page_id" => $page_id
		);

		$this->repository->pages->removeBy($by);

		//$this->repository->pagesStructure->removeBy($by);

		$this->repository->pagesBlocks->removeBy($by);

		$this->slugsService->removeByOwner($this->repository->pages, $page_id);

		return true;
	}

	public function getTemplateMacros($macro, $page_template)
	{
		$instances = array();

		$templatePath = false;

		foreach ($this->translator->getAvailableLocales() AS $locale) {
			if (!$templatePath) {
				foreach ($this->templateDirs AS $templateDir) {

					if (!$page_template[$locale] || $page_template[$locale] == $this->settings["defaultTemplate"]) {
						$_templatePath = $this->settings["templatesDefaultDir"] . "/$locale/" . $this->settings["defaultTemplate"] . ".latte";
					} else {
						$_templatePath = $templateDir . "/$locale/" . $page_template[$locale] . ".latte";
					}
					if (is_file($_templatePath)) {
						$templatePath = $_templatePath;

						break;
					}
				}
			}
		}

		if ($templatePath) {
			$latte = file_get_contents($templatePath);

			preg_match_all('/{' . $macro . '(.*?)}/', $latte, $match);

			foreach ($match[1] AS $instance) {

				//TODO - Neumoňuje te volitelné mezery před a za "=>" - dodělat
				preg_match_all('/([^=>\s]+)(=>"([^"]+)")?/', $instance, $matches, PREG_SET_ORDER);

				$instance_key = $matches[0][3];

				array_shift($matches);

				$instances[$instance_key] = array();

				foreach ($matches as $match) {
					if (count($match) == 4) {
						$instances[$instance_key][$match[1]] = $match[3];
					}
				}
			}
		} else {
			throw new \Exception("Template path for \"" . print_r($page_template, true) . "\" not found");
		}

		return $instances;
	}

	public function getLocaleSlug($page_slug, $currentLocale, $requestedLocale)
	{
		$result = $this->database->query("SELECT r.page_slug FROM {$this->repository->pages->getNameLocale()} AS r LEFT JOIN {$this->repository->pages->getNameLocale()} AS c ON c.page_id = r.page_id WHERE c.page_slug = " . $this->quote($page_slug) . "")->fetch();

		return $result->page_slug;
	}
}

abstract class Entity
{

	function __construct($data = array())
	{
		foreach ($data AS $property => $value) {
			$this->$property = $value;
		}
	}
}

class Block extends Entity
{
	public $block_id = 0,
		$block_html = "";

	public function __toString()
	{
		return (string)$this->block_html;
	}

	public function setKey($key)
	{
		$this->block_key = $key;
	}

	public function setHTML($html, $locales)
	{
		$locales = (array)$locales;

		foreach ($locales AS $locale) {
			$this->block_html[$locale] = is_array($html) ? $html[$locale] : $html;
		}
	}
}

class Structure extends Entity
{
	public $structure_id = 0,
		$structure_rank = 1,
		$structure_parent_id = null;

}