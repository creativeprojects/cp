<?php

namespace Repository;

class pagesBlocks extends \Repository\BaseRepository
{
	public $name = "pages_blocks";

	public $fields = array(
		"block_id" => "primary",
		"page_id" => array(
			"type" => "int",
			"notNull" => true
		),
		"block_key" => "varchar",
		"block_html" => array(
			"type" => "html",
			"locale" => true
		),
	);

	protected $validateTable = true;
}