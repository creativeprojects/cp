<?php

namespace CP\Pages;

use Nette,
	CP,
	Nette\Application\UI\Form,
	App\Model\Error,
	Nette\Utils\Strings,
	Nette\Forms\Controls;

class PageForm extends CP\Forms\Base
{
	use \CP\Forms\SlugForm\SlugPart;

	protected $imagesDir;

	public function setImagesDir($imagesDir)
	{
		$this->presenter->filesService->validateDir($imagesDir);
		
		$this->imagesDir = $imagesDir;

		return $this;
	}

	public function __construct($presenter){
		parent::__construct($presenter);

		$imagesDir = $this->presenter->client["dir"] . "/files/pages";

		$this->setImagesDir($imagesDir);
	}

	public function create()
	{
		$this->form->addHidden('page_id');

		$pageTitleContainer = $this->form->addContainer('page_title');
		
		foreach ($this->presenter->translator->getavailablelocales() as $locale) {
			$pageTitleContainer->addText($locale)
				->setValue($this->values['page_title'][$locale]);
		}

		$this->addBlocksPart();

		$this->addImagesPart();

		$this->addSlugPart();
		
		$page_template = $this->form->addContainer('page_template');
		
		foreach ($this->presenter->translator->getavailablelocales() as $locale) {
			if($this->presenter->user->isInRole("superAdmin")){
				$page_template->addSelect($locale, null, $this->getPageTemplateOptions($locale))
					->setPrompt("Použít výchozí ...")
					->setValue($this->values['page_template'][$locale]);
			} else {
				$page_template->addHidden($locale);
			}
		}

		if($this->presenter->user->isAllowed("Admin:Pages", "publish") && $this->presenter->user->isAllowed("Admin:Pages", "unpublish")){
			$this->form->addCheckbox('page_published');
		} else {
			$this->form->addHidden("page_published");
		}

		$this->form->addSubmit('save', "Save changes")
			->onClick[] = callback($this, "formSave");

		$this->setDefaults();

		return $this->form;
	}

	protected function addBlocksPart()
	{
		$htmlBlocks = $this->presenter->pagesService->getTemplateMacros("htmlblock", $this->values["page_template"]);

		$this->form->properties["htmlBlocks"] = $htmlBlocks;

		$blocksContainer = $this->form->addContainer('blocks');

		foreach ($htmlBlocks as $block_key => $block) {
			$blockContainer = $blocksContainer->addContainer($block_key);

			$blockContainer->addHidden('block_id');
			
			$blockContainer->addHidden('block_key', $block_key);
			
			$blockContainerHtml = $blockContainer->addContainer('block_html');
			
			foreach ($this->presenter->translator->getAvailableLocales() as $locale) {
				$blockContainerHtml->addTextArea($locale);
			}
		}
	}

	protected function addImagesPart()
	{
		$albums = $this->presenter->pagesService->getTemplateMacros("album", $this->values["page_template"]);

		$this->form->properties["albums"] = $albums;

		foreach($albums AS $key => $gallery){
			$this->form->addGallery($this->presenter, $key, $this->imagesDir);
		}
	}

	protected function getPageTemplateOptions($locale)
	{
		$options = array();

		foreach($this->presenter->pagesService->templateDirs AS $folder){
			$folder = $folder . "/{$locale}";
			
			if (is_dir($folder) && $handle = opendir($folder)) {
				while (false !== ($entry = readdir($handle))) {
					if (substr($entry, -5, 5) == "latte") {
						$options[substr($entry, 0, -6)] = ucfirst(substr($entry, 0, -6));
					}
				}
				
				closedir($handle);
			}
		}

		return $options;
	}

	public function formSave($button)
	{
		$page = $this->sanitizeOutputValues($button->parent->getValues(true));

		$this->presenter->pagesService->save((array) $page);

		$this->presenter->flashMessage("Stránka byla uložena", "alert-success");

		$this->presenter->restoreRequest($this->presenter->backlink);
		$this->presenter->redirect("Pages:default");
	}

	protected function sanitizeOutputValues($values)
	{
		foreach($this->form->properties["albums"] AS $key => $album)
			unset($values[$key]["files"]);

		return $values;
	}
}