<?php

namespace Repository;

class pagesStructure extends \Repository\BaseRepository
{
	public $name = "pages_structure";

	public $fields = array(
		"structure_id" => "primary",
		"parent_structure_id" => "int",
		"page_id" => "int",
		"structure_rank" => "tinyint",
	);
}