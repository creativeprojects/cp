<?php

namespace Repository;

class notifications extends \Repository\BaseRepository
{
	public $name = "notifications";

	public $fields = array(
		"notification_id" => "primary",
		"notification_key" => "varchar",
		"notification_table_name" => "varchar",
		"notification_table_key" => "int",
		"notification_deadline" => "timestamp",
		"notification_done" => "timestamp",
		"notification_url" => "varchar",
		"notification_content" => "lighthtml",
		"client_id" => "int",
		"user_id" => "int",
		"role" => "json",
	);

	public $validateTable = true;

}