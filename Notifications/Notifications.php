<?php

namespace CP\Notifications;

use Nette,
	CP,
	CP\Notifications;

class Service extends CP\Service
{
	private $notification;

	protected $locale;

	public function setup(CP\Emails\Service $email)
	{
		$this->email = $email;
	}

	public function exists(array $conditions)
	{
		$where = array();

		foreach ($conditions AS $property) {
			$where[$property] = $this->notification->$property;
		}

		$count = $this->repository->notifications->getCountBy($where);

		return $count;
	}

	public function create()
	{
		$this->notification = new Entity;

		return $this;
	}

	public function setKey($notification_key)
	{
		$this->notification->notification_key = $notification_key;

		return $this;
	}

	public function setTable($notification_table_name, $notification_table_key)
	{
		$this->notification->notification_table_name = (string)$notification_table_name;
		$this->notification->notification_table_key = $notification_table_key;

		return $this;
	}

	public function setValues($notification_values)
	{
		$this->notification->notification_values = json_encode($notification_values);

		return $this;
	}

	public function setLocale($locale)
	{
		$this->locale = $locale;

		return $this;
	}

	public function setTemplate($templatePath, $notification_values = null)
	{
		if (isset($notification_values))
			$this->setValues($notification_values);

		$content = $this->template
			->create()
			->setDir($this->settings["templatesDir"])
			->setLocale($this->locale)
			->setParameters($notification_values)
			->setFile($templatePath)
			->process();

		$this->setContent($content);

		return $this;
	}

	public function setContent($notification_content)
	{
		$this->notification->notification_content = $notification_content;

		return $this;
	}

	public function setPersist($notification_persist = true)
	{
		$this->notification->notification_persist = $notification_persist;

		return $this;
	}

	public function setUrl($notification_url)
	{
		$this->notification->notification_url = "http://" . $_SERVER['SERVER_NAME'] . $notification_url;

		return $this;
	}

	public function setUserId($user_id)
	{
		$this->notification->user_id = $user_id;

		return $this;
	}

	public function serRoleId($role)
	{
		return $this->setRole($role);
	}

	public function setRole($role)
	{
		$this->notification->role = $role;

		return $this;
	}

	public function setDeadline($notification_deadline)
	{
		$this->notification->notification_deadline = date("Y-m-d H:i:s", strtotime($notification_deadline));

		return $this;
	}

	public function save()
	{
		$notification = (array)$this->notification;
		if (!isset($notification['client_id'])) {
			$notification['client_id'] = $this->client['id'];
		}

		$result = $this->repository->notifications->insert($notification);
	}

	public function getById($notification_id)
	{
		$notification = $this->repository->notifications->getOne($notification_id);

		$this->notification = new Entity($notification);

		return $this->notification;
	}

	public function saveDone()
	{
		$by = array(
			"notification_id" => $this->notification->notification_id
		);

		return $this->saveDoneBy($by);
	}

	public function saveDoneBy(array $where = null)
	{
		$values = array(
			'notification_done' => now()
		);

		return $this->updateWhere($where, $values);
	}

	public function saveActive(array $where = null)
	{
		if (!isset($where)) {
			$where = array(
				"notification_id" => $this->notification->notification_id
			);
		}

		$values = array(
			'notification_done' => null
		);

		return $this->updateWhere($where, $values);
	}

	public function getAllMyActive()
	{
		$notifications = $this->getMy(array("notification_done" => null));

		return $notifications;
	}

	public function getAllMyDone()
	{
		$notifications = $this->getMy(array("notification_done IS NOT" => null));

		return $notifications;
	}

	public function getMy(array $where)
	{
		$by = array(
			"where" => array_extend(
				$where,
				array(
					"user_id = {$this->user["user_id"]} OR role LIKE ('%\"" . $this->user["role"] . "\"%')",
					"client_id = {$this->client["id"]}"
				)
			),
			"order" => array(
				"notification_done" => "DESC",
				"notification_deadline" => "ASC",
				"created" => "ASC"
			)
		);

		$notifications = $this->repository->notifications->getAllBy($by);

		foreach ($notifications AS & $notification)
			$notification = new Entity($notification);

		return $notifications;
	}

	public function count($where)
	{
		return $this->repository->notifications->getCountBy($where);
	}

	function updateWhere($where, $values)
	{
		return $this->repository->notifications->updateBy($where, $values);
	}
}

class Entity
{
	public $notification_id,
		$notification_key,
		$notification_table_name,
		$notification_table_key,
		$notification_persist = false,
		$notification_content,
		$notification_url,
		$notification_values,
		$user_id,
		$role;

	public function __construct($notification = null)
	{
		if ($notification)
			$this->load($notification);
	}

	public function load($notification)
	{
		foreach ($notification AS $property => $value)
			$this->$property = $value;
	}
}